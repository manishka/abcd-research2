Experiment all_results for
 datafile = ../data/tsdlr/internet-traffic-data-in-bits-fr-2.mat

 Running experiment:
description = An example experiment,
data_dir = ../data/tsdlr/,
max_depth = 2,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 2,
sd = 2,
jitter_sd = 0.1,
max_jobs = 500,
verbose = True,
make_predictions = False,
skip_complete = False,
results_dir = ../examples/,
iters = 10,
base_kernels = SE,Per,Lin,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 0,
period_heuristic = 10,
max_period_heuristic = 5,
subset = False,
subset_size = 250,
full_iters = 0,
bundle_size = 1,
search_operators = None,
score = BIC,
period_heuristic_type = both,
stopping_criteria = [],
improvement_tolerance = 0.1,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=10.8447836901), SqExpKernel(dimension=0, lengthscale=1.93625484567, sf=-1.74638920188)]), likelihood=LikGauss(sf=-inf), nll=12265.0028257, ndata=1000)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=9.67893753867), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.89170272157, sf=-0.649088403119), SumKernel(operands=[ConstKernel(sf=12.0409455273), SqExpKernel(dimension=0, lengthscale=-6.03024950846, sf=10.0326320093)])])]), likelihood=LikGauss(sf=-inf), nll=11170.9835864, ndata=1000)
