Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-014,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-014/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 7,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.14066146979), SqExpKernel(dimension=0, lengthscale=-0.756413685839, sf=10.0006195151)]), likelihood=LikGauss(sf=-inf), nll=3947.71560153, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1954.98460454, sf=-0.185347128428), SumKernel(operands=[NoiseKernel(sf=3.99046933203), SqExpKernel(dimension=0, lengthscale=-0.751657378573, sf=6.91396724585)])]), likelihood=LikGauss(sf=-inf), nll=3680.16833404, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[NoiseKernel(sf=0.910586673833), SqExpKernel(dimension=0, lengthscale=1.9077023642, sf=6.22070385602)]), SumKernel(operands=[PeriodicKernel(dimension=0, lengthscale=1.87779548476, period=-0.00923311501239, sf=2.05131833812), LinearKernel(dimension=0, location=1953.28581454, sf=2.92015021487)])]), likelihood=LikGauss(sf=-inf), nll=3568.37428935, ndata=428)
