Experiment all_results for
 datafile = ../data/tsdlr_9010/03-mauna2003.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-014,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-014/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 7,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.756678161096), SqExpKernel(dimension=0, lengthscale=3.81320235355, sf=3.63697590827)]), likelihood=LikGauss(sf=-inf), nll=1080.30830134, ndata=490)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.565792420327), SqExpKernel(dimension=0, lengthscale=3.74318992242, sf=3.60605344899), PeriodicKernel(dimension=0, lengthscale=0.727269713709, period=-0.000484619273783, sf=1.1122985212)]), likelihood=LikGauss(sf=-inf), nll=451.803047004, ndata=490)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.23442160671), SqExpKernel(dimension=0, lengthscale=0.494341941755, sf=0.1111190112), SqExpKernel(dimension=0, lengthscale=3.9396085093, sf=3.84524234179), PeriodicKernel(dimension=0, lengthscale=0.864626289454, period=-0.000411004092696, sf=0.906259629962)]), likelihood=LikGauss(sf=-inf), nll=206.673106994, ndata=490)
