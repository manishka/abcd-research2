Experiment all_results for
 datafile = ../data/tsdlr_9010/01-airline.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-014,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-014/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 7,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.62033616522), SqExpKernel(dimension=0, lengthscale=-1.31644104444, sf=5.39930368652)]), likelihood=LikGauss(sf=-inf), nll=653.607365772, ndata=129)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.42506901712), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.73232094195, sf=3.12134089988), SumKernel(operands=[ConstKernel(sf=2.00829808723), PeriodicKernel(dimension=0, lengthscale=-0.973176340718, period=0.694844469801, sf=0.783790448839)])])]), likelihood=LikGauss(sf=-inf), nll=563.582938671, ndata=129)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=1.9841254565), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.91737833048), PeriodicKernel(dimension=0, lengthscale=-0.950802402229, period=0.694794567137, sf=-0.727261473747)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.821519127295, sf=2.7746218117), LinearKernel(dimension=0, location=1949.14468581, sf=3.81152601832)])])]), likelihood=LikGauss(sf=-inf), nll=526.244338405, ndata=129)
