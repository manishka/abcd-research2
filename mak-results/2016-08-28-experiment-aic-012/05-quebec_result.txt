Experiment all_results for
 datafile = ../data/uci-regression/pred/05-quebec.mat

 Running experiment:
description = 2016-08-28-experiment-aic-012,
data_dir = ../data/uci-regression/pred/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-28-experiment-aic-012/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 79,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34495897282), LinearKernel(dimension=1, location=3.96397530447, sf=4.22826716555)]), likelihood=LikGauss(sf=-inf), nll=21929.4905127, ndata=4602)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.44171829278), SqExpKernel(dimension=0, lengthscale=-1.71703863154, sf=-1.0869556831)]), SumKernel(operands=[NoiseKernel(sf=1.61194188525), LinearKernel(dimension=1, location=3.96174439674, sf=2.87887776714)])]), likelihood=LikGauss(sf=-inf), nll=20733.040367, ndata=4602)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.8386005288), SqExpKernel(dimension=0, lengthscale=-1.77201348511, sf=-1.94603042306)]), SumKernel(operands=[NoiseKernel(sf=2.18867420444), LinearKernel(dimension=1, location=3.80977228777, sf=3.56649120121), LinearKernel(dimension=2, location=0.36660299057, sf=5.33795964197)])]), likelihood=LikGauss(sf=-inf), nll=20653.8533661, ndata=4602)
