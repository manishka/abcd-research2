Experiment all_results for
 datafile = ../data/tsdlr_9010/01-airline.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-013,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-013/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 58,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.73364352607), LinearKernel(dimension=0, location=1946.07118815, sf=3.44213242425)]), likelihood=LikGauss(sf=-inf), nll=669.003094152, ndata=129)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.146225314955), PeriodicKernel(dimension=0, lengthscale=2.01336283702, period=0.00128435283293, sf=-0.421026368873)]), SumKernel(operands=[NoiseKernel(sf=2.61221191103), LinearKernel(dimension=0, location=1945.79128158, sf=3.18430785158)])]), likelihood=LikGauss(sf=-inf), nll=573.10004444, ndata=129)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-1.43029680257), PeriodicKernel(dimension=0, lengthscale=-0.416917535297, period=0.00137384635137, sf=-2.28297063143)]), SumKernel(operands=[NoiseKernel(sf=3.36105441066), SqExpKernel(dimension=0, lengthscale=-0.139779294242, sf=4.35464301688), LinearKernel(dimension=0, location=1945.8375241, sf=3.52872939596)])]), likelihood=LikGauss(sf=-inf), nll=500.743071684, ndata=129)
