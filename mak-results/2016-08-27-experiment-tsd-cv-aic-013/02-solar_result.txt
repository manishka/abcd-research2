Experiment all_results for
 datafile = ../data/tsdlr_9010/02-solar.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-013,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-013/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 58,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.56248680781), SqExpKernel(dimension=0, lengthscale=6.23094027496, sf=7.05843919938)]), likelihood=LikGauss(sf=-inf), nll=-12.7252646708, ndata=361)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.4791555636), SqExpKernel(dimension=0, lengthscale=0.860468194406, sf=-5.38619340093)]), SumKernel(operands=[NoiseKernel(sf=-6.23991168553), SqExpKernel(dimension=0, lengthscale=13.2305929673, sf=3.99937646199)])]), likelihood=LikGauss(sf=-inf), nll=-235.043730477, ndata=361)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=4.55276383013), SqExpKernel(dimension=0, lengthscale=0.693608347051, sf=-4.26084593713), SqExpKernel(dimension=0, lengthscale=3.30326944867, sf=-4.01967762412)]), SumKernel(operands=[NoiseKernel(sf=-7.35594725818), SqExpKernel(dimension=0, lengthscale=13.6824583337, sf=2.62433509763)])]), likelihood=LikGauss(sf=-inf), nll=-246.024580309, ndata=361)
