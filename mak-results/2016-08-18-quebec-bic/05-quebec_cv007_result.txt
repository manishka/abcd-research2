Experiment all_results for
 datafile = ../data/uci-regression/quebec/05-quebec_cv007.mat

 Running experiment:
description = An 2016-08-18 quebec bic experiment,
data_dir = ../data/uci-regression/quebec/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = False,
skip_complete = True,
results_dir = ../mak-results/2016-08-18-quebec-bic/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 3,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34373017697), LinearKernel(dimension=1, location=3.96324822825, sf=4.21021591447)]), likelihood=LikGauss(sf=-inf), nll=24357.9829655, rmse=nan, ndata=5113)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.13587724037), SqExpKernel(dimension=0, lengthscale=-1.35113225501, sf=3.26326992324), LinearKernel(dimension=1, location=4.14405895114, sf=4.2574308716)]), likelihood=LikGauss(sf=-inf), nll=23093.8032363, rmse=nan, ndata=5113)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[LinearKernel(dimension=1, location=3.93863789176, sf=4.24242170855), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.738606252286), LinearKernel(dimension=1, location=1.89105529097, sf=0.290846412346)]), SumKernel(operands=[NoiseKernel(sf=1.92501293789), SqExpKernel(dimension=0, lengthscale=-1.77993600179, sf=1.8964583633)])])]), likelihood=LikGauss(sf=-inf), nll=22907.6245648, rmse=nan, ndata=5113)
