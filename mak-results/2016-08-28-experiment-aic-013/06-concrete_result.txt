Experiment all_results for
 datafile = ../data/uci-regression/pred/06-concrete.mat

 Running experiment:
description = 2016-08-28-experiment-aic-013,
data_dir = ../data/uci-regression/pred/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-28-experiment-aic-013/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 1,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.61006134707), SqExpKernel(dimension=7, lengthscale=4.3931128836, sf=3.57812216975)]), likelihood=LikGauss(sf=-inf), nll=3750.59821876, ndata=927)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.06801015488), SqExpKernel(dimension=0, lengthscale=0.369714255834, sf=3.00667450482), SqExpKernel(dimension=7, lengthscale=3.91402427728, sf=3.92196986725)]), likelihood=LikGauss(sf=-inf), nll=3385.30683986, ndata=927)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=1.30954920614), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.970004727207), SqExpKernel(dimension=6, lengthscale=-0.189554987627, sf=-0.560677336317)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=3.13299730422, sf=2.75348157312), SqExpKernel(dimension=7, lengthscale=1.73672895653, sf=1.21359375717)])])]), likelihood=LikGauss(sf=-inf), nll=3097.28606204, ndata=927)
