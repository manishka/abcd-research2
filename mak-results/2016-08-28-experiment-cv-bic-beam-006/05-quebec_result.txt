Experiment all_results for
 datafile = ../data/uci-regression/pred/05-quebec.mat

 Running experiment:
description = 2016-08-28-experiment-cv-bic-beam-006,
data_dir = ../data/uci-regression/pred/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-28-experiment-cv-bic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 51,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34479598196), LinearKernel(dimension=1, location=3.96459094447, sf=4.20196428702)]), likelihood=LikGauss(sf=-inf), nll=21929.4909778, ndata=4602)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34488675735), LinearKernel(dimension=1, location=3.96397204175, sf=4.18557445074)]), likelihood=LikGauss(sf=-inf), nll=21929.491852, ndata=4602)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34486727713), LinearKernel(dimension=1, location=3.9633689789, sf=4.18171498278)]), likelihood=LikGauss(sf=-inf), nll=21929.4922529, ndata=4602)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.05151490494), SqExpKernel(dimension=0, lengthscale=-1.82471405151, sf=2.91134038196), LinearKernel(dimension=1, location=3.961506696, sf=4.17370748958)]), likelihood=LikGauss(sf=-inf), nll=20716.5986557, ndata=4602)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.05118175907), SqExpKernel(dimension=0, lengthscale=-1.80930008153, sf=2.93562170809), LinearKernel(dimension=1, location=3.96024628422, sf=4.22573956559)]), likelihood=LikGauss(sf=-inf), nll=20716.6142796, ndata=4602)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.0455770457), SqExpKernel(dimension=0, lengthscale=-1.78547057841, sf=2.97968981683), LinearKernel(dimension=1, location=3.95564163525, sf=4.22247770814)]), likelihood=LikGauss(sf=-inf), nll=20716.9766207, ndata=4602)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[LinearKernel(dimension=1, location=4.12562954581, sf=4.1627748519), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.75328125799), PeriodicKernel(dimension=0, lengthscale=0.414603760541, period=-3.95440830258, sf=0.986235863782)]), SumKernel(operands=[NoiseKernel(sf=1.1475362885), SqExpKernel(dimension=0, lengthscale=-1.55671361623, sf=1.11687311059)])])]), likelihood=LikGauss(sf=-inf), nll=20567.4174041, ndata=4602)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.1091166085), SqExpKernel(dimension=0, lengthscale=-6.5216095176, sf=-1.40508670964)]), SumKernel(operands=[NoiseKernel(sf=0.496059301244), SqExpKernel(dimension=0, lengthscale=-1.70007155755, sf=1.90994699905), LinearKernel(dimension=1, location=3.92965097646, sf=3.12963008616)])]), likelihood=LikGauss(sf=-inf), nll=20591.1923218, ndata=4602)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.04284265544), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.3488658665), PeriodicKernel(dimension=0, lengthscale=-0.742315253032, period=-0.000232185796133, sf=-2.54840490846)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.1958583834, sf=1.21553456447), LinearKernel(dimension=1, location=3.87271021619, sf=3.57257063875)])])]), likelihood=LikGauss(sf=-inf), nll=20599.0812369, ndata=4602)
