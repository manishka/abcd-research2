Experiment all_results for
 datafile = ../data/tsdlr_9010/daily-minimum-temperatures-in-me.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 57,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=1.42136752932), ConstKernel(sf=2.41100217887)]), likelihood=LikGauss(sf=-inf), nll=2557.82549813, ndata=899)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.987444177838), ConstKernel(sf=2.4373134359), SqExpKernel(dimension=0, lengthscale=-1.55506247334, sf=1.31117007439)]), likelihood=LikGauss(sf=-inf), nll=2233.45526835, ndata=899)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[ConstKernel(sf=2.40834543735), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-1.61427942411, sf=-0.395112497215), SumKernel(operands=[ConstKernel(sf=1.63308406533), PeriodicKernel(dimension=0, lengthscale=-3.39910469988, period=-0.529122852635, sf=1.30657279176)])])]), likelihood=LikGauss(sf=-inf), nll=2178.40597309, ndata=899)
