Experiment all_results for
 datafile = ../data/tsdlr_9010/03-mauna2003.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-011,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-011/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 77,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.47687572966), SqExpKernel(dimension=0, lengthscale=-1.2314752742, sf=2.52606486507)]), likelihood=LikGauss(sf=-inf), nll=672.787887871, ndata=490)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.5807253848), SqExpKernel(dimension=0, lengthscale=-1.64546931224, sf=0.877307920099), SqExpKernel(dimension=0, lengthscale=3.77292118009, sf=3.60858743797)]), likelihood=LikGauss(sf=-inf), nll=499.538030128, ndata=490)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.91470408037), PeriodicKernel(dimension=0, lengthscale=2.14122135764, period=-0.000442794561257, sf=-0.22985839012)]), SumKernel(operands=[NoiseKernel(sf=-3.40281506527), SqExpKernel(dimension=0, lengthscale=-0.0508714497855, sf=-2.5726547435), SqExpKernel(dimension=0, lengthscale=4.55562135908, sf=2.6251090464)])]), likelihood=LikGauss(sf=-inf), nll=146.78569911, ndata=490)
