Experiment all_results for
 datafile = ../data/uci-regression/concrete/06-concrete_cv004.mat

 Running experiment:
description = An 2016-08-18 conrete cv experiment,
data_dir = ../data/uci-regression/concrete/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = False,
skip_complete = True,
results_dir = ../mak-results/2016-08-18-concrete-cv/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 3,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = rmse,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.60069986987), SqExpKernel(dimension=7, lengthscale=4.39707763877, sf=3.5858632813)]), likelihood=LikGauss(sf=-inf), nll=4156.26879718, rmse=13.9176837741, ndata=1030)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.21750199672), SqExpKernel(dimension=0, lengthscale=1.46171069507, sf=-0.790225721816)]), SumKernel(operands=[NoiseKernel(sf=0.885521425054), SqExpKernel(dimension=7, lengthscale=4.91444322429, sf=3.54824866389)])]), likelihood=LikGauss(sf=-inf), nll=3790.89261005, rmse=10.4584835916, ndata=1030)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.10289577694), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.723623446, sf=-1.90727150784), SumKernel(operands=[ConstKernel(sf=0.841335832917), SqExpKernel(dimension=6, lengthscale=-0.0983915362511, sf=0.919572878778)])])]), SumKernel(operands=[NoiseKernel(sf=0.19395471776), SqExpKernel(dimension=7, lengthscale=4.56000944105, sf=3.19525301113)])]), likelihood=LikGauss(sf=-inf), nll=3446.06025781, rmse=8.39391531279, ndata=1030)
