Experiment all_results for
 datafile = ../data/tsdlr_9010/daily-minimum-temperatures-in-me.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-002,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-002/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 47,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=1.00613464579), SqExpKernel(dimension=0, lengthscale=-0.935725449793, sf=2.40840481728)]), likelihood=LikGauss(sf=-inf), nll=2253.64115085, ndata=899)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.931462509636), SqExpKernel(dimension=0, lengthscale=-5.75803346949, sf=-0.353248990713)]), SumKernel(operands=[NoiseKernel(sf=-0.819932377583), SqExpKernel(dimension=0, lengthscale=-0.833783499633, sf=1.36335933994)])]), likelihood=LikGauss(sf=-inf), nll=2201.84829534, ndata=899)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.188283225883), SqExpKernel(dimension=0, lengthscale=-5.61401539159, sf=-1.13602105265)]), SumKernel(operands=[ConstKernel(sf=1.42239110446), PeriodicKernel(dimension=0, lengthscale=0.912290569457, period=0.00922067096099, sf=0.543244188634)]), SumKernel(operands=[NoiseKernel(sf=-1.44424664548), SqExpKernel(dimension=0, lengthscale=3.17826393788, sf=0.528684337631)])]), likelihood=LikGauss(sf=-inf), nll=2150.14072013, ndata=899)
