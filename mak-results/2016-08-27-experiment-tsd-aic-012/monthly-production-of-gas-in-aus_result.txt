Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-012,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-012/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 47,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26133076166), SqExpKernel(dimension=0, lengthscale=2.72437313465, sf=10.1745002033)]), likelihood=LikGauss(sf=-inf), nll=4159.41928922, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.804946923922), LinearKernel(dimension=0, location=1961.24108835, sf=0.291741343283)]), SumKernel(operands=[NoiseKernel(sf=4.93746480525), SqExpKernel(dimension=0, lengthscale=1.53521603188, sf=6.51455582773)])]), likelihood=LikGauss(sf=-inf), nll=3859.01919144, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-1.70116960269), LinearKernel(dimension=0, location=1961.27804697, sf=-1.88307823664)]), SumKernel(operands=[ConstKernel(sf=5.39877782413), PeriodicKernel(dimension=0, lengthscale=0.52077234787, period=-0.000888840966752, sf=0.960760862029)]), SumKernel(operands=[NoiseKernel(sf=0.605546184283), SqExpKernel(dimension=0, lengthscale=1.86692887614, sf=5.89139023396)])]), likelihood=LikGauss(sf=-inf), nll=3462.9076201, ndata=428)
