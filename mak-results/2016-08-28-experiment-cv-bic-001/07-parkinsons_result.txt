Experiment all_results for
 datafile = ../data/uci-regression/pred/07-parkinsons.mat

 Running experiment:
description = 2016-08-28-experiment-cv-bic-001,
data_dir = ../data/uci-regression/pred/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-28-experiment-cv-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 52,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=1.02287164748), SqExpKernel(dimension=0, lengthscale=-0.175528178123, sf=3.2868980177)]), likelihood=LikGauss(sf=-inf), nll=13100.5401776, ndata=5287)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-3.69995960355), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.341816021487, sf=0.356773170576), SumKernel(operands=[ConstKernel(sf=2.91049427364), SqExpKernel(dimension=3, lengthscale=3.34871830512, sf=0.896455399483)])])]), likelihood=LikGauss(sf=-inf), nll=-9068.65654866, ndata=5287)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-8.69419105986), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.618877570022, sf=-0.613021793171), SumKernel(operands=[ConstKernel(sf=0.0117074964523), SqExpKernel(dimension=3, lengthscale=4.2065078024, sf=-1.57184490928)]), SumKernel(operands=[ConstKernel(sf=3.8872757577), SqExpKernel(dimension=3, lengthscale=0.776009800174, sf=-1.06708530018)])])]), likelihood=LikGauss(sf=-inf), nll=-29335.5617451, ndata=5287)
