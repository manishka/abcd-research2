Experiment all_results for
 datafile = ../data/tsdlr_9010/01-airline.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-003,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 1,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-003/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 18,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.73364071532), LinearKernel(dimension=0, location=1946.07115403, sf=3.44193252627)]), likelihood=LikGauss(sf=-inf), nll=669.003094108, ndata=129)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.850090627799), ProductKernel(operands=[LinearKernel(dimension=0, location=1945.47749772, sf=0.531951181425), SumKernel(operands=[ConstKernel(sf=2.82798443152), SqExpKernel(dimension=0, lengthscale=-2.43115467796, sf=0.835268332061)])])]), likelihood=LikGauss(sf=-inf), nll=587.093533582, ndata=129)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1945.45950278, sf=0.504913079286), SumKernel(operands=[ConstKernel(sf=2.8623128257), SqExpKernel(dimension=0, lengthscale=-2.45019729199, sf=0.858982762166)])]), likelihood=LikGauss(sf=-inf), nll=587.216202335, ndata=129)
