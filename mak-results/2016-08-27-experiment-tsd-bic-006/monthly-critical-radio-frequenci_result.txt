Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-critical-radio-frequenci.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-003,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 1,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-003/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 18,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.883003478555), ConstKernel(sf=2.12101252488)]), likelihood=LikGauss(sf=-inf), nll=501.145310732, ndata=216)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.834912322955, sf=-0.936954050072), SumKernel(operands=[NoiseKernel(sf=0.659712402399), ConstKernel(sf=2.95738057786)])]), likelihood=LikGauss(sf=-inf), nll=371.342047954, ndata=216)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.876299621268), SqExpKernel(dimension=0, lengthscale=-1.88965148811, sf=-0.435128451503)]), SumKernel(operands=[NoiseKernel(sf=-2.01049841379), ConstKernel(sf=1.23653643939)])]), likelihood=LikGauss(sf=-inf), nll=302.442724966, ndata=216)
