Experiment all_results for
 datafile = ../data/uci-regression/quebec/05-quebec_cv012.mat

 Running experiment:
description = An 2016-08-18 quebec cv experiment,
data_dir = ../data/uci-regression/quebec/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = False,
skip_complete = True,
results_dir = ../mak-results/2016-08-18-quebec-cv/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 3,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = rmse,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34373017697), LinearKernel(dimension=1, location=3.96324822825, sf=4.21021591447)]), likelihood=LikGauss(sf=-inf), nll=24357.9829655, rmse=28.8751139036, ndata=5113)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.21764070166), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.45103875162, sf=1.48378129921), LinearKernel(dimension=1, location=3.96937716708, sf=2.73174827801)])]), likelihood=LikGauss(sf=-inf), nll=23713.2074175, rmse=25.5053715516, ndata=5113)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.99731464166), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.53179816571, sf=1.74181248255), SumKernel(operands=[PeriodicKernel(dimension=0, lengthscale=1.28011730228, period=-3.95491793179, sf=0.746633575369), LinearKernel(dimension=1, location=4.68842409147, sf=2.68803034935)])])]), likelihood=LikGauss(sf=-inf), nll=23689.7102428, rmse=24.7789086369, ndata=5113)
