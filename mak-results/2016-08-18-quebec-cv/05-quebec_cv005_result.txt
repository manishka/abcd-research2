Experiment all_results for
 datafile = ../data/uci-regression/quebec/05-quebec_cv005.mat

 Running experiment:
description = An 2016-08-18 quebec cv experiment,
data_dir = ../data/uci-regression/quebec/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = False,
skip_complete = True,
results_dir = ../mak-results/2016-08-18-quebec-cv/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 3,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = rmse,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34373017697), LinearKernel(dimension=1, location=3.96324822825, sf=4.21021591447)]), likelihood=LikGauss(sf=-inf), nll=24357.9829655, rmse=28.8751139036, ndata=5113)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.09438084312), SqExpKernel(dimension=0, lengthscale=1.69782564094, sf=1.24935076804)]), SumKernel(operands=[NoiseKernel(sf=0.10540380381), LinearKernel(dimension=1, location=3.97905721347, sf=0.831127238006)])]), likelihood=LikGauss(sf=-inf), nll=23708.1468585, rmse=25.4017712351, ndata=5113)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.11795031765), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.45484340466, sf=-1.72008275114), SumKernel(operands=[ConstKernel(sf=6.49702977649), PeriodicKernel(dimension=0, lengthscale=-0.461939645755, period=0.000480683278948, sf=2.90533102366)])])]), SumKernel(operands=[NoiseKernel(sf=-1.72678888984), LinearKernel(dimension=1, location=3.77760992411, sf=-0.180908943652)])]), likelihood=LikGauss(sf=-inf), nll=22996.7565114, rmse=22.0612646121, ndata=5113)
