Experiment all_results for
 datafile = ../data/tsdlr_9010/01-airline.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-004,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-004/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 90,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.61971494178), SqExpKernel(dimension=0, lengthscale=-1.31652590107, sf=5.39860107285)]), likelihood=LikGauss(sf=-inf), nll=653.607358466, ndata=129)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.50376870961), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.93709090982, sf=1.12491714062), SumKernel(operands=[ConstKernel(sf=7.59233151898), PeriodicKernel(dimension=0, lengthscale=-0.385558364968, period=0.692719695742, sf=3.9549367493)])])]), likelihood=LikGauss(sf=-inf), nll=579.068845977, ndata=129)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.01276950549), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.5291558525, sf=-1.95198662298), SumKernel(operands=[ConstKernel(sf=2.70990284727), SqExpKernel(dimension=0, lengthscale=0.790830828929, sf=0.856708272441)]), SumKernel(operands=[ConstKernel(sf=5.22718658521), PeriodicKernel(dimension=0, lengthscale=-0.235238458024, period=0.00129181157635, sf=3.25416129456)])])]), likelihood=LikGauss(sf=-inf), nll=513.578382654, ndata=129)
