Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-beam-006,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 79,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26253722012), SqExpKernel(dimension=0, lengthscale=2.73999292021, sf=10.1500510975)]), likelihood=LikGauss(sf=-inf), nll=4159.40858889, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26250965052), SqExpKernel(dimension=0, lengthscale=2.7399853026, sf=10.1508220577)]), likelihood=LikGauss(sf=-inf), nll=4159.40859282, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26274975281), SqExpKernel(dimension=0, lengthscale=2.73921531641, sf=10.1491769275)]), likelihood=LikGauss(sf=-inf), nll=4159.40861681, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.741188630502), LinearKernel(dimension=0, location=1961.11601721, sf=0.22485643418)]), SumKernel(operands=[NoiseKernel(sf=4.9964531951), SqExpKernel(dimension=0, lengthscale=1.53399777958, sf=6.57150293948)])]), likelihood=LikGauss(sf=-inf), nll=3859.03003473, ndata=428)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.51725508031), LinearKernel(dimension=0, location=1960.87500344, sf=0.998314286988)]), SumKernel(operands=[NoiseKernel(sf=4.20973709932), SqExpKernel(dimension=0, lengthscale=1.53320746752, sf=5.82895574155)])]), likelihood=LikGauss(sf=-inf), nll=3859.16025827, ndata=428)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.25421605062), LinearKernel(dimension=0, location=1959.78901058, sf=2.69573626202)]), SumKernel(operands=[NoiseKernel(sf=2.45393838575), SqExpKernel(dimension=0, lengthscale=1.48797048365, sf=4.08451163269)])]), likelihood=LikGauss(sf=-inf), nll=3861.21750213, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-1.10719618988), LinearKernel(dimension=0, location=1960.96727868, sf=-1.07044456668)]), SumKernel(operands=[ConstKernel(sf=2.49114195024), PeriodicKernel(dimension=0, lengthscale=0.429634754374, period=-0.00439157314631, sf=0.527093833067)]), SumKernel(operands=[NoiseKernel(sf=2.70860196064), SqExpKernel(dimension=0, lengthscale=1.58073863114, sf=5.72387025645)])]), likelihood=LikGauss(sf=-inf), nll=3452.02912731, ndata=428)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.29049240398), LinearKernel(dimension=0, location=1962.32522429, sf=0.186922056077)]), SumKernel(operands=[ConstKernel(sf=-0.14980534487), PeriodicKernel(dimension=0, lengthscale=0.648609987916, period=-0.0102732885224, sf=-1.45857599009)]), SumKernel(operands=[NoiseKernel(sf=4.13997885233), SqExpKernel(dimension=0, lengthscale=1.42908277454, sf=6.62440410133)])]), likelihood=LikGauss(sf=-inf), nll=3460.22614532, ndata=428)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.378392861969), PeriodicKernel(dimension=0, lengthscale=2.18959797978, period=-0.00419261406622, sf=-0.0487472948938), LinearKernel(dimension=0, location=1960.06589718, sf=0.1908508595)]), SumKernel(operands=[NoiseKernel(sf=3.96279958257), SqExpKernel(dimension=0, lengthscale=1.69815023194, sf=7.79493264901)])]), likelihood=LikGauss(sf=-inf), nll=3478.50582538, ndata=428)
