Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-beam-006,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 49,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370841607957), SqExpKernel(dimension=0, lengthscale=3.44126718106, sf=1.74169705435)]), likelihood=LikGauss(sf=-inf), nll=777.525577642, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370860972548), SqExpKernel(dimension=0, lengthscale=3.44217176162, sf=1.7551787591)]), likelihood=LikGauss(sf=-inf), nll=777.531378341, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.368729074966), SqExpKernel(dimension=0, lengthscale=3.43604501359, sf=1.72073473889)]), likelihood=LikGauss(sf=-inf), nll=777.534676375, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.200949104095), SqExpKernel(dimension=0, lengthscale=0.399695910039, sf=-2.59036964241)]), SumKernel(operands=[NoiseKernel(sf=-1.13232424062), SqExpKernel(dimension=0, lengthscale=4.44287851413, sf=2.38594189524)])]), likelihood=LikGauss(sf=-inf), nll=589.373241379, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.28947702576), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.43777382336, sf=1.30763478473), SumKernel(operands=[ConstKernel(sf=0.656903355391), SqExpKernel(dimension=0, lengthscale=0.392841752485, sf=-1.51330353397)])])]), likelihood=LikGauss(sf=-inf), nll=589.772412832, ndata=661)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.15264852777), SqExpKernel(dimension=0, lengthscale=0.00315248790005, sf=-2.07688822822)]), SumKernel(operands=[NoiseKernel(sf=-2.65045445791), SqExpKernel(dimension=0, lengthscale=4.38738720725, sf=1.79666620605)])]), likelihood=LikGauss(sf=-inf), nll=630.181310228, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-1.12048150137), SqExpKernel(dimension=0, lengthscale=0.403704728878, sf=-2.84464301007)]), SumKernel(operands=[NoiseKernel(sf=-0.232151499032), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.41542841144, sf=0.407176448969), SumKernel(operands=[ConstKernel(sf=1.7752401446), LinearKernel(dimension=0, location=1598.40941085, sf=-3.28389496042)])])])]), likelihood=LikGauss(sf=-inf), nll=567.720957041, ndata=661)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.229550878405), SqExpKernel(dimension=0, lengthscale=0.366139915176, sf=-1.70346572576)]), SumKernel(operands=[NoiseKernel(sf=-1.56411005549), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.3990917597, sf=1.32150493918), SumKernel(operands=[ConstKernel(sf=-0.146968126334), LinearKernel(dimension=0, location=1556.0695111, sf=-5.57166899704)])])])]), likelihood=LikGauss(sf=-inf), nll=570.319488067, ndata=661)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.299131878243), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.423209709846, sf=-4.65214275653), SumKernel(operands=[ConstKernel(sf=2.46276125672), LinearKernel(dimension=0, location=1588.13477653, sf=-2.54571168511)])])]), SumKernel(operands=[NoiseKernel(sf=-1.58223663818), SqExpKernel(dimension=0, lengthscale=4.45467848809, sf=1.54243828012)])]), likelihood=LikGauss(sf=-inf), nll=571.152631664, ndata=661)
