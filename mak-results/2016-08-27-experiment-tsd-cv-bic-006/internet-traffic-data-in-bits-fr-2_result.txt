Experiment all_results for
 datafile = ../data/tsdlr_9010/internet-traffic-data-in-bits-fr-2.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-003,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 1,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-003/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 46,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=9.79342201556), SqExpKernel(dimension=0, lengthscale=-3.40686632464, sf=10.6854859683)]), likelihood=LikGauss(sf=-inf), nll=10212.2027771, ndata=909)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.88962929384), PeriodicKernel(dimension=0, lengthscale=-0.910745411726, period=-5.20756909879, sf=-1.00570979157)]), SumKernel(operands=[NoiseKernel(sf=5.49727397725), SqExpKernel(dimension=0, lengthscale=-2.75237038533, sf=10.4665868147)])]), likelihood=LikGauss(sf=-inf), nll=9915.21716459, ndata=909)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=9.38962829683), SqExpKernel(dimension=0, lengthscale=-1.96538501921, sf=5.81109441587), PeriodicKernel(dimension=0, lengthscale=-1.82494077123, period=-3.95434010996, sf=-0.287367263712)]), SumKernel(operands=[NoiseKernel(sf=-0.256380279423), SqExpKernel(dimension=0, lengthscale=-0.395942423479, sf=10.1381885014)])]), likelihood=LikGauss(sf=-inf), nll=9745.90116304, ndata=909)
