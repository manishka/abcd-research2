# An example experiment that runs quickly.

Experiment(description='2016-08-28-experiment-bic-beam-005',
		data_dir='../data/uci-regression/pred/05-quebec.mat',
		results_dir='../mak-results/2016-08-28-experiment-bic-beam-005/',
		max_depth=3,		  # How deep to run the search.
		k=3,				  # Keep the k best kernels at every iteration.  1 => greedy search.
		n_rand=9,		     # Number of random restarts.
		local_computation = True,
		sd=2,
		jitter_sd=0.1,
		max_jobs = 4500,
		iters=100,		     # How long to optimize hyperparameters for.
		base_kernels='SE,Per,Lin,Const,Noise',
		verbose=True,
		random_seed = random.randint(1,100),
		period_heuristic = 3,
		subset = True,
		full_iters = 10,
		bundle_size = 2000,
        search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
		#search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('CP', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('CW', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('B', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('BL', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('None',), {'A': 'kernel'})],
		period_heuristic_type = 'min',
		make_predictions=True,
		skip_complete=True,
		stopping_criteria = ['no_improvement'],
		improvement_tolerance = 0.1,
		job_time='01:30:00',
        score = 'bic', crossval = False)
