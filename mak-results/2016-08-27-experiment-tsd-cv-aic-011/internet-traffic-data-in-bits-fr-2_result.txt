Experiment all_results for
 datafile = ../data/tsdlr_9010/internet-traffic-data-in-bits-fr-2.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-011,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-011/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 28,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.61573655062), SqExpKernel(dimension=0, lengthscale=-7.51737276813, sf=10.4308025954)]), likelihood=LikGauss(sf=-inf), nll=9339.76319077, ndata=909)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.7830188758), SqExpKernel(dimension=0, lengthscale=-2.81642960504, sf=10.734853643), SqExpKernel(dimension=0, lengthscale=-7.63588827107, sf=9.8857812723)]), likelihood=LikGauss(sf=-inf), nll=9162.75571111, ndata=909)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-7.47473847657, sf=9.8868131997), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=7.59822296931), PeriodicKernel(dimension=0, lengthscale=1.99229705041, period=-6.59275798956, sf=2.27504646166)]), SumKernel(operands=[NoiseKernel(sf=0.108877373518), SqExpKernel(dimension=0, lengthscale=-0.665841562731, sf=8.88498269179)])])]), likelihood=LikGauss(sf=-inf), nll=9055.84148549, ndata=909)
