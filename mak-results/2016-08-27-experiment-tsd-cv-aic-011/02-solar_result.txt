Experiment all_results for
 datafile = ../data/tsdlr_9010/02-solar.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-011,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-011/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 28,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.5624686819), SqExpKernel(dimension=0, lengthscale=6.23113825708, sf=7.05933725757)]), likelihood=LikGauss(sf=-inf), nll=-12.7252675465, ndata=361)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.72132382601), ConstKernel(sf=7.21485409506), SqExpKernel(dimension=0, lengthscale=3.15756200578, sf=-1.16709069055)]), likelihood=LikGauss(sf=-inf), nll=-70.021322383, ndata=361)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.68763061915), ConstKernel(sf=7.54605351957), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.39330941082, sf=-2.09322554666), SumKernel(operands=[ConstKernel(sf=0.652913275104), PeriodicKernel(dimension=0, lengthscale=0.832282975725, period=2.36682989419, sf=0.252383925616)])])]), likelihood=LikGauss(sf=-inf), nll=-258.140845605, ndata=361)
