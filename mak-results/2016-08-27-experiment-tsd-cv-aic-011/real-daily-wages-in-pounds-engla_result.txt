Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-011,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-011/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 28,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.37135922464), SqExpKernel(dimension=0, lengthscale=3.44043690459, sf=1.73870614704)]), likelihood=LikGauss(sf=-inf), nll=777.52541024, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.33447406582), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.40131893009, sf=1.82801884983), SumKernel(operands=[ConstKernel(sf=0.109370108209), SqExpKernel(dimension=0, lengthscale=0.399796300211, sf=-2.02491882146)])])]), likelihood=LikGauss(sf=-inf), nll=589.186430796, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.35838537149), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.103854486119), SqExpKernel(dimension=0, lengthscale=0.41475528209, sf=-2.46658120035)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.41040259369, sf=1.66691107549), LinearKernel(dimension=0, location=1651.45628043, sf=-3.1277204545)])])]), likelihood=LikGauss(sf=-inf), nll=567.624582366, ndata=661)
