Experiment all_results for
 datafile = ../data/tsdlr_9010/number-of-daily-births-in-quebec.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 47,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67579703143), SqExpKernel(dimension=0, lengthscale=2.07965734452, sf=5.68310737844)]), likelihood=LikGauss(sf=-inf), nll=4565.13273369, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.6766687823), SqExpKernel(dimension=0, lengthscale=2.08640625598, sf=5.61327738577)]), likelihood=LikGauss(sf=-inf), nll=4565.13887162, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67649217934), SqExpKernel(dimension=0, lengthscale=2.05840553394, sf=5.58160712328)]), likelihood=LikGauss(sf=-inf), nll=4565.15033965, ndata=893)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.3166285081), PeriodicKernel(dimension=0, lengthscale=-0.0802450461794, period=-4.64730109136, sf=1.261795146)]), SumKernel(operands=[NoiseKernel(sf=0.73506193763), SqExpKernel(dimension=0, lengthscale=2.58899113023, sf=5.09655068285)])]), likelihood=LikGauss(sf=-inf), nll=4225.92044484, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.41061144841), PeriodicKernel(dimension=0, lengthscale=4.54939620591, period=-3.95459511269, sf=2.15248968279)]), SumKernel(operands=[NoiseKernel(sf=1.21210967088), SqExpKernel(dimension=0, lengthscale=6.30604475312, sf=2.39476136725)])]), likelihood=LikGauss(sf=-inf), nll=4360.16660364, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.36871395352), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.96712523257, sf=5.45916830313), PeriodicKernel(dimension=0, lengthscale=-0.128357337616, period=-4.64713724128, sf=4.04150292337)])]), likelihood=LikGauss(sf=-inf), nll=4377.35717994, ndata=893)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[NoiseKernel(sf=-0.287143295568), SqExpKernel(dimension=0, lengthscale=2.74120627855, sf=2.96749408629)]), SumKernel(operands=[NoiseKernel(sf=-0.758704051951), ConstKernel(sf=3.40401469894), PeriodicKernel(dimension=0, lengthscale=-0.122923261078, period=-3.95462686699, sf=1.10701045364)])]), likelihood=LikGauss(sf=-inf), nll=4167.51849435, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.85113502885), PeriodicKernel(dimension=0, lengthscale=4.25400502794, period=-3.95471630744, sf=1.38446293619)]), SumKernel(operands=[NoiseKernel(sf=0.363884315774), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=6.3960706359, sf=3.7885527412), SumKernel(operands=[ConstKernel(sf=0.985802017324), SqExpKernel(dimension=0, lengthscale=1.56874092709, sf=-1.60466942365)])])])]), likelihood=LikGauss(sf=-inf), nll=4190.0607516, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.83590655644), PeriodicKernel(dimension=0, lengthscale=1.10568612456, period=-3.95469664213, sf=0.287442557992)]), SumKernel(operands=[NoiseKernel(sf=-0.53967992044), SqExpKernel(dimension=0, lengthscale=4.07938608267, sf=3.74477573532)])]), likelihood=LikGauss(sf=-inf), nll=4194.6653827, ndata=893)
