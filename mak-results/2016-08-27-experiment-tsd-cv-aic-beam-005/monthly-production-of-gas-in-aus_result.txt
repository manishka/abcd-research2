Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 47,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26218061957), SqExpKernel(dimension=0, lengthscale=2.73672201344, sf=10.1392848026)]), likelihood=LikGauss(sf=-inf), nll=4159.40906075, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26252739182), SqExpKernel(dimension=0, lengthscale=2.74321513326, sf=10.165711995)]), likelihood=LikGauss(sf=-inf), nll=4159.40943429, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.23204834226), SqExpKernel(dimension=0, lengthscale=1.57504570868, sf=9.87619600792)]), likelihood=LikGauss(sf=-inf), nll=4160.76261253, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.97541414084), PeriodicKernel(dimension=0, lengthscale=-0.311873381121, period=0.69357026402, sf=0.362390073287)]), SumKernel(operands=[NoiseKernel(sf=4.16470367724), SqExpKernel(dimension=0, lengthscale=1.70530944, sf=7.77853494758)])]), likelihood=LikGauss(sf=-inf), nll=3798.36135326, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.3119369762), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.54639636985, sf=9.62590532657), SumKernel(operands=[ConstKernel(sf=8.37031976596), PeriodicKernel(dimension=0, lengthscale=1.114428002, period=0.00441772711542, sf=-1.68331501607)])])]), likelihood=LikGauss(sf=-inf), nll=3841.30708454, ndata=428)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.41659874535), LinearKernel(dimension=0, location=1959.46551693, sf=1.91997236496)]), SumKernel(operands=[NoiseKernel(sf=3.21917834713), SqExpKernel(dimension=0, lengthscale=1.52012135134, sf=4.74886327776)])]), likelihood=LikGauss(sf=-inf), nll=3861.89671018, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.57256328771), PeriodicKernel(dimension=0, lengthscale=2.00037495019, period=-0.00299728891805, sf=2.12767513421), LinearKernel(dimension=0, location=1959.97271689, sf=2.48543812825)]), SumKernel(operands=[NoiseKernel(sf=1.66646250231), SqExpKernel(dimension=0, lengthscale=1.73424807479, sf=5.70922972974)])]), likelihood=LikGauss(sf=-inf), nll=3478.56774772, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[ProductKernel(operands=[NoiseKernel(sf=0.234550510771), SumKernel(operands=[ConstKernel(sf=4.68585579208), LinearKernel(dimension=0, location=1959.67513946, sf=3.85595182385)])]), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.66743459038, sf=7.24806816494), SumKernel(operands=[ConstKernel(sf=3.55905627304), PeriodicKernel(dimension=0, lengthscale=2.07778267988, period=0.00298349984489, sf=0.650220303392)])])]), likelihood=LikGauss(sf=-inf), nll=3505.02409547, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[ProductKernel(operands=[NoiseKernel(sf=-0.00244477446974), SumKernel(operands=[ConstKernel(sf=4.05237388621), LinearKernel(dimension=0, location=1962.79896202, sf=4.26172982367)])]), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.24909300582, sf=9.39319854882), SumKernel(operands=[ConstKernel(sf=7.87871329263), PeriodicKernel(dimension=0, lengthscale=1.16416978595, period=-0.000413356740144, sf=-1.4187864386)])])]), likelihood=LikGauss(sf=-inf), nll=3523.8833714, ndata=428)
