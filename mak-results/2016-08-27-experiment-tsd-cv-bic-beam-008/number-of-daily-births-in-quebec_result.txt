Experiment all_results for
 datafile = ../data/tsdlr_9010/number-of-daily-births-in-quebec.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-beam-008,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-beam-008/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 41,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67628165173), SqExpKernel(dimension=0, lengthscale=2.09303480092, sf=5.67185473748)]), likelihood=LikGauss(sf=-inf), nll=4565.12988254, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67623384947), SqExpKernel(dimension=0, lengthscale=2.09011271904, sf=5.66959047722)]), likelihood=LikGauss(sf=-inf), nll=4565.13004324, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67617393275), SqExpKernel(dimension=0, lengthscale=2.07881939115, sf=5.63313928777)]), likelihood=LikGauss(sf=-inf), nll=4565.13417657, ndata=893)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.67797263595), PeriodicKernel(dimension=0, lengthscale=1.17330681592, period=-3.95462586841, sf=0.836687816722)]), SumKernel(operands=[NoiseKernel(sf=0.491247562632), SqExpKernel(dimension=0, lengthscale=2.28450433179, sf=5.60475371311)])]), likelihood=LikGauss(sf=-inf), nll=4205.62542746, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-2.38627640406), PeriodicKernel(dimension=0, lengthscale=-0.399158570374, period=-4.42406897571, sf=-0.265802334864)]), SumKernel(operands=[NoiseKernel(sf=3.54219628146), SqExpKernel(dimension=0, lengthscale=2.62120257744, sf=7.00944599721)])]), likelihood=LikGauss(sf=-inf), nll=4383.29043647, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.115171980827), PeriodicKernel(dimension=0, lengthscale=-4.49128874993, period=-4.55675491533, sf=-1.64657662932)]), SumKernel(operands=[NoiseKernel(sf=3.27574333331), SqExpKernel(dimension=0, lengthscale=2.0970290968, sf=4.82089637603)])]), likelihood=LikGauss(sf=-inf), nll=4464.08391022, ndata=893)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[NoiseKernel(sf=0.41081593658), SqExpKernel(dimension=0, lengthscale=2.20553599672, sf=3.25522764906)]), SumKernel(operands=[NoiseKernel(sf=-0.679859636525), ConstKernel(sf=2.64399695371), PeriodicKernel(dimension=0, lengthscale=1.098610386, period=-3.95474878538, sf=0.76609073816)])]), likelihood=LikGauss(sf=-inf), nll=4170.07444964, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-2.44303495018), PeriodicKernel(dimension=0, lengthscale=0.545792150907, period=-4.35370889642, sf=-3.61091823449)]), SumKernel(operands=[ConstKernel(sf=3.42004444254), PeriodicKernel(dimension=0, lengthscale=-0.639143699848, period=-3.95461306487, sf=2.0248779928)]), SumKernel(operands=[NoiseKernel(sf=1.97061198266), SqExpKernel(dimension=0, lengthscale=3.5808566569, sf=4.75211683822)])]), likelihood=LikGauss(sf=-inf), nll=4163.4877927, ndata=893)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[NoiseKernel(sf=0.507532285602), SqExpKernel(dimension=0, lengthscale=2.47554941444, sf=3.17635772041)]), SumKernel(operands=[NoiseKernel(sf=-0.874024948837), ConstKernel(sf=2.61190326034), PeriodicKernel(dimension=0, lengthscale=0.918768831907, period=-3.95473326286, sf=0.801129398717)])]), likelihood=LikGauss(sf=-inf), nll=4170.54014346, ndata=893)
