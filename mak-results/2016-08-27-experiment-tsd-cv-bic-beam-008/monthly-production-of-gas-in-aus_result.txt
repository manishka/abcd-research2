Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-beam-008,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-beam-008/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 41,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.14014852155), SqExpKernel(dimension=0, lengthscale=-0.756136155054, sf=10.0017566381)]), likelihood=LikGauss(sf=-inf), nll=3947.71569426, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26257745304), SqExpKernel(dimension=0, lengthscale=2.73987782279, sf=10.1505297909)]), likelihood=LikGauss(sf=-inf), nll=4159.40859157, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26256389675), SqExpKernel(dimension=0, lengthscale=2.74506645765, sf=10.1688160249)]), likelihood=LikGauss(sf=-inf), nll=4159.40983773, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.78053842846, sf=9.92324500019), ProductKernel(operands=[NoiseKernel(sf=-0.379224069598), SumKernel(operands=[ConstKernel(sf=4.39797745811), LinearKernel(dimension=0, location=1959.18636289, sf=4.46205446075)])])]), likelihood=LikGauss(sf=-inf), nll=3705.38505699, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.72262672468, sf=10.0419749406), ProductKernel(operands=[NoiseKernel(sf=4.66904322671), LinearKernel(dimension=0, location=1955.61705254, sf=-0.810456926057)])]), likelihood=LikGauss(sf=-inf), nll=3736.04070849, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.740841776018, sf=10.030127017), ProductKernel(operands=[NoiseKernel(sf=4.95346321596), LinearKernel(dimension=0, location=1955.78408107, sf=-1.06879217709)])]), likelihood=LikGauss(sf=-inf), nll=3738.29259862, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[ProductKernel(operands=[NoiseKernel(sf=5.05937225819), LinearKernel(dimension=0, location=1955.10199064, sf=-1.13662686387)]), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.63213533276, sf=2.29980269822), SumKernel(operands=[ConstKernel(sf=8.20850822858), PeriodicKernel(dimension=0, lengthscale=3.75694635869, period=-0.00929796104542, sf=5.57944463297)])])]), likelihood=LikGauss(sf=-inf), nll=3540.84855781, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[ProductKernel(operands=[NoiseKernel(sf=-0.539572484527), SumKernel(operands=[ConstKernel(sf=3.95730688152), LinearKernel(dimension=0, location=1959.54375599, sf=4.63583310244)])]), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.89124585727, sf=2.17068630523), SumKernel(operands=[ConstKernel(sf=9.99190959213), PeriodicKernel(dimension=0, lengthscale=-0.425599854058, period=1.09913527451, sf=5.98350713585)])])]), likelihood=LikGauss(sf=-inf), nll=3580.12734644, ndata=428)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[ProductKernel(operands=[NoiseKernel(sf=-0.436962760367), SumKernel(operands=[ConstKernel(sf=4.17586656899), LinearKernel(dimension=0, location=1959.20615899, sf=4.87053533682)])]), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.53144388325, sf=2.87149381356), SumKernel(operands=[ConstKernel(sf=8.38460231546), PeriodicKernel(dimension=0, lengthscale=0.981411349546, period=0.00128427570492, sf=5.04660358094)])])]), likelihood=LikGauss(sf=-inf), nll=3587.27822907, ndata=428)
