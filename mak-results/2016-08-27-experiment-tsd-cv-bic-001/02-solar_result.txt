Experiment all_results for
 datafile = ../data/tsdlr_9010/02-solar.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 52,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.56249963149), SqExpKernel(dimension=0, lengthscale=6.22856575747, sf=7.0477129114)]), likelihood=LikGauss(sf=-inf), nll=-12.7248983039, ndata=361)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.79158550103), SqExpKernel(dimension=0, lengthscale=0.748171008404, sf=-1.55233218298), SqExpKernel(dimension=0, lengthscale=6.54789264089, sf=7.25102568155)]), likelihood=LikGauss(sf=-inf), nll=-229.747020951, ndata=361)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.80170656329), SqExpKernel(dimension=0, lengthscale=0.693989438796, sf=-1.63655232593), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=13.6420112632, sf=3.87274315156), SumKernel(operands=[ConstKernel(sf=3.34312066735), SqExpKernel(dimension=0, lengthscale=3.29630944993, sf=-5.2848287783)])])]), likelihood=LikGauss(sf=-inf), nll=-246.025525945, ndata=361)
