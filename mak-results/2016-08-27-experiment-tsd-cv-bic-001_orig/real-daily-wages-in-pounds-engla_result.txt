Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 52,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370986256021), SqExpKernel(dimension=0, lengthscale=3.43985656546, sf=1.73737437871)]), likelihood=LikGauss(sf=-inf), nll=777.525325331, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32899826911), SqExpKernel(dimension=0, lengthscale=0.405482451579, sf=-0.194120791539), SqExpKernel(dimension=0, lengthscale=4.42835748326, sf=2.08929438623)]), likelihood=LikGauss(sf=-inf), nll=589.219374285, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.31592468274), SqExpKernel(dimension=0, lengthscale=4.45785443966, sf=2.04859428324), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.406952972999, sf=-2.07946859202), SumKernel(operands=[ConstKernel(sf=1.19602253244), LinearKernel(dimension=0, location=1640.17521043, sf=-3.4825313665)])])]), likelihood=LikGauss(sf=-inf), nll=564.116175169, ndata=661)
