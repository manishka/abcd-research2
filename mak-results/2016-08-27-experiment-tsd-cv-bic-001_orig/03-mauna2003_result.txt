Experiment all_results for
 datafile = ../data/tsdlr_9010/03-mauna2003.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 52,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.757140422205), SqExpKernel(dimension=0, lengthscale=3.82469568374, sf=3.66206527506)]), likelihood=LikGauss(sf=-inf), nll=1080.30974382, ndata=490)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.04942677814), PeriodicKernel(dimension=0, lengthscale=2.85770025779, period=-0.000364774212642, sf=-0.0758631322661)]), SumKernel(operands=[NoiseKernel(sf=-2.63527956864), SqExpKernel(dimension=0, lengthscale=3.66788377652, sf=1.6936668328)])]), likelihood=LikGauss(sf=-inf), nll=462.621622693, ndata=490)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.74177783757), PeriodicKernel(dimension=0, lengthscale=2.20043262089, period=-0.000302943704238, sf=0.400902118584)]), SumKernel(operands=[NoiseKernel(sf=-3.32052359866), SqExpKernel(dimension=0, lengthscale=-0.78981421186, sf=-2.51016309634), SqExpKernel(dimension=0, lengthscale=3.98213592081, sf=2.21389146525)])]), likelihood=LikGauss(sf=-inf), nll=146.990513993, ndata=490)
