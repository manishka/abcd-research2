Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 52,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.26259126829), SqExpKernel(dimension=0, lengthscale=2.74002597908, sf=10.150684746)]), likelihood=LikGauss(sf=-inf), nll=4159.40859193, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.35804784643, sf=9.96283279423), ProductKernel(operands=[NoiseKernel(sf=-0.0188991897823), SumKernel(operands=[ConstKernel(sf=5.64725504567), LinearKernel(dimension=0, location=1959.91416614, sf=5.22510330107)])])]), likelihood=LikGauss(sf=-inf), nll=3874.81525657, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=5.82983222021), PeriodicKernel(dimension=0, lengthscale=2.69856628806, period=1.07071116779e-05, sf=2.88936180058)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.62391597398, sf=6.11927003021), ProductKernel(operands=[NoiseKernel(sf=-4.76528097571), SumKernel(operands=[ConstKernel(sf=2.64028022522), LinearKernel(dimension=0, location=1959.97520355, sf=3.44483081899)])])])]), likelihood=LikGauss(sf=-inf), nll=3591.69761528, ndata=428)
