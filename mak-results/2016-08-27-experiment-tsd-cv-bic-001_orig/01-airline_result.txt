Experiment all_results for
 datafile = ../data/tsdlr_9010/01-airline.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 52,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.73362902322), LinearKernel(dimension=0, location=1946.07064082, sf=3.44311483154)]), likelihood=LikGauss(sf=-inf), nll=669.003096808, ndata=129)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.610713989364), ProductKernel(operands=[LinearKernel(dimension=0, location=1945.47775754, sf=1.00454497847), SumKernel(operands=[ConstKernel(sf=2.35878097537), SqExpKernel(dimension=0, lengthscale=-2.43784476004, sf=0.362104935486)])])]), likelihood=LikGauss(sf=-inf), nll=587.109152057, ndata=129)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.64241902226), PeriodicKernel(dimension=0, lengthscale=0.282131309932, period=0.000810167024206, sf=1.98973144292)]), SumKernel(operands=[NoiseKernel(sf=-1.23139301205), ProductKernel(operands=[LinearKernel(dimension=0, location=1945.31545394, sf=-0.717770157485), SumKernel(operands=[ConstKernel(sf=2.07866986109), SqExpKernel(dimension=0, lengthscale=-0.961093206692, sf=-1.26604531026)])])])]), likelihood=LikGauss(sf=-inf), nll=505.940345481, ndata=129)
