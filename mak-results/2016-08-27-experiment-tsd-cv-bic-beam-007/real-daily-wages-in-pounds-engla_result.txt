Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-beam-007,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-beam-007/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 50,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370886791524), SqExpKernel(dimension=0, lengthscale=3.44060572444, sf=1.73739911994)]), likelihood=LikGauss(sf=-inf), nll=777.525292846, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.371053799462), SqExpKernel(dimension=0, lengthscale=3.43894016927, sf=1.7304666964)]), likelihood=LikGauss(sf=-inf), nll=777.52634038, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.368776799467), SqExpKernel(dimension=0, lengthscale=3.43930074411, sf=1.73429427902)]), likelihood=LikGauss(sf=-inf), nll=777.528583866, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.33362547333), SqExpKernel(dimension=0, lengthscale=0.39823120592, sf=-0.19789459627), SqExpKernel(dimension=0, lengthscale=4.40823335103, sf=1.98180733698)]), likelihood=LikGauss(sf=-inf), nll=589.158479497, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.33456405854), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.39734603712, sf=-0.276299862216), SumKernel(operands=[ConstKernel(sf=2.21519050337), SqExpKernel(dimension=0, lengthscale=0.397758694397, sf=0.0773424190133)])])]), likelihood=LikGauss(sf=-inf), nll=589.184589677, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32717433157), SqExpKernel(dimension=0, lengthscale=0.414915759587, sf=-0.193307574886), SqExpKernel(dimension=0, lengthscale=4.41544363098, sf=1.98907725731)]), likelihood=LikGauss(sf=-inf), nll=589.22510192, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32245817839), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.279525930691), LinearKernel(dimension=0, location=1634.47643384, sf=-4.83783300541)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.403595980244, sf=-0.686718933096), SqExpKernel(dimension=0, lengthscale=4.45824939615, sf=1.40201125758)])])]), likelihood=LikGauss(sf=-inf), nll=562.877169331, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32600203834), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.50605331332), LinearKernel(dimension=0, location=1625.08974714, sf=-4.21172405396)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.39681024403, sf=-1.37043500376), SqExpKernel(dimension=0, lengthscale=4.46492494075, sf=0.718838925085)])])]), likelihood=LikGauss(sf=-inf), nll=563.612088585, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.30268043908), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.614997210852), LinearKernel(dimension=0, location=1626.37999351, sf=-3.96769106483)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.415509252635, sf=-1.56202499764), SqExpKernel(dimension=0, lengthscale=4.44395508387, sf=0.440401904161)])])]), likelihood=LikGauss(sf=-inf), nll=563.64102183, ndata=661)
