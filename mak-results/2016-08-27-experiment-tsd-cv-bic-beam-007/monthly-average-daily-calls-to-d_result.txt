Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-average-daily-calls-to-d.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-beam-007,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-beam-007/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 50,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64809809633), SqExpKernel(dimension=0, lengthscale=-0.106469232955, sf=6.01195331427)]), likelihood=LikGauss(sf=-inf), nll=876.109863546, ndata=162)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64755483182), SqExpKernel(dimension=0, lengthscale=-0.106719067136, sf=6.01269632127)]), likelihood=LikGauss(sf=-inf), nll=876.109885424, ndata=162)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64729400183), SqExpKernel(dimension=0, lengthscale=-0.1070359218, sf=6.01272718894)]), likelihood=LikGauss(sf=-inf), nll=876.109903817, ndata=162)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.97342701862), LinearKernel(dimension=0, location=1964.17281006, sf=0.952995356357)]), SumKernel(operands=[NoiseKernel(sf=0.807201882046), SqExpKernel(dimension=0, lengthscale=0.147086004658, sf=3.39754201682)])]), likelihood=LikGauss(sf=-inf), nll=835.880624417, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.38160929884), LinearKernel(dimension=0, location=1964.26681437, sf=0.370977348305)]), SumKernel(operands=[NoiseKernel(sf=1.40108093385), SqExpKernel(dimension=0, lengthscale=0.147721436643, sf=3.99738532798)])]), likelihood=LikGauss(sf=-inf), nll=835.887234782, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.89061147056), LinearKernel(dimension=0, location=1964.08738156, sf=1.85674189852)]), SumKernel(operands=[NoiseKernel(sf=-0.110518336579), SqExpKernel(dimension=0, lengthscale=0.14408756265, sf=2.48590030454)])]), likelihood=LikGauss(sf=-inf), nll=835.889893055, ndata=162)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.691346676819), LinearKernel(dimension=0, location=1964.15221461, sf=-0.39608394956)]), SumKernel(operands=[ConstKernel(sf=1.08124082751), PeriodicKernel(dimension=0, lengthscale=-1.08273919416, period=0.69786674274, sf=-1.46940322176)]), SumKernel(operands=[NoiseKernel(sf=0.270689496695), SqExpKernel(dimension=0, lengthscale=0.465301677335, sf=3.5837425434)])]), likelihood=LikGauss(sf=-inf), nll=797.443595487, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.17930592914), LinearKernel(dimension=0, location=1964.21663609, sf=-0.893127145765)]), SumKernel(operands=[ConstKernel(sf=1.99766487291), PeriodicKernel(dimension=0, lengthscale=-1.12722687096, period=0.696511563509, sf=-0.3734476844)]), SumKernel(operands=[NoiseKernel(sf=-0.155964957538), SqExpKernel(dimension=0, lengthscale=0.420811420771, sf=3.10587410102)])]), likelihood=LikGauss(sf=-inf), nll=797.466302902, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.62504617275), ProductKernel(operands=[LinearKernel(dimension=0, location=1964.70889805, sf=-0.848330007156), SumKernel(operands=[ConstKernel(sf=1.89415547151), SqExpKernel(dimension=0, lengthscale=-1.71809097403, sf=0.197662182603)])])]), SumKernel(operands=[NoiseKernel(sf=0.00838542531905), SqExpKernel(dimension=0, lengthscale=0.990938617604, sf=3.036518102)])]), likelihood=LikGauss(sf=-inf), nll=800.244809129, ndata=162)
