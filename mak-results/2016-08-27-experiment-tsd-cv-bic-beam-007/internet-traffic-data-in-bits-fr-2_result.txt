Experiment all_results for
 datafile = ../data/tsdlr_9010/internet-traffic-data-in-bits-fr-2.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-beam-007,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-beam-007/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 50,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=9.79311066432), SqExpKernel(dimension=0, lengthscale=-3.40483346136, sf=10.6992686431)]), likelihood=LikGauss(sf=-inf), nll=10212.2036782, ndata=909)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=9.79774675517), SqExpKernel(dimension=0, lengthscale=-3.36735127291, sf=10.6186897981)]), likelihood=LikGauss(sf=-inf), nll=10212.315023, ndata=909)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=9.79847755691), SqExpKernel(dimension=0, lengthscale=-3.30401054543, sf=11.9163828376)]), likelihood=LikGauss(sf=-inf), nll=10215.9991643, ndata=909)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.70201680422), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-4.48578702491, sf=9.52584480252), SumKernel(operands=[ConstKernel(sf=0.921911814246), PeriodicKernel(dimension=0, lengthscale=-1.63528815457, period=-4.28866795647, sf=0.239375281672)])])]), likelihood=LikGauss(sf=-inf), nll=9123.84479273, ndata=909)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.78272843872), SqExpKernel(dimension=0, lengthscale=-2.82498421191, sf=10.7599572472), SqExpKernel(dimension=0, lengthscale=-7.63591584852, sf=9.88669697499)]), likelihood=LikGauss(sf=-inf), nll=9162.739998, ndata=909)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.78364556766), SqExpKernel(dimension=0, lengthscale=-2.81980256545, sf=10.7627105046), SqExpKernel(dimension=0, lengthscale=-7.63500779743, sf=9.88663384455)]), likelihood=LikGauss(sf=-inf), nll=9162.74507793, ndata=909)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.42714619529), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-5.71743912997, sf=10.6322786442), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-2.29035458297, sf=-0.107471283664), PeriodicKernel(dimension=0, lengthscale=0.012847439673, period=-5.89172894122, sf=-0.957218889156)])])]), likelihood=LikGauss(sf=-inf), nll=8994.49994916, ndata=909)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.37450341782), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-3.77343328581, sf=7.07441383924), SumKernel(operands=[ConstKernel(sf=3.12782323), ProductKernel(operands=[PeriodicKernel(dimension=0, lengthscale=-2.05488285537, period=-3.95436628911, sf=-4.29554951967), SumKernel(operands=[NoiseKernel(sf=4.61348215552), ConstKernel(sf=6.93977049145)])])])])]), likelihood=LikGauss(sf=-inf), nll=9010.61809122, ndata=909)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=7.80931554123), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=-3.79836956834, sf=10.5838364403), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.21652594716, sf=0.059894104094), PeriodicKernel(dimension=0, lengthscale=-1.79355411136, period=-3.95339481508, sf=-0.762257540949)])])]), likelihood=LikGauss(sf=-inf), nll=9014.29844363, ndata=909)
