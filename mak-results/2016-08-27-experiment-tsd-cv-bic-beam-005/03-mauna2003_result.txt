Experiment all_results for
 datafile = ../data/tsdlr_9010/03-mauna2003.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-bic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-bic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 71,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.41576016996), SqExpKernel(dimension=0, lengthscale=-0.712821964479, sf=2.69998530495)]), likelihood=LikGauss(sf=-inf), nll=836.410794262, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.756670166008), SqExpKernel(dimension=0, lengthscale=3.81506424169, sf=3.6326630658)]), likelihood=LikGauss(sf=-inf), nll=1080.30826861, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.756366023588), SqExpKernel(dimension=0, lengthscale=3.81541107441, sf=3.63511786855)]), likelihood=LikGauss(sf=-inf), nll=1080.30831585, ndata=490)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.48211703106), PeriodicKernel(dimension=0, lengthscale=-0.810242313845, period=1.098089131, sf=-1.88948614572)]), SumKernel(operands=[NoiseKernel(sf=-2.70034660536), SqExpKernel(dimension=0, lengthscale=1.99628181576, sf=2.53124511272)])]), likelihood=LikGauss(sf=-inf), nll=364.156015896, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.812979698548), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=3.06444089925, sf=2.30633852879), SumKernel(operands=[ConstKernel(sf=4.58549391056), PeriodicKernel(dimension=0, lengthscale=4.26792407178, period=-8.82960868658e-05, sf=1.07412086888)])])]), likelihood=LikGauss(sf=-inf), nll=407.4083478, ndata=490)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.89653138356), PeriodicKernel(dimension=0, lengthscale=1.39340896093, period=-0.000337222374727, sf=-1.24014767226)]), SumKernel(operands=[NoiseKernel(sf=-3.47988075477), SqExpKernel(dimension=0, lengthscale=4.19853928342, sf=1.69633435125)])]), likelihood=LikGauss(sf=-inf), nll=450.112405976, ndata=490)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.48167004172), SqExpKernel(dimension=0, lengthscale=0.204744769458, sf=-2.21386272597), PeriodicKernel(dimension=0, lengthscale=1.07314667069, period=-0.000167318388198, sf=-1.09197065893)]), SumKernel(operands=[NoiseKernel(sf=-3.812143577), SqExpKernel(dimension=0, lengthscale=4.41595101683, sf=2.41747065196)])]), likelihood=LikGauss(sf=-inf), nll=153.754038575, ndata=490)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.39502497669), PeriodicKernel(dimension=0, lengthscale=-0.927504153228, period=1.09823254996, sf=-1.8969146996)]), SumKernel(operands=[NoiseKernel(sf=-2.86702155519), SqExpKernel(dimension=0, lengthscale=-0.109034308402, sf=-1.86928735429), SqExpKernel(dimension=0, lengthscale=3.87209950747, sf=2.27082991761)])]), likelihood=LikGauss(sf=-inf), nll=170.754640652, ndata=490)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.05615788039), PeriodicKernel(dimension=0, lengthscale=-0.998017566053, period=1.09899660632, sf=-1.80125676503)]), SumKernel(operands=[NoiseKernel(sf=-2.64264720069), SqExpKernel(dimension=0, lengthscale=0.284026226124, sf=-0.328988690917), SqExpKernel(dimension=0, lengthscale=3.92747424579, sf=2.32660377441)])]), likelihood=LikGauss(sf=-inf), nll=173.183383852, ndata=490)
