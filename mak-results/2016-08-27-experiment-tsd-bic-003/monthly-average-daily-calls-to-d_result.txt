Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-average-daily-calls-to-d.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-003,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 1,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-003/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 80,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.6476416523), SqExpKernel(dimension=0, lengthscale=-0.107292831927, sf=6.00965808956)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.0603505427347, sf=6.0211297857), ProductKernel(operands=[NoiseKernel(sf=0.967462363168), LinearKernel(dimension=0, location=1958.81927141, sf=0.293270481678)])]), likelihood=LikGauss(sf=-inf), nll=843.486319014, ndata=162)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.763110176411, sf=4.7398831784), ProductKernel(operands=[LinearKernel(dimension=0, location=1959.41576739, sf=-0.239795935878), SumKernel(operands=[NoiseKernel(sf=1.39023337116), LinearKernel(dimension=0, location=1978.70996291, sf=2.10995836778)])])]), likelihood=LikGauss(sf=-inf), nll=825.108626056, ndata=162)
