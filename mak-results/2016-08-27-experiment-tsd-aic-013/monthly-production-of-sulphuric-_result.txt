Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-sulphuric-.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-013,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-013/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 89,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.01531870214), SqExpKernel(dimension=0, lengthscale=0.548439103828, sf=4.64779016403)]), likelihood=LikGauss(sf=-inf), nll=1902.27999352, ndata=415)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.40556208346), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.634857802958, sf=4.75966413453), SumKernel(operands=[ConstKernel(sf=-0.0935386573458), PeriodicKernel(dimension=0, lengthscale=-0.901157431762, period=0.694234904405, sf=-1.94497500889)])])]), likelihood=LikGauss(sf=-inf), nll=1792.69805357, ndata=415)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1939.7561217, sf=-1.42979496266), SumKernel(operands=[NoiseKernel(sf=0.282312851543), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.628661910852, sf=3.39250225751), SumKernel(operands=[ConstKernel(sf=-0.832670959953), PeriodicKernel(dimension=0, lengthscale=-0.922806308095, period=0.698240539214, sf=-2.68666833882)])])])]), likelihood=LikGauss(sf=-inf), nll=1759.26105132, ndata=415)
