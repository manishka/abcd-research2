Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-gas-in-aus.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-013,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-013/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 89,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=8.2625491896), SqExpKernel(dimension=0, lengthscale=2.7398142875, sf=10.149772022)]), likelihood=LikGauss(sf=-inf), nll=4159.40858922, ndata=428)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.96522567736), LinearKernel(dimension=0, location=1959.98605758, sf=1.42707515305)]), SumKernel(operands=[NoiseKernel(sf=3.73522466035), SqExpKernel(dimension=0, lengthscale=1.55033846393, sf=5.43250140046)])]), likelihood=LikGauss(sf=-inf), nll=3860.49310675, ndata=428)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.279470227984), LinearKernel(dimension=0, location=1963.5159553, sf=-0.177591943502)]), SumKernel(operands=[ConstKernel(sf=3.07044347471), PeriodicKernel(dimension=0, lengthscale=-0.839688484173, period=0.686114246335, sf=0.633796823929)]), SumKernel(operands=[NoiseKernel(sf=1.43001738503), SqExpKernel(dimension=0, lengthscale=1.60356597899, sf=4.41054842598)])]), likelihood=LikGauss(sf=-inf), nll=3516.669513, ndata=428)
