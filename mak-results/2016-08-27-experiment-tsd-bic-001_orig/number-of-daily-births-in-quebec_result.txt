Experiment all_results for
 datafile = ../data/tsdlr_9010/number-of-daily-births-in-quebec.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 57,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67595364253), SqExpKernel(dimension=0, lengthscale=2.07594134704, sf=5.62554694106)]), likelihood=LikGauss(sf=-inf), nll=4565.13586365, ndata=893)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.61862874817), SqExpKernel(dimension=0, lengthscale=-1.843580824, sf=2.72841604871), SqExpKernel(dimension=0, lengthscale=4.96467388193, sf=5.64272229555)]), likelihood=LikGauss(sf=-inf), nll=4540.29608816, ndata=893)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.198652044), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.03326280843), PeriodicKernel(dimension=0, lengthscale=1.79998669945, period=-3.95471739497, sf=1.48425808603)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.785558040863, sf=-1.23957083643), SqExpKernel(dimension=0, lengthscale=5.20498146275, sf=5.19299589152)])])]), likelihood=LikGauss(sf=-inf), nll=4183.37772641, ndata=893)
