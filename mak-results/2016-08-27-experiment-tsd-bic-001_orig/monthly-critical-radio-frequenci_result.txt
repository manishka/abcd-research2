Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-critical-radio-frequenci.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 57,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.545683164452), SqExpKernel(dimension=0, lengthscale=1.29979706231, sf=1.85750429699)]), likelihood=LikGauss(sf=-inf), nll=441.900121469, ndata=216)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.571349694858), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.04726364423, sf=0.782433389631), SumKernel(operands=[ConstKernel(sf=1.18967236916), PeriodicKernel(dimension=0, lengthscale=0.186320435786, period=0.00196317588106, sf=-0.422239481141)])])]), likelihood=LikGauss(sf=-inf), nll=265.210685025, ndata=216)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.11517972329), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.16780864495, sf=0.638781310203), SumKernel(operands=[ConstKernel(sf=1.23413892362), SqExpKernel(dimension=0, lengthscale=-2.0853256101, sf=-1.20841618317), PeriodicKernel(dimension=0, lengthscale=0.109691338736, period=0.000175919220589, sf=-0.332623085767)])])]), likelihood=LikGauss(sf=-inf), nll=230.058926618, ndata=216)
