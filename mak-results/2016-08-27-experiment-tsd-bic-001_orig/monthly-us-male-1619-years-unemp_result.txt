Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-us-male-1619-years-unemp.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-001,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-001/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 57,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=4.77824479387), SqExpKernel(dimension=0, lengthscale=1.97258972674, sf=6.15043491939)]), likelihood=LikGauss(sf=-inf), nll=2292.61995734, ndata=367)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=4.54388310519), SqExpKernel(dimension=0, lengthscale=0.872842468641, sf=6.25248706843), PeriodicKernel(dimension=0, lengthscale=4.41355497404, period=-0.69302944579, sf=5.04701859827)]), likelihood=LikGauss(sf=-inf), nll=2234.47915443, ndata=367)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.778410379666, sf=6.34659045768), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.93867981423), PeriodicKernel(dimension=0, lengthscale=-2.57165619453, period=0.689730016563, sf=-1.31018697754)]), SumKernel(operands=[NoiseKernel(sf=0.658243020004), PeriodicKernel(dimension=0, lengthscale=4.47117177803, period=-0.691404733863, sf=5.23931251346)])])]), likelihood=LikGauss(sf=-inf), nll=2071.96547317, ndata=367)
