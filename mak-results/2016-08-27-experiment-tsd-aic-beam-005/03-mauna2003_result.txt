Experiment all_results for
 datafile = ../data/tsdlr_9010/03-mauna2003.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 95,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.415931570306), SqExpKernel(dimension=0, lengthscale=-0.713068631376, sf=2.70042748582)]), likelihood=LikGauss(sf=-inf), nll=836.41098588, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.410351034878), SqExpKernel(dimension=0, lengthscale=-0.72274655307, sf=2.64989834136)]), likelihood=LikGauss(sf=-inf), nll=836.570719732, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.756811543859), SqExpKernel(dimension=0, lengthscale=3.81491219377, sf=3.6357848591)]), likelihood=LikGauss(sf=-inf), nll=1080.30821373, ndata=490)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.316775162888), PeriodicKernel(dimension=0, lengthscale=-1.56434504234, period=1.79088171225, sf=-1.06557945214)]), SumKernel(operands=[NoiseKernel(sf=-1.95000304896), SqExpKernel(dimension=0, lengthscale=2.12997934159, sf=1.69892651324)])]), likelihood=LikGauss(sf=-inf), nll=362.88900324, ndata=490)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.77969947878), PeriodicKernel(dimension=0, lengthscale=1.42325264094, period=0.00223651451467, sf=0.296210270107)]), SumKernel(operands=[NoiseKernel(sf=-2.73262259772), SqExpKernel(dimension=0, lengthscale=1.93536383051, sf=0.803280121829)])]), likelihood=LikGauss(sf=-inf), nll=364.45839799, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.30462356664), SqExpKernel(dimension=0, lengthscale=0.319692042839, sf=2.40794100508), PeriodicKernel(dimension=0, lengthscale=-1.85478619814, period=1.60903698729, sf=-0.27908052818)]), likelihood=LikGauss(sf=-inf), nll=379.299697552, ndata=490)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.50014117566, sf=-0.830640909746), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.7494684176), PeriodicKernel(dimension=0, lengthscale=1.13658126224, period=0.000371612888275, sf=0.190218101069)]), SumKernel(operands=[NoiseKernel(sf=-3.22416684081), SqExpKernel(dimension=0, lengthscale=3.13756755193, sf=0.93122048622)])])]), likelihood=LikGauss(sf=-inf), nll=151.088446861, ndata=490)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=-0.47298076399, sf=-0.889703847475), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.63368631212), PeriodicKernel(dimension=0, lengthscale=1.29172548924, period=-0.000352624719335, sf=0.0965456090491)]), SumKernel(operands=[NoiseKernel(sf=-3.0836558154), SqExpKernel(dimension=0, lengthscale=2.81857881702, sf=0.961326891468)])])]), likelihood=LikGauss(sf=-inf), nll=161.625941696, ndata=490)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.42841849111), PeriodicKernel(dimension=0, lengthscale=1.09484441039, period=0.000823799134802, sf=-0.341565943527)]), SumKernel(operands=[NoiseKernel(sf=-2.92170190806), SqExpKernel(dimension=0, lengthscale=0.324855896041, sf=-2.10928478182), SqExpKernel(dimension=0, lengthscale=3.13458029992, sf=1.13608635569)])]), likelihood=LikGauss(sf=-inf), nll=184.600794833, ndata=490)
