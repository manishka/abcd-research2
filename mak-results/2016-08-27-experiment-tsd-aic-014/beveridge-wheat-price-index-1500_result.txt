Experiment all_results for
 datafile = ../data/tsdlr_9010/beveridge-wheat-price-index-1500.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-014,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-014/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 76,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.86974630993), SqExpKernel(dimension=0, lengthscale=1.86941124297, sf=4.50618008738)]), likelihood=LikGauss(sf=-inf), nll=1548.85601973, ndata=333)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.964800398045), LinearKernel(dimension=0, location=1496.0902016, sf=-2.21609934579)]), SumKernel(operands=[NoiseKernel(sf=0.0862903461864), SqExpKernel(dimension=0, lengthscale=3.39908968022, sf=1.48065888524)])]), likelihood=LikGauss(sf=-inf), nll=1445.94164871, ndata=333)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.0255660058203), LinearKernel(dimension=0, location=1494.26945787, sf=-3.18863019646)]), SumKernel(operands=[NoiseKernel(sf=0.058338684324), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.51851012628, sf=-0.410987446435), SumKernel(operands=[ConstKernel(sf=3.07433185508), SqExpKernel(dimension=0, lengthscale=0.195760312103, sf=1.51050431288)])])])]), likelihood=LikGauss(sf=-inf), nll=1361.71112002, ndata=333)
