Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-014,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-014/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 76,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370021243756), SqExpKernel(dimension=0, lengthscale=3.4426179873, sf=1.73350216023)]), likelihood=LikGauss(sf=-inf), nll=777.527376444, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.31271933708), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.46086549887, sf=0.485781008808), SumKernel(operands=[ConstKernel(sf=1.48232127289), SqExpKernel(dimension=0, lengthscale=0.382960507387, sf=-0.733410145979)])])]), likelihood=LikGauss(sf=-inf), nll=590.17396262, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32620606106), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.49145551828, sf=-1.34143399976), SumKernel(operands=[ConstKernel(sf=2.00822150036), LinearKernel(dimension=0, location=1655.85092343, sf=-2.35224606895)]), SumKernel(operands=[ConstKernel(sf=0.468580282888), SqExpKernel(dimension=0, lengthscale=0.415214770202, sf=-1.76728673205)])])]), likelihood=LikGauss(sf=-inf), nll=563.802357355, ndata=661)
