Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-004,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-004/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 11,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.369826525868), SqExpKernel(dimension=0, lengthscale=3.44418640046, sf=1.76154836126)]), likelihood=LikGauss(sf=-inf), nll=777.536773652, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.33315439382), SqExpKernel(dimension=0, lengthscale=0.41205554591, sf=-0.187837829569), SqExpKernel(dimension=0, lengthscale=4.42332900743, sf=1.99762443497)]), likelihood=LikGauss(sf=-inf), nll=589.22426288, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.33430245162), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.699020548645), LinearKernel(dimension=0, location=1644.31861378, sf=-4.04004922566)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.411980498336, sf=-1.51671239583), SqExpKernel(dimension=0, lengthscale=4.43881976858, sf=0.558396500066)])])]), likelihood=LikGauss(sf=-inf), nll=563.723326899, ndata=661)
