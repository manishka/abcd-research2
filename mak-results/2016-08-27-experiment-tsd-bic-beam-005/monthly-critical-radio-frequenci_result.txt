Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-critical-radio-frequenci.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 64,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.545683136976), SqExpKernel(dimension=0, lengthscale=1.29979694629, sf=1.85750384554)]), likelihood=LikGauss(sf=-inf), nll=441.900121469, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.545683159995), SqExpKernel(dimension=0, lengthscale=1.29979713346, sf=1.85750515833)]), likelihood=LikGauss(sf=-inf), nll=441.900121469, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.545683129505), SqExpKernel(dimension=0, lengthscale=1.29979588571, sf=1.85750468829)]), likelihood=LikGauss(sf=-inf), nll=441.900121469, ndata=216)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.996442872899), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.929892531393, sf=1.53022780734), SumKernel(operands=[ConstKernel(sf=0.136558870797), PeriodicKernel(dimension=0, lengthscale=-0.780071687894, period=0.696749890461, sf=-1.0930429237)])])]), likelihood=LikGauss(sf=-inf), nll=260.519899748, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.962131853187), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.968685783738, sf=0.135940358573), SumKernel(operands=[ConstKernel(sf=1.8996660212), PeriodicKernel(dimension=0, lengthscale=-0.768310429484, period=0.695100969339, sf=0.249800643439)])])]), likelihood=LikGauss(sf=-inf), nll=261.619446421, ndata=216)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.158106147672), PeriodicKernel(dimension=0, lengthscale=-0.804938795625, period=0.696660815372, sf=-1.91188708354)]), SumKernel(operands=[NoiseKernel(sf=-0.85968829302), SqExpKernel(dimension=0, lengthscale=0.938008514446, sf=2.28163893988)])]), likelihood=LikGauss(sf=-inf), nll=262.1395606, ndata=216)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.05250995659), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.09989734716, sf=0.364410457982), SumKernel(operands=[ConstKernel(sf=2.0482877883), PeriodicKernel(dimension=0, lengthscale=0.115633356238, period=-0.00190358466129, sf=-0.179710159437), PeriodicKernel(dimension=0, lengthscale=-1.2401416053, period=0.986804023936, sf=-1.00124339392)])])]), likelihood=LikGauss(sf=-inf), nll=233.507062011, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.12346829775), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.173418992506), PeriodicKernel(dimension=0, lengthscale=-0.865394294504, period=0.692427257077, sf=-1.21950247889)]), SumKernel(operands=[ConstKernel(sf=1.63217143757), SqExpKernel(dimension=0, lengthscale=0.393967503869, sf=0.987024151542)])])]), likelihood=LikGauss(sf=-inf), nll=239.775982462, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.05046985424), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.05844821209, sf=0.966880309699), SumKernel(operands=[ConstKernel(sf=0.892663867205), PeriodicKernel(dimension=0, lengthscale=0.084379800612, period=0.000748475520603, sf=-0.711617849635), PeriodicKernel(dimension=0, lengthscale=-1.35135779635, period=1.26108911408, sf=-1.50344407528)])])]), likelihood=LikGauss(sf=-inf), nll=234.513850954, ndata=216)
