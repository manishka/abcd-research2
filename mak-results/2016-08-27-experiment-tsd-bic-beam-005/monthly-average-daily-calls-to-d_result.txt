Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-average-daily-calls-to-d.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 64,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64764176617), SqExpKernel(dimension=0, lengthscale=-0.107292630633, sf=6.00965886333)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64764173386), SqExpKernel(dimension=0, lengthscale=-0.107292687062, sf=6.00965878036)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64764195976), SqExpKernel(dimension=0, lengthscale=-0.107292631533, sf=6.0096595815)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.54862601715), LinearKernel(dimension=0, location=1964.18479173, sf=1.52716333322)]), SumKernel(operands=[NoiseKernel(sf=0.23258243909), SqExpKernel(dimension=0, lengthscale=0.14737333497, sf=2.8288915858)])]), likelihood=LikGauss(sf=-inf), nll=835.879901683, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.2810860471), LinearKernel(dimension=0, location=1964.18479575, sf=1.25962720076)]), SumKernel(operands=[NoiseKernel(sf=0.500120394436), SqExpKernel(dimension=0, lengthscale=0.147373083687, sf=3.09642634602)])]), likelihood=LikGauss(sf=-inf), nll=835.879901683, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.20671579371), LinearKernel(dimension=0, location=1964.18397398, sf=1.18511502611)]), SumKernel(operands=[NoiseKernel(sf=0.574507526556), SqExpKernel(dimension=0, lengthscale=0.147363557969, sf=3.17076312619)])]), likelihood=LikGauss(sf=-inf), nll=835.879902406, ndata=162)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.67191898812), LinearKernel(dimension=0, location=1964.13672582, sf=0.659503621067)]), SumKernel(operands=[NoiseKernel(sf=0.152926670857), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.450368762451, sf=2.11449399455), SumKernel(operands=[ConstKernel(sf=1.61073085212), PeriodicKernel(dimension=0, lengthscale=-1.2579983873, period=0.693232931689, sf=-0.993918239041)])])])]), likelihood=LikGauss(sf=-inf), nll=796.286246648, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.05869103238), ProductKernel(operands=[LinearKernel(dimension=0, location=1964.73364823, sf=-1.02232393627), SumKernel(operands=[ConstKernel(sf=2.50750465207), SqExpKernel(dimension=0, lengthscale=-1.71537194417, sf=0.814901556883)])])]), SumKernel(operands=[NoiseKernel(sf=-0.425258924159), SqExpKernel(dimension=0, lengthscale=0.989101801057, sf=2.59725387502)])]), likelihood=LikGauss(sf=-inf), nll=800.241164636, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.98241490938), ProductKernel(operands=[LinearKernel(dimension=0, location=1964.73365135, sf=-2.67729656525), SumKernel(operands=[ConstKernel(sf=5.08620219712), SqExpKernel(dimension=0, lengthscale=-1.71537162291, sf=3.39360226264)])])]), SumKernel(operands=[NoiseKernel(sf=-1.34898310209), SqExpKernel(dimension=0, lengthscale=0.989101438851, sf=1.67352736812)])]), likelihood=LikGauss(sf=-inf), nll=800.241164636, ndata=162)
