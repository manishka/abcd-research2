Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-beam-005,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-beam-005/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 64,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.372303731551), SqExpKernel(dimension=0, lengthscale=3.44173293173, sf=1.7443975291)]), likelihood=LikGauss(sf=-inf), nll=777.527340973, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.373052001027), SqExpKernel(dimension=0, lengthscale=3.44633747856, sf=1.77244413372)]), likelihood=LikGauss(sf=-inf), nll=777.551005365, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.368674579191), SqExpKernel(dimension=0, lengthscale=3.43666686091, sf=1.77865336483)]), likelihood=LikGauss(sf=-inf), nll=777.580772443, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.28773296178), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.31047798816, sf=1.96850935965), SumKernel(operands=[ConstKernel(sf=-0.0256905305009), PeriodicKernel(dimension=0, lengthscale=-2.51801594194, period=4.76079869251, sf=-2.19256360475)])])]), likelihood=LikGauss(sf=-inf), nll=585.863062873, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.3335794172), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.40132945779, sf=1.288393749), SumKernel(operands=[ConstKernel(sf=0.662514976866), SqExpKernel(dimension=0, lengthscale=0.400905457891, sf=-1.48840746431)])])]), likelihood=LikGauss(sf=-inf), nll=589.178324972, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.37885314794), SqExpKernel(dimension=0, lengthscale=0.278590048851, sf=-0.321462278571), SqExpKernel(dimension=0, lengthscale=3.57540463078, sf=1.80872240088)]), likelihood=LikGauss(sf=-inf), nll=590.906516618, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.33037941185), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.41980648993, sf=1.268564795), SumKernel(operands=[ConstKernel(sf=0.641279487873), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.409734702429, sf=-1.61251974394), SumKernel(operands=[ConstKernel(sf=-0.676193600905), LinearKernel(dimension=0, location=1646.83792775, sf=-5.15330382934)])])])])]), likelihood=LikGauss(sf=-inf), nll=563.80354869, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.31739638091), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.38969833693), LinearKernel(dimension=0, location=1586.6153787, sf=-5.42019491037)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.388766537013, sf=-0.264786695514), SqExpKernel(dimension=0, lengthscale=4.42691752059, sf=1.68092527956)])])]), likelihood=LikGauss(sf=-inf), nll=568.460390019, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.3329454088), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.15689238878), SqExpKernel(dimension=0, lengthscale=0.348007396828, sf=-1.31848262546)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.42644649649, sf=0.855416344668), LinearKernel(dimension=0, location=1516.79368648, sf=-4.78503794019)])])]), likelihood=LikGauss(sf=-inf), nll=574.85776662, ndata=661)
