Experiment all_results for
 datafile = ../data/tsdlr-renamed/08-radio.mat

 Running experiment:
description = An 2014-01-16-GPSS-full experiment,
data_dir = ../data/tsdlr-renamed/08-radio.mat,
max_depth = 10,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 400,
verbose = False,
make_predictions = False,
skip_complete = True,
results_dir = ../mak-results/2016-05-13-GPSS-full/,
iters = 250,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 3,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 1,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('CP', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('CW', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('B', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('BL', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('None',), {'A': 'kernel'})],
score = BIC,
period_heuristic_type = min,
stopping_criteria = [],
improvement_tolerance = 0.1,
job_time = 00:15:00,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.507932054932), SqExpKernel(dimension=0, lengthscale=1.31367440658, sf=1.84113864318)]), likelihood=LikGauss(sf=-inf), nll=481.647902219, ndata=240)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.0658543481978), PeriodicKernel(dimension=0, lengthscale=0.203549846676, period=0.00181992871009, sf=-1.41856432097)]), SumKernel(operands=[NoiseKernel(sf=-0.703959909017), SqExpKernel(dimension=0, lengthscale=1.04404386962, sf=1.74991497077)])]), likelihood=LikGauss(sf=-inf), nll=283.446979338, ndata=240)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.450340505356), PeriodicKernel(dimension=0, lengthscale=0.0655522888614, period=-0.000737459966383, sf=-1.42209209041)]), SumKernel(operands=[NoiseKernel(sf=-0.650716917907), ConstKernel(sf=1.89616183271), SqExpKernel(dimension=0, lengthscale=-0.0535740119777, sf=1.04126571926)])]), likelihood=LikGauss(sf=-inf), nll=247.017102302, ndata=240)

%%%%% Level 3 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.554991821031), PeriodicKernel(dimension=0, lengthscale=0.268895021729, period=-0.000722692043346, sf=-1.34080501319)]), SumKernel(operands=[ConstKernel(sf=1.91035198426), ChangeWindowKernel(dimension=0, location=1949.51419193, steepness=0.647688967416, width=-0.375561675385, operands=[ SumKernel(operands=[NoiseKernel(sf=-0.503771695846), SqExpKernel(dimension=0, lengthscale=0.254237010116, sf=1.07591283367)]), ConstKernel(sf=2.30664103152) ])])]), likelihood=LikGauss(sf=-inf), nll=234.200320018, ndata=240)

%%%%% Level 4 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-1.66797224639), PeriodicKernel(dimension=0, lengthscale=0.254755472826, period=-0.000730100448849, sf=-2.50231900345)]), SumKernel(operands=[ConstKernel(sf=3.09780570554), ChangeWindowKernel(dimension=0, location=1949.48349713, steepness=0.816732479376, width=0.248143355404, operands=[ SumKernel(operands=[NoiseKernel(sf=0.640062923918), SqExpKernel(dimension=0, lengthscale=0.246851995449, sf=2.24188156301)]), ConstKernel(sf=2.8084355201) ])])]), likelihood=LikGauss(sf=-inf), nll=230.08711421, ndata=240)

%%%%% Level 5 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-1.92602494922), PeriodicKernel(dimension=0, lengthscale=0.233678278609, period=-0.00107068044519, sf=-2.80627755723)]), SumKernel(operands=[ConstKernel(sf=3.34611698581), ChangeWindowKernel(dimension=0, location=1949.49038946, steepness=0.826214048441, width=0.168697877625, operands=[ SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.318946323072, sf=2.49765648588), ProductKernel(operands=[NoiseKernel(sf=-0.641082800755), LinearKernel(dimension=0, location=1970.7750622, sf=-1.72296937034)])]), ConstKernel(sf=3.10564037991) ])])]), likelihood=LikGauss(sf=-inf), nll=226.783432767, ndata=240)

%%%%% Level 6 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-2.04963135792), PeriodicKernel(dimension=0, lengthscale=0.250405336898, period=-0.00107724704899, sf=-2.95281232396)]), SumKernel(operands=[ConstKernel(sf=3.49721381825), ChangeWindowKernel(dimension=0, location=1949.492219, steepness=0.802806154936, width=0.154257972553, operands=[ SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.328394199498, sf=2.65341610551), ProductKernel(operands=[NoiseKernel(sf=-0.215625365791), LinearKernel(dimension=0, location=1970.86989787, sf=-2.02397822523)])]), ConstKernel(sf=3.33897308122) ])])]), likelihood=LikGauss(sf=-inf), nll=226.70051678, ndata=240)

%%%%% Level 7 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-2.06109547982), PeriodicKernel(dimension=0, lengthscale=0.256646571201, period=-0.00107681367457, sf=-2.96166222244)]), SumKernel(operands=[ConstKernel(sf=3.51893764455), ChangeWindowKernel(dimension=0, location=1949.49202139, steepness=0.79724361322, width=0.15220768843, operands=[ SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.326235941135, sf=2.66645676586), ProductKernel(operands=[NoiseKernel(sf=-0.20489235625), LinearKernel(dimension=0, location=1970.90984635, sf=-2.02328780761)])]), ConstKernel(sf=3.36151074054) ])])]), likelihood=LikGauss(sf=-inf), nll=226.698413286, ndata=240)

%%%%% Level 8 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-2.05592884709), PeriodicKernel(dimension=0, lengthscale=0.258044830487, period=-0.00107848491222, sf=-2.9370149809)]), SumKernel(operands=[ConstKernel(sf=3.49507784863), ChangeWindowKernel(dimension=0, location=1949.49219338, steepness=0.802809935367, width=0.148180293373, operands=[ SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.323111890664, sf=2.63807700464), ProductKernel(operands=[NoiseKernel(sf=-0.173948305272), LinearKernel(dimension=0, location=1971.06184101, sf=-2.0668039892)])]), ConstKernel(sf=3.35167353947) ])])]), likelihood=LikGauss(sf=-inf), nll=226.691810692, ndata=240)

%%%%% Level 9 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-2.01317064646), PeriodicKernel(dimension=0, lengthscale=0.257071217407, period=-0.00107454901576, sf=-2.91140579501)]), SumKernel(operands=[ConstKernel(sf=3.45177455588), ChangeWindowKernel(dimension=0, location=1949.49247564, steepness=0.793068997122, width=0.150536364767, operands=[ SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.329081637806, sf=2.61699101885), ProductKernel(operands=[NoiseKernel(sf=-0.206509243701), LinearKernel(dimension=0, location=1971.21121771, sf=-2.08203851923)])]), ConstKernel(sf=3.32936827389) ])])]), likelihood=LikGauss(sf=-inf), nll=226.681001969, ndata=240)
