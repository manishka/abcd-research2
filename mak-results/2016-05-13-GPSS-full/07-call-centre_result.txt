Experiment all_results for
 datafile = ../data/tsdlr-renamed/07-call-centre.mat

 Running experiment:
description = An 2014-01-16-GPSS-full experiment,
data_dir = ../data/tsdlr-renamed/07-call-centre.mat,
max_depth = 10,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 400,
verbose = True,
make_predictions = False,
skip_complete = True,
results_dir = ../mak-results/2016-05-13-GPSS-full/,
iters = 250,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 3,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 1,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('CP', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('CW', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('B', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('BL', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('None',), {'A': 'kernel'})],
score = BIC,
period_heuristic_type = min,
stopping_criteria = [],
improvement_tolerance = 0.1,
job_time = 00:15:00,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.61612333533), SqExpKernel(dimension=0, lengthscale=-0.11895889308, sf=5.93152939534)]), likelihood=LikGauss(sf=-inf), nll=966.931895869, ndata=180)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ChangePointKernel(dimension=0, location=1974.10313778, steepness=4.24975538177, operands=[ SumKernel(operands=[NoiseKernel(sf=3.34925645146), SqExpKernel(dimension=0, lengthscale=0.489950679855, sf=6.06114611495)]), SumKernel(operands=[NoiseKernel(sf=2.23005465523), SqExpKernel(dimension=0, lengthscale=-0.426143296977, sf=5.15683242343)]) ]), likelihood=LikGauss(sf=-inf), nll=877.783334677, ndata=180)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ChangePointKernel(dimension=0, location=1974.09359145, steepness=4.34756110435, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.0834790901), SqExpKernel(dimension=0, lengthscale=-1.31542855443, sf=1.14167308673)]), SumKernel(operands=[NoiseKernel(sf=-0.188574755212), SqExpKernel(dimension=0, lengthscale=2.71658557044, sf=2.71482252013)])]), SumKernel(operands=[NoiseKernel(sf=2.2197098762), SqExpKernel(dimension=0, lengthscale=-0.443535580368, sf=5.17696983943)]) ]), likelihood=LikGauss(sf=-inf), nll=842.482445933, ndata=180)

%%%%% Level 3 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1957.67217336, sf=-1.67418851032), ChangePointKernel(dimension=0, location=1974.09294909, steepness=4.44447582814, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.12308910197), SqExpKernel(dimension=0, lengthscale=-1.19551850611, sf=0.540743012108)]), SumKernel(operands=[NoiseKernel(sf=-0.757016578071), SqExpKernel(dimension=0, lengthscale=2.58803167306, sf=2.70138737379)])]), SumKernel(operands=[NoiseKernel(sf=1.18188567783), SqExpKernel(dimension=0, lengthscale=1.21767506351, sf=4.15983826648)]) ])]), likelihood=LikGauss(sf=-inf), nll=830.019339861, ndata=180)

%%%%% Level 4 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1954.01882292, sf=-2.0042429345), ChangePointKernel(dimension=0, location=1974.09264386, steepness=4.49292415932, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.82321644571), SqExpKernel(dimension=0, lengthscale=-1.23871419464, sf=0.184598670918)]), SumKernel(operands=[NoiseKernel(sf=-0.537886062149), ConstKernel(sf=2.80764758285)])]), SumKernel(operands=[NoiseKernel(sf=1.4568366013), SqExpKernel(dimension=0, lengthscale=2.01996923241, sf=4.01871258542)]) ])]), likelihood=LikGauss(sf=-inf), nll=821.664756132, ndata=180)

%%%%% Level 5 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1954.07672098, sf=-4.28974412894), SumKernel(operands=[ConstKernel(sf=2.62806102748), PeriodicKernel(dimension=0, lengthscale=0.120813369595, period=-0.00211769465606, sf=1.0722007612)]), ChangePointKernel(dimension=0, location=1974.09456766, steepness=4.49955868587, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.40507651934), SqExpKernel(dimension=0, lengthscale=-0.488815721943, sf=0.304266785334)]), SumKernel(operands=[NoiseKernel(sf=-0.954423800241), ConstKernel(sf=2.63908502949)])]), SumKernel(operands=[NoiseKernel(sf=0.422009137754), SqExpKernel(dimension=0, lengthscale=2.0899633912, sf=3.37881890012)]) ])]), likelihood=LikGauss(sf=-inf), nll=779.697170136, ndata=180)

%%%%% Level 6 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1954.23147421, sf=-5.37443958303), SumKernel(operands=[ConstKernel(sf=4.23510634958), PeriodicKernel(dimension=0, lengthscale=-0.405630894589, period=-0.00224302194107, sf=1.73532911025)]), ChangePointKernel(dimension=0, location=1974.09379076, steepness=4.33371325832, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.2040447301), SqExpKernel(dimension=0, lengthscale=-1.16950681719, sf=-0.00854900093679)]), SumKernel(operands=[NoiseKernel(sf=-1.25553587344), ConstKernel(sf=2.2531021629)])]), SumKernel(operands=[NoiseKernel(sf=-0.304997566539), SqExpKernel(dimension=0, lengthscale=2.21829022129, sf=3.1362416956)]) ])]), likelihood=LikGauss(sf=-inf), nll=777.522225511, ndata=180)

%%%%% Level 7 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1953.50211869, sf=-5.68421286293), SumKernel(operands=[ConstKernel(sf=4.29813284496), PeriodicKernel(dimension=0, lengthscale=-0.587599258983, period=-0.00251640391058, sf=1.25275995634)]), ChangePointKernel(dimension=0, location=1974.09499844, steepness=4.19474924988, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.44636877755), SqExpKernel(dimension=0, lengthscale=-1.23692001762, sf=0.00461450332079)]), SumKernel(operands=[NoiseKernel(sf=-1.2962907852), ConstKernel(sf=2.37732884514)])]), SumKernel(operands=[NoiseKernel(sf=0.149306925048), SqExpKernel(dimension=0, lengthscale=1.83536990689, sf=3.4049216435)]) ])]), likelihood=LikGauss(sf=-inf), nll=773.345532604, ndata=180)

%%%%% Level 8 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1953.32825271, sf=-5.62407352058), SumKernel(operands=[ConstKernel(sf=4.19708122637), PeriodicKernel(dimension=0, lengthscale=-0.571009308445, period=-0.00251226813839, sf=1.20930504531)]), ChangePointKernel(dimension=0, location=1974.09670838, steepness=3.98901457135, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.3810538609), SqExpKernel(dimension=0, lengthscale=-1.2565983981, sf=-0.0219739973222)]), SumKernel(operands=[NoiseKernel(sf=-1.20508253003), ConstKernel(sf=2.38320709687)])]), SumKernel(operands=[NoiseKernel(sf=0.221910878707), SqExpKernel(dimension=0, lengthscale=1.88121114701, sf=3.42776881133)]) ])]), likelihood=LikGauss(sf=-inf), nll=773.103827234, ndata=180)

%%%%% Level 9 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1953.21221409, sf=-5.82193203206), SumKernel(operands=[ConstKernel(sf=4.18185300297), PeriodicKernel(dimension=0, lengthscale=-0.58202922755, period=-0.00253816700508, sf=0.915617483944)]), ChangePointKernel(dimension=0, location=1974.09629512, steepness=4.02184150536, operands=[ ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.5977850114), SqExpKernel(dimension=0, lengthscale=-1.27348588524, sf=-0.0631799458373)]), SumKernel(operands=[NoiseKernel(sf=-1.20547779195), ConstKernel(sf=2.62656208132)])]), SumKernel(operands=[NoiseKernel(sf=0.528866823311), LinearKernel(dimension=0, location=1966.49372658, sf=1.70993987584)]) ])]), likelihood=LikGauss(sf=-inf), nll=772.124488817, ndata=180)
