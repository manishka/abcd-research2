Experiment all_results for
 datafile = ../data/tsdlr_9010/real-daily-wages-in-pounds-engla.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-beam-007,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-beam-007/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 34,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.371118127819), SqExpKernel(dimension=0, lengthscale=3.44067517525, sf=1.73793601749)]), likelihood=LikGauss(sf=-inf), nll=777.525311707, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370812580126), SqExpKernel(dimension=0, lengthscale=3.43768822598, sf=1.73572584198)]), likelihood=LikGauss(sf=-inf), nll=777.526287894, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.370952967658), SqExpKernel(dimension=0, lengthscale=3.44216960902, sf=1.75930955473)]), likelihood=LikGauss(sf=-inf), nll=777.534844853, ndata=661)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32276436673), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.46074851965, sf=1.30205992324), SumKernel(operands=[ConstKernel(sf=0.837628504333), SqExpKernel(dimension=0, lengthscale=0.409941241283, sf=-1.49359584798)])])]), likelihood=LikGauss(sf=-inf), nll=589.34455429, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.64273994773), SqExpKernel(dimension=0, lengthscale=0.195099481623, sf=-0.268027753867), SqExpKernel(dimension=0, lengthscale=4.41610277147, sf=1.99645361326)]), likelihood=LikGauss(sf=-inf), nll=602.746663781, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.29617916141), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=4.49267535912, sf=1.92591514465), SumKernel(operands=[ConstKernel(sf=0.224052260361), SqExpKernel(dimension=0, lengthscale=0.316154555883, sf=-1.97779769446)])])]), likelihood=LikGauss(sf=-inf), nll=603.395790613, ndata=661)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.32991915913), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.979362390057), LinearKernel(dimension=0, location=1643.03658333, sf=-5.51235318892)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.412870699196, sf=-0.00372894249719), SqExpKernel(dimension=0, lengthscale=4.47087961655, sf=2.09548883171)])])]), likelihood=LikGauss(sf=-inf), nll=562.711819793, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.35085243764), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.636410880443), LinearKernel(dimension=0, location=1639.79719673, sf=-5.09176328878)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.397310685629, sf=-0.407367636309), SqExpKernel(dimension=0, lengthscale=4.45274860749, sf=1.61424280059)])])]), likelihood=LikGauss(sf=-inf), nll=562.88791993, ndata=661)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.30122678307), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.471969079731), LinearKernel(dimension=0, location=1650.46651697, sf=-3.81294627116)]), SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.423594860645, sf=-1.64874696678), SqExpKernel(dimension=0, lengthscale=4.45611325388, sf=0.458051502738)])])]), likelihood=LikGauss(sf=-inf), nll=563.444555332, ndata=661)
