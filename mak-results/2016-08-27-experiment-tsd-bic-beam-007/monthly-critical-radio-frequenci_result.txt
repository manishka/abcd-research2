Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-critical-radio-frequenci.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-bic-beam-007,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-bic-beam-007/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 34,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.66119549485), SqExpKernel(dimension=0, lengthscale=-1.32633289318, sf=1.89764146832)]), likelihood=LikGauss(sf=-inf), nll=367.215544753, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.277241892543), SqExpKernel(dimension=0, lengthscale=-0.834912404527, sf=2.0204264617)]), likelihood=LikGauss(sf=-inf), nll=371.342047954, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.545683164144), SqExpKernel(dimension=0, lengthscale=1.2997970781, sf=1.85750460143)]), likelihood=LikGauss(sf=-inf), nll=441.900121469, ndata=216)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.638222331364), PeriodicKernel(dimension=0, lengthscale=0.0917229632806, period=0.00389993494198, sf=-1.12022997353)]), SumKernel(operands=[NoiseKernel(sf=-1.49096756455), SqExpKernel(dimension=0, lengthscale=0.457401742412, sf=1.3737931185)])]), likelihood=LikGauss(sf=-inf), nll=263.180489668, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.566534422542), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.0613113527, sf=0.433148193194), SumKernel(operands=[ConstKernel(sf=1.72783969395), PeriodicKernel(dimension=0, lengthscale=0.12146819136, period=0.00299836893295, sf=-0.158362976349)])])]), likelihood=LikGauss(sf=-inf), nll=265.794794788, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.12742385772), SqExpKernel(dimension=0, lengthscale=-1.97862866695, sf=0.569336348054), SqExpKernel(dimension=0, lengthscale=1.4746467758, sf=1.92073522584)]), likelihood=LikGauss(sf=-inf), nll=293.725997105, ndata=216)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.22300940827), PeriodicKernel(dimension=0, lengthscale=0.0558752089869, period=-0.00049078838958, sf=-0.764469131111)]), SumKernel(operands=[NoiseKernel(sf=-1.28862060985), ConstKernel(sf=1.19637927924), SqExpKernel(dimension=0, lengthscale=-0.080523218734, sf=0.365825644785)])]), likelihood=LikGauss(sf=-inf), nll=227.980029062, ndata=216)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.0484797761741), PeriodicKernel(dimension=0, lengthscale=0.115943492381, period=-0.000583768501359, sf=-0.9416036181)]), SumKernel(operands=[NoiseKernel(sf=-1.12393222569), ConstKernel(sf=1.82424503422), SqExpKernel(dimension=0, lengthscale=-0.0859796474068, sf=0.627341510582)])]), likelihood=LikGauss(sf=-inf), nll=229.37079067, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.986078234845), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.107843272338), SqExpKernel(dimension=0, lengthscale=-0.0378546752352, sf=-0.560750990397)]), SumKernel(operands=[ConstKernel(sf=1.24425483584), PeriodicKernel(dimension=0, lengthscale=0.302767951554, period=-0.000589803207883, sf=0.277602710886)])])]), likelihood=LikGauss(sf=-inf), nll=230.094228185, ndata=216)
