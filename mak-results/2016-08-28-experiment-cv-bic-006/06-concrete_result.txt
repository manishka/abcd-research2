Experiment all_results for
 datafile = ../data/uci-regression/pred/06-concrete.mat

 Running experiment:
description = 2016-08-28-experiment-cv-bic-006,
data_dir = ../data/uci-regression/pred/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 1,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-28-experiment-cv-bic-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 7,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = bic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.61004059263), SqExpKernel(dimension=7, lengthscale=4.42157204315, sf=3.63738566752)]), likelihood=LikGauss(sf=-inf), nll=3750.61515047, ndata=927)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.07499098598), SqExpKernel(dimension=5, lengthscale=-3.89719160192, sf=2.34048636691), SqExpKernel(dimension=7, lengthscale=4.29762325053, sf=3.93547731755)]), likelihood=LikGauss(sf=-inf), nll=3473.27560716, ndata=927)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=7, lengthscale=3.83091582475, sf=3.87594076184), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=3.64678155901, sf=0.996665483931), SumKernel(operands=[NoiseKernel(sf=0.738816672214), SqExpKernel(dimension=5, lengthscale=0.464088783908, sf=1.62890972135)])])]), likelihood=LikGauss(sf=-inf), nll=3221.16913593, ndata=927)
