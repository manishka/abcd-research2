Experiment all_results for
 datafile = ../data/tsdlr_9010/number-of-daily-births-in-quebec.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-beam-007,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-beam-007/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 96,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.44737583869), PeriodicKernel(dimension=0, lengthscale=-4.64496626026, period=-3.36619975238, sf=5.67858799993)]), likelihood=LikGauss(sf=-inf), nll=4563.07506169, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67729952963), SqExpKernel(dimension=0, lengthscale=2.06871498431, sf=5.57269260933)]), likelihood=LikGauss(sf=-inf), nll=4565.15321434, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.67631150152), SqExpKernel(dimension=0, lengthscale=2.04948269007, sf=5.57537476755)]), likelihood=LikGauss(sf=-inf), nll=4565.15526135, ndata=893)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.370478738552), PeriodicKernel(dimension=0, lengthscale=1.69007807999, period=-3.95477968215, sf=-0.643361141447)]), SumKernel(operands=[NoiseKernel(sf=2.76424728132), SqExpKernel(dimension=0, lengthscale=2.32753636607, sf=5.72117857085)])]), likelihood=LikGauss(sf=-inf), nll=4176.03300817, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.21616396869), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.55784992408, sf=6.1093349076), PeriodicKernel(dimension=0, lengthscale=-2.98400154561, period=-3.26084008775, sf=-0.680013353435)])]), likelihood=LikGauss(sf=-inf), nll=4307.28646751, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[PeriodicKernel(dimension=0, lengthscale=-4.71617011859, period=-3.26083394043, sf=5.75144948915), ProductKernel(operands=[NoiseKernel(sf=0.0588043147891), SumKernel(operands=[ConstKernel(sf=2.8621744514), LinearKernel(dimension=0, location=1998.71734289, sf=0.270169940983)])])]), likelihood=LikGauss(sf=-inf), nll=4319.81824927, ndata=893)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=-0.337686831374), PeriodicKernel(dimension=0, lengthscale=1.58648382829, period=-3.95472127165, sf=-1.50254454506)]), SumKernel(operands=[NoiseKernel(sf=3.49496890149), SqExpKernel(dimension=0, lengthscale=-1.11041465443, sf=2.65847107958), SqExpKernel(dimension=0, lengthscale=2.63398068347, sf=5.61408002567)])]), likelihood=LikGauss(sf=-inf), nll=4129.54728388, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[PeriodicKernel(dimension=0, lengthscale=-3.37526184985, period=-2.49597185237, sf=2.09142432488), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.252358815803), PeriodicKernel(dimension=0, lengthscale=1.77103925267, period=-3.95479895842, sf=-0.532651590834)]), SumKernel(operands=[NoiseKernel(sf=2.78083919873), SqExpKernel(dimension=0, lengthscale=2.31148182856, sf=5.7428943643)])])]), likelihood=LikGauss(sf=-inf), nll=4146.40058812, ndata=893)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.858417883904, sf=2.58829904175), ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.47262727639), PeriodicKernel(dimension=0, lengthscale=1.84685274873, period=-3.95474541831, sf=-0.832297149682)]), SumKernel(operands=[NoiseKernel(sf=2.69153469201), SqExpKernel(dimension=0, lengthscale=2.47520319792, sf=5.61498990013)])])]), likelihood=LikGauss(sf=-inf), nll=4169.42774568, ndata=893)
