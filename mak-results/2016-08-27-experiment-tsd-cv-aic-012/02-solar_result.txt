Experiment all_results for
 datafile = ../data/tsdlr_9010/02-solar.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-012,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-012/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 63,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.56246713326), SqExpKernel(dimension=0, lengthscale=6.23087115685, sf=7.0581057441)]), likelihood=LikGauss(sf=-inf), nll=-12.7252579715, ndata=361)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.7215162734), ConstKernel(sf=7.21795067422), SqExpKernel(dimension=0, lengthscale=3.15963822843, sf=-1.16215797614)]), likelihood=LikGauss(sf=-inf), nll=-70.0219468502, ndata=361)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.61629128665), ConstKernel(sf=8.89451956024), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=2.41926713564, sf=1.89503997237), SumKernel(operands=[ConstKernel(sf=-3.29981215451), PeriodicKernel(dimension=0, lengthscale=1.24269715225, period=2.35680821256, sf=-3.65023808755)])])]), likelihood=LikGauss(sf=-inf), nll=-254.630466372, ndata=361)
