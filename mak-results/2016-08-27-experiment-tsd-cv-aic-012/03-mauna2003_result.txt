Experiment all_results for
 datafile = ../data/tsdlr_9010/03-mauna2003.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-cv-aic-012,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-cv-aic-012/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 63,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.415706144475), SqExpKernel(dimension=0, lengthscale=-0.712879096481, sf=2.70000166453)]), likelihood=LikGauss(sf=-inf), nll=836.410795473, ndata=490)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.488708901798), PeriodicKernel(dimension=0, lengthscale=-1.36154958788, period=1.60939209497, sf=-1.22155971784)]), SumKernel(operands=[NoiseKernel(sf=-2.12289073032), SqExpKernel(dimension=0, lengthscale=1.91631344908, sf=2.01331051065)])]), likelihood=LikGauss(sf=-inf), nll=356.504458602, ndata=490)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.303874838986), PeriodicKernel(dimension=0, lengthscale=-1.64335418255, period=1.60919800847, sf=-1.80385153327)]), SumKernel(operands=[NoiseKernel(sf=-1.94155716251), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=3.37697743851, sf=1.29205377447), SumKernel(operands=[ConstKernel(sf=0.925861341), SqExpKernel(dimension=0, lengthscale=0.546590484548, sf=-1.08131297219)])])])]), likelihood=LikGauss(sf=-inf), nll=232.758012055, ndata=490)
