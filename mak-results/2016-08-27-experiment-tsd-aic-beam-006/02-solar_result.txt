Experiment all_results for
 datafile = ../data/tsdlr_9010/02-solar.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-beam-006,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 78,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.56252105638), SqExpKernel(dimension=0, lengthscale=6.22684453036, sf=7.04305561449)]), likelihood=LikGauss(sf=-inf), nll=-12.724511934, ndata=361)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.5623283749), SqExpKernel(dimension=0, lengthscale=6.23794540643, sf=7.08619666378)]), likelihood=LikGauss(sf=-inf), nll=-12.7231577084, ndata=361)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.56228139194), SqExpKernel(dimension=0, lengthscale=6.23970954804, sf=7.09703287895)]), likelihood=LikGauss(sf=-inf), nll=-12.7212172423, ndata=361)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.22282387464), SqExpKernel(dimension=0, lengthscale=0.867532909769, sf=-7.003302887)]), SumKernel(operands=[NoiseKernel(sf=-3.97508018626), SqExpKernel(dimension=0, lengthscale=11.9450242094, sf=5.62440744418)])]), likelihood=LikGauss(sf=-inf), nll=-234.400580822, ndata=361)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.79119660777), SqExpKernel(dimension=0, lengthscale=0.747572922419, sf=-1.54950621827), SqExpKernel(dimension=0, lengthscale=6.53834997902, sf=7.2442586066)]), likelihood=LikGauss(sf=-inf), nll=-229.74666318, ndata=361)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.79137937957), SqExpKernel(dimension=0, lengthscale=0.748903072025, sf=-1.54967154853), SqExpKernel(dimension=0, lengthscale=6.53134768315, sf=7.23406704394)]), likelihood=LikGauss(sf=-inf), nll=-229.745984121, ndata=361)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.80382102292), ConstKernel(sf=7.16923195085), SqExpKernel(dimension=0, lengthscale=0.69468882207, sf=-1.63411234472), SqExpKernel(dimension=0, lengthscale=3.48089062992, sf=-1.15179370419)]), likelihood=LikGauss(sf=-inf), nll=-245.653628887, ndata=361)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.80117222766), ConstKernel(sf=7.22433798184), SqExpKernel(dimension=0, lengthscale=0.695445813126, sf=-1.63192105041), SqExpKernel(dimension=0, lengthscale=3.51106714033, sf=-1.15386284216)]), likelihood=LikGauss(sf=-inf), nll=-245.647427059, ndata=361)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-2.80123404038), ConstKernel(sf=7.43095589954), SqExpKernel(dimension=0, lengthscale=0.701493749449, sf=-1.6328910802), SqExpKernel(dimension=0, lengthscale=3.43574729087, sf=-1.19953437008)]), likelihood=LikGauss(sf=-inf), nll=-245.586602719, ndata=361)
