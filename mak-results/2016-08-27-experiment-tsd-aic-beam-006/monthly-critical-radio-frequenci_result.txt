Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-critical-radio-frequenci.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-beam-006,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 78,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.661195253831), SqExpKernel(dimension=0, lengthscale=-1.32633284594, sf=1.89764152838)]), likelihood=LikGauss(sf=-inf), nll=367.215544753, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.958555604833), SqExpKernel(dimension=0, lengthscale=-1.5981059942, sf=1.84954722231)]), likelihood=LikGauss(sf=-inf), nll=378.067275079, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=0.545683154221), SqExpKernel(dimension=0, lengthscale=1.29979694234, sf=1.85750439635)]), likelihood=LikGauss(sf=-inf), nll=441.900121469, ndata=216)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-0.937919154326), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=0.939436459229, sf=0.146842853596), SumKernel(operands=[ConstKernel(sf=1.74919417187), PeriodicKernel(dimension=0, lengthscale=-0.825289162148, period=0.696751769554, sf=0.236167502608)])])]), likelihood=LikGauss(sf=-inf), nll=261.140955976, ndata=216)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.63386545989), PeriodicKernel(dimension=0, lengthscale=0.212555365681, period=0.00162772108937, sf=0.175457681023)]), SumKernel(operands=[NoiseKernel(sf=-2.23169342583), SqExpKernel(dimension=0, lengthscale=1.04485492195, sf=0.21721091218)])]), likelihood=LikGauss(sf=-inf), nll=265.190536052, ndata=216)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=-1.12742386578), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=1.47464642084, sf=0.29156847152), SumKernel(operands=[ConstKernel(sf=1.62916473887), SqExpKernel(dimension=0, lengthscale=-1.97812779232, sf=0.277768052185)])])]), likelihood=LikGauss(sf=-inf), nll=293.725997105, ndata=216)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.24597340773), PeriodicKernel(dimension=0, lengthscale=0.192416535878, period=-0.000504780229956, sf=0.0154893268672)]), SumKernel(operands=[NoiseKernel(sf=-2.27568327673), ConstKernel(sf=0.494902010327), SqExpKernel(dimension=0, lengthscale=-0.0786831894844, sf=-0.380798975026)])]), likelihood=LikGauss(sf=-inf), nll=230.117889354, ndata=216)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.30211151977), PeriodicKernel(dimension=0, lengthscale=0.107030208055, period=-0.000371183791946, sf=0.221541027952)]), SumKernel(operands=[NoiseKernel(sf=-2.37405837605), SqExpKernel(dimension=0, lengthscale=0.0144123320394, sf=-0.527947672608), SqExpKernel(dimension=0, lengthscale=4.78929583455, sf=-0.0132469982172)])]), likelihood=LikGauss(sf=-inf), nll=230.069610536, ndata=216)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.24091520445), PeriodicKernel(dimension=0, lengthscale=0.109364276921, period=0.000225143450527, sf=-0.21571029641)]), SumKernel(operands=[NoiseKernel(sf=-2.38458709767), SqExpKernel(dimension=0, lengthscale=-2.05695685077, sf=-1.84071706414), SqExpKernel(dimension=0, lengthscale=1.17814123015, sf=0.546029757887)])]), likelihood=LikGauss(sf=-inf), nll=230.139243418, ndata=216)
