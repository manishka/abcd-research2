Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-production-of-sulphuric-.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-beam-006,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 78,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.01560652249), SqExpKernel(dimension=0, lengthscale=0.550210992595, sf=4.64680979898)]), likelihood=LikGauss(sf=-inf), nll=1902.27962591, ndata=415)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.01426518305), SqExpKernel(dimension=0, lengthscale=0.549636200414, sf=4.65026658388)]), likelihood=LikGauss(sf=-inf), nll=1902.28060191, ndata=415)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.28517065701), SqExpKernel(dimension=0, lengthscale=1.41082284718, sf=4.87530940539)]), likelihood=LikGauss(sf=-inf), nll=1947.0195976, ndata=415)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.60581943118), PeriodicKernel(dimension=0, lengthscale=-0.8584360809, period=0.699898148549, sf=-0.141847041033)]), SumKernel(operands=[NoiseKernel(sf=0.78718319389), SqExpKernel(dimension=0, lengthscale=0.707438491486, sf=3.20694799543)])]), likelihood=LikGauss(sf=-inf), nll=1797.86772297, ndata=415)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.44979134135), ProductKernel(operands=[SqExpKernel(dimension=0, lengthscale=3.18059838242, sf=2.02430443577), SumKernel(operands=[ConstKernel(sf=2.6695100399), SqExpKernel(dimension=0, lengthscale=-1.82436898132, sf=1.1504105629)])])]), likelihood=LikGauss(sf=-inf), nll=1808.63677398, ndata=415)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.61170603409), SqExpKernel(dimension=0, lengthscale=-1.82585418659, sf=0.0606848943996)]), SumKernel(operands=[NoiseKernel(sf=0.817100682251), SqExpKernel(dimension=0, lengthscale=3.19203571037, sf=3.1142785615)])]), likelihood=LikGauss(sf=-inf), nll=1808.6374416, ndata=415)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1945.12043875, sf=-2.57221575808), SumKernel(operands=[ConstKernel(sf=1.189669864), PeriodicKernel(dimension=0, lengthscale=-0.905623467262, period=0.698065951819, sf=-0.622017228161)]), SumKernel(operands=[NoiseKernel(sf=0.402428574416), SqExpKernel(dimension=0, lengthscale=0.635564943431, sf=2.69668641943)])]), likelihood=LikGauss(sf=-inf), nll=1757.66918917, ndata=415)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1947.65793935, sf=-3.10025151288), SumKernel(operands=[ConstKernel(sf=1.5553337836), PeriodicKernel(dimension=0, lengthscale=-0.907888774016, period=0.698964516403, sf=-0.306121872764)]), SumKernel(operands=[NoiseKernel(sf=0.684347675757), SqExpKernel(dimension=0, lengthscale=0.637355176953, sf=3.02804268124)])]), likelihood=LikGauss(sf=-inf), nll=1758.3915515, ndata=415)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[LinearKernel(dimension=0, location=1943.74376615, sf=-1.97550593342), SumKernel(operands=[ConstKernel(sf=0.826082386277), PeriodicKernel(dimension=0, lengthscale=-1.4248523229, period=1.10919250429, sf=-1.0823652356)]), SumKernel(operands=[NoiseKernel(sf=0.0683894665003), SqExpKernel(dimension=0, lengthscale=0.905695896698, sf=2.57258963901)])]), likelihood=LikGauss(sf=-inf), nll=1761.16648424, ndata=415)
