Experiment all_results for
 datafile = ../data/tsdlr_9010/monthly-average-daily-calls-to-d.mat

 Running experiment:
description = 2016-08-27-experiment-tsd-aic-beam-006,
data_dir = ../data/tsdlr_9010/,
max_depth = 3,
random_order = False,
k = 3,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 5000,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-27-experiment-tsd-aic-beam-006/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 78,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = False,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64764180958), SqExpKernel(dimension=0, lengthscale=-0.107292640081, sf=6.00965883055)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64764176939), SqExpKernel(dimension=0, lengthscale=-0.107292713651, sf=6.00965922686)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)
GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.64764203641), SqExpKernel(dimension=0, lengthscale=-0.107292560862, sf=6.00965811709)]), likelihood=LikGauss(sf=-inf), nll=876.10975733, ndata=162)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.45083560144), LinearKernel(dimension=0, location=1964.18513143, sf=1.42943310399)]), SumKernel(operands=[NoiseKernel(sf=0.330370776377), SqExpKernel(dimension=0, lengthscale=0.147387637504, sf=2.92670661592)])]), likelihood=LikGauss(sf=-inf), nll=835.879901839, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.91771466798), LinearKernel(dimension=0, location=1964.18510199, sf=1.89633404485)]), SumKernel(operands=[NoiseKernel(sf=-0.136522509354), SqExpKernel(dimension=0, lengthscale=0.147374525071, sf=2.45968737342)])]), likelihood=LikGauss(sf=-inf), nll=835.879901957, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.219827220118), LinearKernel(dimension=0, location=1964.18573342, sf=-0.801685647933)]), SumKernel(operands=[NoiseKernel(sf=2.56148513173), SqExpKernel(dimension=0, lengthscale=0.147345889492, sf=5.15787695371)])]), likelihood=LikGauss(sf=-inf), nll=835.879903279, ndata=162)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=1.28374001527), PeriodicKernel(dimension=0, lengthscale=-1.1836703405, period=0.695991180551, sf=-0.994577944287)]), SumKernel(operands=[ConstKernel(sf=2.11051763714), LinearKernel(dimension=0, location=1964.19625658, sf=1.04317191651)]), SumKernel(operands=[NoiseKernel(sf=-1.45111293156), SqExpKernel(dimension=0, lengthscale=0.441550401336, sf=1.7847339582)])]), likelihood=LikGauss(sf=-inf), nll=797.478689244, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=2.98322468087), ProductKernel(operands=[LinearKernel(dimension=0, location=1964.7336471, sf=-0.376866904421), SumKernel(operands=[ConstKernel(sf=1.78658227095), SqExpKernel(dimension=0, lengthscale=-1.71537135432, sf=0.0939765438766)])])]), SumKernel(operands=[NoiseKernel(sf=-0.349792836579), SqExpKernel(dimension=0, lengthscale=0.989102547731, sf=2.67272242693)])]), likelihood=LikGauss(sf=-inf), nll=800.241164636, ndata=162)
GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=0.767263566192), ProductKernel(operands=[LinearKernel(dimension=0, location=1964.73364787, sf=-2.39527235958), SumKernel(operands=[ConstKernel(sf=1.58902554895), SqExpKernel(dimension=0, lengthscale=-1.71537220409, sf=-0.103580076924)])])]), SumKernel(operands=[NoiseKernel(sf=1.86616881751), SqExpKernel(dimension=0, lengthscale=0.989101484742, sf=4.88868332253)])]), likelihood=LikGauss(sf=-inf), nll=800.241164636, ndata=162)
