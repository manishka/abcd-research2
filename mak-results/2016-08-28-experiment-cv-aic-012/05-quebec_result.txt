Experiment all_results for
 datafile = ../data/uci-regression/pred/05-quebec.mat

 Running experiment:
description = 2016-08-28-experiment-cv-aic-012,
data_dir = ../data/uci-regression/pred/,
max_depth = 3,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 9,
sd = 2,
jitter_sd = 0.1,
max_jobs = 4500,
verbose = True,
make_predictions = True,
skip_complete = True,
results_dir = ../mak-results/2016-08-28-experiment-cv-aic-012/,
iters = 100,
base_kernels = SE,Per,Lin,Const,Noise,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 61,
period_heuristic = 3,
max_period_heuristic = 5,
subset = True,
subset_size = 250,
full_iters = 10,
bundle_size = 2000,
search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('None',), {'A': 'kernel'})],
score = aic,
period_heuristic_type = min,
stopping_criteria = ['no_improvement'],
improvement_tolerance = 0.1,
job_time = 01:30:00,
crossval = True,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.34483411819), LinearKernel(dimension=1, location=3.9639876832, sf=4.21967524373)]), likelihood=LikGauss(sf=-inf), nll=21929.4904676, ndata=4602)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=3.04344497207), SqExpKernel(dimension=0, lengthscale=-1.80143629866, sf=2.68597885204), LinearKernel(dimension=1, location=3.88746111928, sf=4.25861019638)]), likelihood=LikGauss(sf=-inf), nll=20722.6622334, ndata=4602)

%%%%% Level 2 %%%%%

GPModel(mean=MeanZero(), kernel=SumKernel(operands=[NoiseKernel(sf=2.93096488231), SqExpKernel(dimension=0, lengthscale=-1.24623632991, sf=3.69507310053), PeriodicKernel(dimension=0, lengthscale=-1.61683434175, period=-3.95462207444, sf=5.93124131583)]), likelihood=LikGauss(sf=-inf), nll=20599.2438213, ndata=4602)
