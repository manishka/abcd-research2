"""
A module to make it much easier to send code to the CBL computing cluster.

Contributions encouraged

@authors:
James Robert Lloyd (jrl44@cam.ac.uk)

"""

import pyfear # Is this line necessary?
import pyslurm
from pyfear import fear
from pyslurm_local import slurm
from util import mkstemp_safe, string_is_int
import os
import psutil, subprocess, sys, time
from counter import Progress
from datetime import datetime as dt
import datetime, inspect
import tempfile
try:
    from config import *
except:
    print '\n\nERROR : source/cblparallel/config.py not found\n\nPlease create it following example file as a guide\n\n'
    raise Exception('No config')

import zipfile, zlib

slurm_obj = pyslurm_local.slurm() 

#### WISHLIST
####  - Limit number of active local jobs
####  - Display progress
####  - Loop more frequently
####  - Write setup function / make it possible for people to use this code without lots of hassle
####  - Provide convenience functions to setup MATLAB/python paths
####  - Merge job handling code
####  - Return STDOUT and STDERR from scripts
####  - Tidy up port forwarding
####  - Write unit tests
####  - Write asynchronous job handling
####  - Add support for jobs in arbitrary languages (will require more user input)

def start_port_forwarding():
    #### TODO - Check that the port forward is not already active to prevent ssh process proliferation
    ####      - Use python statements rather than system calls
    cmd = 'ssh -N -f -L %d:fear:22 %s@gate.eng.cam.ac.uk' % (HOME_TO_REMOTE_PORT, USERNAME)
    subprocess.call(cmd.split(' '))
    cmd = 'ssh -N -f -R %d:localhost:22 -p %d %s@localhost' % (REMOTE_TO_HOME_PORT, HOME_TO_REMOTE_PORT, USERNAME)
    subprocess.call(cmd.split(' '))
    
if LOCATION=='home':
    start_port_forwarding()

def setup():
    '''
    Run an interactive script to setup various preliminaries e.g.
     - RSA key pairs
     - Fear .profile including script that makes qsub, qstat etc available
     - Local directory on fear with python scripts
     - Local directory on machine where temporary files are stored
     - Local directory on fear where temporary files are stored
    '''
    pass
    
def create_temp_file(extension):
    if LOCATION=='home':
        return mkstemp_safe(HOME_TEMP_PATH, extension)
    else:
        return mkstemp_safe(LOCAL_TEMP_PATH, extension)
        
def copy_to_remote(file_name):
    with pyfear.fear(via_gate=(LOCATION=='home')) as fear:
        fear.copy_to_temp(file_name)
      
def remove_temp_file(file_name, local_computation=False):
    os.remove(file_name)
    if not local_computation:
        with pyfear.fear(via_gate=(LOCATION=='home')) as fear:
            fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(file_name)[-1]))
        
def gpml_path(local_computation=True):
    if not local_computation:
        return REMOTE_GPML_PATH
    elif LOCATION == 'local':
        return LOCAL_GPML_PATH
    else:
        return HOME_GPML_PATH
    
#### TODO - Combine these functions? - they share much code
####      - Maybe this could be achieved by creating a generic object like fear that (re)moves files etc.
####      - but either does this on fear, on local machine, or on fear via gate.eng.cam.ac.uk

def run_batch_on_fear(scripts, language='python', job_check_sleep=30, file_copy_timeout=300, max_jobs=500, verbose=True, zip_files=False, bundle_size=1):
    '''
    Receives a list of python scripts to run

    Assumes the code has an output file - i.e. %(output_file)s - that will be managed by this function
    
    Returns a list of local file names where the code has presumably stored output
    '''
    # Define some code constants
    #### TODO - this path adding code should accept an optional list of paths
    python_path_code = '''
import sys
sys.path.append('%s')
''' % REMOTE_PYTHON_PATH

    #### This will be deprecated in future MATLAB - hopefully the -singleCompThread command is sufficient
    #### TODO - No longer used? Remove?
    matlab_single_thread = '''
maxNumCompThreads(1);
'''

    matlab_path_code = '''
addpath(genpath('%s'))
''' % REMOTE_MATLAB_PATH
    
    #### TODO - allow port forwarding / tunneling - copy to machine on network with more disk space than fear, then copy from that machine?
    #### FIXME - Now does port forwarding but with fixed port numbers = bad
    #### TODO - Fix port forard leak
    if LOCATION == 'home':
        python_transfer_code = '''
from subprocess_timeout import timeoutCommand
import subprocess
print "Setting up port forwarding"
if subprocess.Popen("netstat -an | grep %(r2rport)d | grep LISTEN", shell=True, stdout=subprocess.PIPE).communicate()[0] == '':
    timeoutCommand(cmd='ssh -i %(rsa_remote)s -N -f -L %(r2rport)d:localhost:%(r2hport)d %(username)s@fear').run(timeout=%(timeout)d)
print "Moving output file"
if not timeoutCommand(cmd='scp -P %(r2rport)d -i %(rsa_home)s %(output_file)s %(home_user)s@localhost:%(local_temp_path)s; rm %(output_file)s').run(timeout=%(timeout)d)[0]:
    raise RuntimeError('Copying output raised error or timed out')
''' % {'rsa_remote' : REMOTE_TO_REMOTE_KEY_FILE,
       'r2rport' : REMOTE_TO_REMOTE_PORT,
       'r2hport' : REMOTE_TO_HOME_PORT,
       'username' : USERNAME,
       'timeout' : file_copy_timeout,
       'rsa_home' : REMOTE_TO_HOME_KEY_FILE,
       'output_file' : '%(output_file)s',
       'home_user' : HOME_USERNAME,
       'local_temp_path' : HOME_TEMP_PATH}
    else:    
        python_transfer_code = '''
#from util import timeoutCommand
from subprocess_timeout import timeoutCommand
print "Moving output file"
if not timeoutCommand(cmd='scp -i %(rsa_key)s %(output_file)s %(username)s@%(local_host)s:%(local_temp_path)s; rm %(output_file)s').run(timeout=%(timeout)d)[0]:
    raise RuntimeError('Copying output raised error or timed out')
''' % {'rsa_key' : REMOTE_TO_LOCAL_KEY_FILE,
       'output_file' : '%(output_file)s',
       'username' : USERNAME,
       'local_host' : LOCAL_HOST,
       'local_temp_path' : LOCAL_TEMP_PATH,
       'timeout' : file_copy_timeout}
    
    #### TODO - make this location independent
    #### TODO - does this suffer from the instabilities that lead to the verbosity of the python command above
    if LOCATION == 'home':
        matlab_transfer_code = '''
[status, result] = system('netstat -an | grep %(r2rport)d | grep LISTEN')
if isempty(strfind(result, 'LISTEN'))
  system('ssh -i %(rsa_remote)s -N -f -L %(r2rport)d:localhost:%(r2hport)d %(username)s@fear')
end
system('scp -o ConnectTimeout=%(timeout)d -P %(r2rport)d -i %(rsa_home)s %(output_file)s %(home_user)s@localhost:%(local_temp_path)s; rm %(output_file)s')
''' % {'rsa_remote' : REMOTE_TO_REMOTE_KEY_FILE,
       'r2rport' : REMOTE_TO_REMOTE_PORT,
       'r2hport' : REMOTE_TO_HOME_PORT,
       'username' : USERNAME,
       'timeout' : file_copy_timeout,
       'rsa_home' : REMOTE_TO_HOME_KEY_FILE,
       'output_file' : '%(output_file)s',
       'home_user' : HOME_USERNAME,
       'local_temp_path' : HOME_TEMP_PATH}
    else:
        matlab_transfer_code = '''
system('scp -o ConnectTimeout=%(timeout)d -i %(rsa_key)s %(output_file)s %(username)s@%(local_host)s:%(local_temp_path)s; rm %(output_file)s')
''' % {'rsa_key' : REMOTE_TO_LOCAL_KEY_FILE,
       'output_file' : '%(output_file)s',
       'username' : USERNAME,
       'timeout' : file_copy_timeout,
       'local_host' : LOCAL_HOST,
       'local_temp_path' : LOCAL_TEMP_PATH}
       
#    python_completion_code = '''
#print 'Writing completion flag'
#with open('%(flag_file)s', 'w') as f:
#    f.write('Goodbye, World')
#print "I'll bite your legs off!"
#quit()
#'''       
    python_completion_code = '''
print "I will bite your legs off!"
quit()
'''
  
    #### TODO - Is this completely stable       
#    matlab_completion_code = '''
#fprintf('\\nWriting completion flag\\n');
#ID = fopen('%(flag_file)s', 'w');
#fprintf(ID, 'Goodbye, world');
#fclose(ID);
#fprintf('\\nGoodbye, World\\n');
#quit()
#'''
    matlab_completion_code = """
fprintf('\\nGoodbye, World\\n');
quit()
"""
    
    # Open a connection to fear as a with block - ensures connection is closed
    with pyfear.fear(via_gate=(LOCATION=='home')) as fear:
    
        if LOCATION == 'local':
            temp_dir = LOCAL_TEMP_PATH
        else:
            temp_dir = HOME_TEMP_PATH
            
        if bundle_size > 1:  
            
            # Build bundled scripts 
            
            current_script = ''
            current_outputs = []
            shell_files = []
            script_files = []
            output_files = []
            bundled = 0
            for code in scripts:
                bundled += 1
                
                # Build up a bundle of scripts
                if language == 'python':
                    individual_script = python_path_code + code + python_transfer_code
                elif language == 'matlab':
                    individual_script = matlab_path_code + code + matlab_transfer_code
                individual_output_file = mkstemp_safe(temp_dir, '.out')
                
                current_script += individual_script % {'output_file': os.path.join(REMOTE_SCRATCH_PATH, os.path.split(individual_output_file)[-1])}
                current_outputs.append(individual_output_file)
                
                if bundled >= bundle_size:
                    # Complete the code and create files
                    if language == 'python':
                        current_script += python_completion_code
                        script_files.append(mkstemp_safe(temp_dir, '.py'))
                    elif language == 'matlab':
                        current_script += matlab_completion_code
                        script_files.append(mkstemp_safe(temp_dir, '.m'))
                    shell_files.append(mkstemp_safe(temp_dir, '.sh'))
                    # Write code and shell file
                    with open(script_files[-1], 'w') as f:
                        f.write(current_script)
                    with open(shell_files[-1], 'w') as f:
                        #### TODO - is os.path.join always correct - what happens if this program is being run on windows?
                        if language == 'python':
                            f.write('python ' + os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[-1])[-1]) + '\n')
                        elif language == 'matlab':
                            f.write('cd ' + REMOTE_TEMP_PATH + ';\n' + REMOTE_MATLAB + ' -nosplash -nojvm -nodisplay -r ' + \
                                    os.path.split(script_files[-1])[-1].split('.')[0] + '\n')
                    # Record output files for each job
                    output_files.append(current_outputs)
                    # Reset
                    bundled = 0
                    current_script = ''
                    current_outputs = []      
                       
            if bundled > 0:
                # Complete the code and create files
                if language == 'python':
                    current_script += python_completion_code
                    script_files.append(mkstemp_safe(temp_dir, '.py'))
                elif language == 'matlab':
                    current_script += matlab_completion_code
                    script_files.append(mkstemp_safe(temp_dir, '.m'))
                shell_files.append(mkstemp_safe(temp_dir, '.sh'))
                # Write code and shell file
                with open(script_files[-1], 'w') as f:
                    f.write(current_script)
                with open(shell_files[-1], 'w') as f:
                    #### TODO - is os.path.join always correct - what happens if this program is being run on windows?
                    if language == 'python':
                        f.write('python ' + os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[-1])[-1]) + '\n')
                    elif language == 'matlab':
                        f.write('cd ' + REMOTE_TEMP_PATH + ';\n' + REMOTE_MATLAB + ' -nosplash -nojvm -nodisplay -r ' + \
                                os.path.split(script_files[-1])[-1].split('.')[0] + '\n')
                # Record output files for each job
                output_files.append(current_outputs)
                # Reset
                bundled = 0
                current_script = ''
                current_outputs = []     
                                
            job_ids = [None] * len(script_files) 
            fear_finished = False
            job_finished = [False] * len(script_files) 
            fail_count = [0] * len(script_files)
            
        else: # bundle_size = 0
    
            # Initialise lists of file locations job ids
            shell_files = [None] * len(scripts)
            script_files = [None] * len(scripts)
            output_files = [None] * len(scripts)
            stdout_files = [None] * len(scripts)
            flag_files = [None] * len(scripts) # TODO - Am I used anymore?
            job_ids = [None] * len(scripts) 
            fear_finished = False
            job_finished = [False] * len(scripts) 
            fail_count = [0] * len(scripts)
            
            # Modify all scripts and create local temporary files

            if verbose:
                print 'Writing temporary files locally'
            for (i, code) in enumerate(scripts):
                if language == 'python':
                    script_files[i] = mkstemp_safe(temp_dir, '.py')
                elif language == 'matlab':
                    script_files[i] = mkstemp_safe(temp_dir, '.m')
                shell_files[i] = mkstemp_safe(temp_dir, '.sh')
                output_files[i] = mkstemp_safe(temp_dir, '.out')
                flag_files[i] = mkstemp_safe(temp_dir, '.flg')
                # Customise code (path, transfer of output back to local host, flag file writing)
                #### TODO - make path and output_transfer optional
                if language == 'python':
                    code = python_path_code + code + python_transfer_code + python_completion_code
                elif language == 'matlab':
                    code = matlab_path_code + code + matlab_transfer_code + matlab_completion_code
                #code = code % {'output_file': os.path.join(REMOTE_TEMP_PATH, os.path.split(output_files[i])[-1]),
                #               'flag_file' : os.path.join(REMOTE_TEMP_PATH, os.path.split(flag_files[i])[-1])}
                code = code % {'output_file': os.path.join(REMOTE_SCRATCH_PATH, os.path.split(output_files[i])[-1])}
                # Write code and shell file
                with open(script_files[i], 'w') as f:
                    f.write(code)
                with open(shell_files[i], 'w') as f:
                    #### TODO - is os.path.join always correct - what happens if this program is being run on windows?
                    if language == 'python':
                        f.write('python ' + os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[i])[-1]) + '\n')
                    elif language == 'matlab':
                        f.write('cd ' + REMOTE_TEMP_PATH + ';\n' + REMOTE_MATLAB + ' -nosplash -nojvm -nodisplay -r ' + \
                                os.path.split(script_files[i])[-1].split('.')[0] + '\n')
                                    
        BUF_SIZE = 25
        script_buffer = []
        job_number_buffer = []

        print 'Submitting %d experiments in %d bundles' % (len(scripts), len(script_files))

        # Zip and send files

        if zip_files:
            print 'Zipping files'
            zip_file_name = mkstemp_safe(temp_dir, '.zip')
            zf = zipfile.ZipFile(zip_file_name, mode='w')
            for script in script_files:
                zf.write(script, arcname=(os.path.split(script)[-1]), compress_type=zipfile.ZIP_DEFLATED)
            for shell in shell_files:
                zf.write(shell, arcname=(os.path.split(shell)[-1]), compress_type=zipfile.ZIP_DEFLATED)
            zf.close()
            print 'Sending zip file to fear'
            fear.copy_to_temp(zip_file_name)
            print 'Unzipping on fear'
            fear.command('cd %(temp_path)s ; unzip %(zip_file)s ; rm %(zip_file)s' % {'temp_path' : REMOTE_TEMP_PATH, 'zip_file' : os.path.split(zip_file_name)[-1]})
            print 'Finished unzipping'

        # Loop through jobs, submitting jobs whenever fear usage low enough, re-submitting failed jobs
        while not fear_finished:
            # Update knowledge of fear - trying to limit communication
            fear.qstat()
            jobs_alive = fear.jobs_alive(update=False)
            # Sleep unless anything happens
            should_sleep = True
            for i in range(len(script_files)): # Make me more pythonic with zipping
                # Should we flush the job buffer
                if len(script_buffer) >= BUF_SIZE:
                    new_ids = fear.qsub_multi(script_buffer, verbose=verbose)
                    for (job_number, job_id) in zip(job_number_buffer, new_ids):
                        if not string_is_int(job_id):
                            print 'Incorrect job id ''%s'' for script %s returned - resumbitting later' % (job_id, os.path.split(shell_files[job_number])[-1])
                            fail_count[job_number] += 1 
                            fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[job_number])[-1]))
                            fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[job_number])[-1]))
                        else:
                            job_ids[job_number] = job_id
                            jobs_alive += 1
                    script_buffer = []
                    job_number_buffer = []
            
                #fear.qstat()
                #jobs_alive = fear.jobs_alive(update=False)
                # Does the job need to be run and can we run it?
                if (not job_finished[i]) and (job_ids[i] is None) and (jobs_alive <= max_jobs):
                    # Something has happened
                    should_sleep = False
                    # Transfer files to fear
                    if (fail_count[i] > 0) or (not zip_files):
                        fear.copy_to_temp(script_files[i])
                        fear.copy_to_temp(shell_files[i])
                    # Submit the job to fear
                    if verbose:
                        print 'Submitting job %d of %d' % (i + 1, len(scripts)),
                        
                    #job_ids[i] = fear.qsub(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]), verbose=verbose) # Hide path constant
                    #if not string_is_int(job_ids[i]):
                    #    print 'Incorrect job id ''%s'' for script %s returned - resumbitting later' % (job_ids[i], os.path.split(shell_files[i])[-1])
                    #    job_ids[i] = None
                    #    fail_count[i] += 1                        
                    #    fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[i])[-1]))
                    #    fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]))
                    #else:
                    #    jobs_alive += 1
                    
                    script_buffer.append(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]))
                    job_number_buffer.append(i)

                #### FIXME - This was an attempt at better utilisation of fear - back fired since calling qstat is slow - correct solution is to have job sending and job completion checking in different threads - but would need to be carerful with the shared lists

                #for j in range(len(scripts)):
                #    if (not job_finished[j]) and (job_ids[j] is None) and (jobs_alive <= max_jobs):
                #        # Something has happened
                #        should_sleep = False
                #        # Transfer files to fear
                #        if not zip_files:
                #            fear.copy_to_temp(script_files[j])
                #            fear.copy_to_temp(shell_files[j])
                #        # Submit the job to fear
                #        print 'Submitting job %d of %d' % (j + 1, len(scripts))
                #        job_ids[j] = fear.qsub(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[j])[-1]), verbose=verbose) # Hide path constant
                #        # Increment job count
                #        jobs_alive += 1

                # Otherwise was it running last we checked?
                elif (not job_finished[i]) and (not job_ids[i] is None):
                #if (not job_finished[i]) and (not job_ids[i] is None):
                    # Has the process terminated?
                    if fear.job_terminated(job_ids[i], update=False):
                        # Decrement job count
                        #jobs_alive -= 1 - would not have been counted earlier
                        should_sleep = False
                        # Has the job failed to write a flag or is the output file empty
                        #### FIXME - If LOCATION=='home' need to check .out file on local is non-empty
                        #### TODO - Can likely increase speed by checking status of files (on fear and local) in one block - not sure how to do it though
                        ####      - In particular checking for file existence should probably be done with a SFTP client rather than SSH - might be faster?
                        #if (not fear.file_exists(os.path.join(REMOTE_TEMP_PATH, os.path.split(flag_files[i])[-1]))) or \
                        #   (os.stat(output_files[i]).st_size == 0):
                        if ((bundle_size == 1) and (os.stat(output_files[i]).st_size == 0)) or \
                           ((bundle_size > 1) and any((os.stat(output_file).st_size == 0) for output_file in output_files[i])):
                            # Job has finished but missing output - resubmit later
                            print 'Shell script %s job_id %s failed' % (os.path.split(shell_files[i])[-1], job_ids[i])
                            # Save job id for file deletion
                            old_job_id = job_ids[i]
                            job_ids[i] = None
                            fail_count[i] += 1  
                        else:
                            # Job has finished successfully
                            job_finished[i] = True
                            # Save job id for file deletion
                            old_job_id = job_ids[i]
                            # Move files if necessary
                            #if LOCATION=='home':
                            #    # Copy the file from local storage machine (and delete it)
                            #    fear.copy_from_localhost(localpath=output_files[i], remotepath=os.path.join(LOCAL_TEMP_PATH, os.path.split(output_files[i])[-1]))
                            # Tell the world
                            if verbose:
                                print '%d / %d jobs complete' % (sum(job_finished), len(job_finished))
                            # Tidy up local temporary directory - actually - do this in one batch later
                            #os.remove(script_files[i])
                            #os.remove(shell_files[i])
                            #os.remove(flag_files[i])
                        # Tidy up fear
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[i])[-1]))
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]))
                        #fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(flag_files[i])[-1]))
                        #### TODO - record the output and error files for future reference
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.o%s' % old_job_id))
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.e%s' % old_job_id))
                        #### TODO - is the following line faster?
                        #fear.command('rm ' + ' ; rm '.join([os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[i])[-1]), os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]), os.path.join(REMOTE_TEMP_PATH, os.path.split(flag_files[i])[-1]), os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.o%s' % old_job_id), os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.e%s' % old_job_id)]))
                        # Tidy up local temporary directory
                        #os.remove(script_files[i])
                        #os.remove(shell_files[i])
                        #os.remove(flag_files[i])    
                        job_ids[i] = None                     
                            
                    elif not (fear.job_queued(job_ids[i]) or fear.job_running(job_ids[i]) \
                              or fear.job_loading(job_ids[i])):
                        # Job has some status other than running, queuing or loading - something is wrong, delete it
                        jobs_alive -= 1
                        should_sleep = False
                        old_job_id = job_ids[i]
                        fear.qdel(job_ids[i])
                        fail_count[i] += 1  
                        print 'Shell script %s job_id %s stuck, deleting' % (os.path.split(shell_files[i])[-1], job_ids[i])
                        #### TODO - remove this code duplication
                        # Tidy up fear
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[i])[-1]))
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]))
                        # fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(flag_files[i])[-1]))
                        #### TODO - record the output and error files for future reference
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.o%s' % old_job_id))
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.e%s' % old_job_id))
                        #### TODO - is the following line faster?
                        #fear.command('rm ' + ' ; rm '.join([os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[i])[-1]), os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]), os.path.join(REMOTE_TEMP_PATH, os.path.split(flag_files[i])[-1]), os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.o%s' % old_job_id), os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[i])[-1]) + ('.e%s' % old_job_id)]))
                        # Tidy up local temporary directory
                        #os.remove(script_files[i])
                        #os.remove(shell_files[i])
                        #os.remove(flag_files[i])    
                        job_ids[i] = None   
            # Flush the job buffer if non-empty
            if len(script_buffer) > 0:
                new_ids = fear.qsub_multi(script_buffer, verbose=verbose)
                for (job_number, job_id) in zip(job_number_buffer, new_ids):
                    if not string_is_int(job_id):
                        print 'Incorrect job id ''%s'' for script %s returned - resumbitting later' % (job_id, os.path.split(shell_files[job_number])[-1])
                        fail_count[job_number] += 1                        
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(script_files[job_number])[-1]))
                        fear.rm(os.path.join(REMOTE_TEMP_PATH, os.path.split(shell_files[job_number])[-1]))
                    else:
                        job_ids[job_number] = job_id
                        jobs_alive += 1
                script_buffer = []
                job_number_buffer = []
            # Are we finished?
            if all(job_finished):
                fear_finished = True    
            elif should_sleep:
                print '%d of %d jobs complete' % (sum(job_finished), len(job_finished))
                if verbose:
                    #fear.qstat()
                    print '%d jobs running' % fear.jobs_running(update=False)
                    print '%d jobs loading' % fear.jobs_loading(update=False)
                    print '%d jobs queued' % fear.jobs_queued(update=False)
                    #print 'Sleeping for %d seconds' % job_check_sleep
                time.sleep(job_check_sleep)

    # Tidy up temporary directory
    if verbose:
        print 'Removing temporary files'
    for i in range(len(script_files)):
        os.remove(script_files[i])
        os.remove(shell_files[i])
        #os.remove(flag_files[i])
    if zip_files:
        os.remove(zip_file_name)

    #### TODO - return job output and error files as applicable (e.g. there may be multiple error files associated with one script)
    if bundle_size > 1:
        return [output_file for list_of_files in output_files for output_file in list_of_files]
    else:
        return output_files
    
def run_batch_locally(scripts, language='python', paths=[], max_cpu=0.9, max_mem=0.9, submit_sleep=1, job_check_sleep=10, \
                      verbose=True, max_files_open=100, max_running_jobs=10, single_thread=True):
    '''
    Receives a list of python scripts to run

    Assumes the code has an output file that will be managed by this function
    
    Returns a list of local file names where the code has presumably stored output
    '''
    # Define some code constants
    #### Do we need to set paths explicitly?

    #### This will be deprecated in future MATLAB - hopefully the -singleCompThread command is sufficient
    matlab_single_thread = '''
maxNumCompThreads(1);
'''

    python_path_code = '''
import sys
sys.path.append('%s')
'''

    matlab_path_code = '''
addpath(genpath('%s'))
'''
       
    python_completion_code = '''
print 'Writing completion flag'
with open('%(flag_file)s', 'w') as f:
    f.write('Goodbye, World')
print "Goodbye, World"
quit()
'''
  
    #### TODO - Is this completely stable       
    matlab_completion_code = '''
fprintf('\\nWriting completion flag\\n');
ID = fopen('%(flag_file)s', 'w');
fprintf(ID, 'Goodbye, world');
fclose(ID);
fprintf('\\nGoodbye, World\\n');
quit()
'''
    
    # Initialise lists of file locations job ids
    shell_files = [None] * len(scripts)
    script_files = [None] * len(scripts)
    output_files = [None] * len(scripts)
    stdout_files = [None] * len(scripts)
    stdout_file_handles = [None] * len(scripts)
    flag_files = [None] * len(scripts)
    processes = [None] * len(scripts)
    fear_finished = False
    job_finished = [False] * len(scripts)  
    
    files_open = 0

    # Loop through jobs, submitting jobs whenever CPU usage low enough, re-submitting failed jobs
    if not verbose:
        prog = Progress(len(scripts))
    while not fear_finished:
        should_sleep = True
        for (i, code) in enumerate(scripts):
            if (not job_finished[i]) and (processes[i] is None) and (files_open <= max_files_open) and (len([1 for p in processes if not p is None]) < max_running_jobs):
                # This script has not been run - check CPU and potentially run
                #### FIXME - Merge if statements
                if (psutil.cpu_percent() < max_cpu * 100) and (psutil.virtual_memory().percent < max_mem * 100):
                    # Jobs can run
                    should_sleep = False
                    # Get the job ready
                    if LOCATION == 'local':
                        temp_dir = LOCAL_TEMP_PATH
                    else:
                        temp_dir = HOME_TEMP_PATH
                    if language == 'python':
                        script_files[i] = (mkstemp_safe(temp_dir, '.py'))
                    elif language == 'matlab':
                        script_files[i] = (mkstemp_safe(temp_dir, '.m'))
                    # Create necessary files in local path
                    shell_files[i] = (mkstemp_safe(temp_dir, '.sh'))
                    output_files[i] = (mkstemp_safe(temp_dir, '.out'))
                    stdout_files[i] = (mkstemp_safe(temp_dir, '.o'))
                    flag_files[i] = (mkstemp_safe(temp_dir, '.flg'))
                    # Customise code
                    #### TODO - make path and output_transfer optional
                    if language == 'python':
                        code = code + python_completion_code
                        for path in paths:
                            code = (python_path_code % path) + code
                    elif language == 'matlab':
                        code = code + matlab_completion_code
                        for path in paths:
                            code = (matlab_path_code % path) + code
                    code = code % {'output_file': output_files[i],
                                   'flag_file' : flag_files[i]}
                    # Write code and shell file
                    with open(script_files[i], 'w') as f:
                        f.write(code)
                    with open(shell_files[i], 'w') as f:
                        #### TODO - is os.path.join always correct - what happens if this program is being run on windows?
                        if language == 'python':
                            f.write('python ' + script_files[i] + '\n')
                        elif language == 'matlab':
                            if LOCATION == 'home':
                                matlab_path = HOME_MATLAB
                            else:
                                matlab_path = LOCAL_MATLAB
                            if single_thread:
                                #f.write('cd ' + os.path.split(script_files[i])[0] + ';\n' + matlab_path + ' -nosplash -nojvm -nodisplay -r ' + \
                                f.write('cd ' + os.path.split(script_files[i])[0] + ';\n' + matlab_path + ' --no-gui -W --texi-macros-file=/dev/null --built-in-docstrings-file=/dev/null -q "' + \
                                        os.path.split(script_files[i])[-1] + '"\n')
                            else:
                                #f.write('cd ' + os.path.split(script_files[i])[0] + ';\n' + matlab_path + ' -nosplash -nojvm -nodisplay -r ' + \
                                f.write('cd ' + os.path.split(script_files[i])[0] + ';\n' + matlab_path + '  --no-gui -W --texi-macros-file=/dev/null --built-in-docstrings-file=/dev/null -q "' + \
                                        os.path.split(script_files[i])[-1] + '"\n')
                    # Start running the job
                    if verbose:
                        print 'Submitting job %d of %d' % (i + 1, len(scripts))
                    stdout_file_handles[i] = open(stdout_files[i], 'w')
                    files_open = files_open + 1
                    processes[i] = subprocess.Popen(['sh', shell_files[i]], stdout = stdout_file_handles[i]);
                    # Sleep for a bit so the process can kick in (prevents 100s of jobs being sent to processor)
                    time.sleep(submit_sleep)
            elif (not job_finished[i]) and (not processes[i] is None):
                # Ask the process how its doing
                processes[i].poll()
                # Check to see if the process has completed
                if not processes[i].returncode is None:
                    if os.path.isfile(flag_files[i]):
                        job_finished[i] = True
                        if verbose:
                            print 'Job %d of %d has completed' % (i + 1, len(scripts))
                        else:
                            prog.tick()
                    else:
                        if verbose:
                            print 'Job %d has failed - will try again later' % i + 1
                        processes[i] = None
                    # Tidy up temp files
                    os.remove(script_files[i])
                    os.remove(shell_files[i])
                    stdout_file_handles[i].close()
                    files_open = files_open - 1
                    os.remove(stdout_files[i])
                    os.remove(flag_files[i])
                    processes[i] = None
                    # Something useful happened
                    should_sleep = False
        if all(job_finished):
            fear_finished = True 
            if not verbose: 
                prog.done()  
        elif should_sleep:
            # Count how many jobs are queued
            n_queued = 0
            # Count how many jobs are running
            n_running = 0
            if verbose:
                # print '%d jobs running' % n_running
                # print '%d jobs queued' % n_queued
                print 'Sleeping for %d seconds' % job_check_sleep
            time.sleep(job_check_sleep)

    #### TODO - return job output and error files as applicable (e.g. there may be multiple error files associated with one script)
    return output_files

# Print iterations progress
def printProgress (iteration, total, prefix = '', suffix = '', decimals = 2, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    filledLength    = int(round(barLength * iteration / float(total)))
    percents        = round(100.00 * (iteration / float(total)), decimals)
    bar             = '#' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),
    sys.stdout.flush()
    if iteration == total:
        sys.stdout.write('\n')
        sys.stdout.flush()


def formatter(start, end, step):
    return '{}-{}'.format(start, end)

def re_range(inplst):
    lst = sorted(inplst)
    n = len(lst)
    result = []
    scan = 0
    while n - scan > 2:
        step = lst[scan + 1] - lst[scan]
        if lst[scan + 2] - lst[scan + 1] != step:
            result.append(str(lst[scan]))
            scan += 1
            continue

        for j in range(scan+2, n-1):
            if lst[j+1] - lst[j] != step:
                result.append(formatter(lst[scan], lst[j], step))
                scan = j+1
                break
        else:
            result.append(formatter(lst[scan], lst[-1], step))
            return ','.join(result)

    if n - scan == 1:
        result.append(str(lst[scan]))
    elif n - scan == 2:
        result.append(','.join(map(str, lst[scan:])))

    return ','.join(result)
		
def create_files_for_slurm(scripts, start, end, output_files, script_files, shell_files, param_files, temp_dir, matlab_path, matlab_completion_code, matlab_path_code,paths,data_file):
    current_outputs = []
    temp_script_dir = tempfile.mkdtemp(dir=temp_dir)
    os.system('cp %s %s' %(data_file,temp_script_dir))
    for i in range(start, end):
        code = scripts[i]

        # Create output files
        output_files.append(mkstemp_safe(temp_script_dir, '.out'))

        # Customise code
        script_files.append(mkstemp_safe(temp_script_dir, '.m'))
        code = code + matlab_completion_code
        for path in paths:
            code = (matlab_path_code % path) + code
        code = code % {'output_file': output_files[-1]}

        # Write code in script file
        with open(script_files[-1], 'w') as f:
            f.write(code)

        # Build up a bundle of scripts
        current_outputs.append(script_files[-1] + '\n')

    # Let's create a shell file & param_file
    shell_files.append(mkstemp_safe(temp_script_dir, '.sh'))
    param_files.append(mkstemp_safe(temp_script_dir, '.dat'))
    with open(param_files[-1], 'w') as f:
        f.writelines(current_outputs)
    with open(shell_files[-1], 'w') as f:
        f.write('#!/bin/sh '+\
                        '\n cd ' + temp_script_dir + ';'+\
                        '\n module load Octave;\n srun ' + matlab_path + ' --no-gui -W '+\
                        '--texi-macros-file=/dev/null --built-in-docstrings-file=/dev/null -q '+\
                        '$(sed -n ${SLURM_ARRAY_TASK_ID}p '+ os.path.split(param_files[-1])[-1] +\
                        ') \n')
    current_outputs = []
    return

def find_job_state(myjobs,job_id,verbose):                                                     
    state = ''
    if '_' in job_id:
        array_job_id = int(job_id.split('_')[0])
        array_task_id = int(job_id.split('_')[1])                           
        x = [myjobs[i]['job_state'] for i in myjobs if myjobs[i]['array_job_id']==array_job_id and myjobs[i]['array_task_id']==array_task_id]
        if len(x) > 0:
            state = x[0]
        elif array_job_id in myjobs.keys():
            ranges = myjobs[array_job_id]['array_task_str'].split(',') if myjobs[array_job_id]['array_task_str'] != None else []
            for range in ranges:
                range = range.split('-')
                if(len(range) == 1 and array_task_id == int(range[0])) or (len(range) > 1 and int(range[0]) <= array_task_id <= int(range[1])):
                    state = myjobs[array_job_id]['job_state']
                    break
        else:
            if verbose:
                print 'Error: job_id', array_job_id, 'is not in current jobs',myjobs.keys()
    else:
        if verbose:
            print 'Error: job_id', job_id, 'does not contain index'
    return state

def get_alive_jobs_count(myjobs):
    max_jobs = 2000
    USERNAME = 'mkuk073'
    verbose = True
    jobs_cnt = max_jobs
    try: 
        jobs_cnt = subprocess.check_output('/usr/bin/squeue  -r --noheader --states=completing,running,pending -u %s | wc -l'%USERNAME,stderr=subprocess.STDOUT, shell=True)
        jobs_cnt = int(jobs_cnt)
    except (subprocess.CalledProcessError, ValueError) as e:
        if verbose:
            print 'Error: fetching jobs alive count', e
        jobs_cnt = max_jobs
    finally:
        return jobs_cnt
    # count = 0
    # for job_id in [myjobs[i]['array_task_id'] if myjobs[i]['array_task_id'] is not None else myjobs[i]['array_task_str'] for i in myjobs if myjobs[i]['job_state']=='RUNNING' or myjobs[i]['job_state']=='PENDING']:
        # if type(job_id) == int: 
            # count+=1
        # else:
            # ranges = job_id.split(",")
            # for range_ in ranges:
                # range_ = range_.split('-')
                # count += len(range(int(range_[0]),int(range_[1]))) if len(range_) > 1 else 1
    # return count

def get_jobs_alive_count(USERNAME, max_jobs,verbose):
    jobs_cnt = max_jobs
    try: 
        jobs_cnt = subprocess.check_output('/usr/bin/squeue  -r --noheader --states=completing,running,pending -u %s | wc -l'%USERNAME,stderr=subprocess.STDOUT, shell=True)
        jobs_cnt = int(jobs_cnt)
    except (subprocess.CalledProcessError, ValueError) as e:
        if verbose:
            print 'Error: fetching jobs alive count', e
        jobs_cnt = max_jobs
    finally:
        return jobs_cnt

def run_batch_on_slurm(scripts, data_file, language='matlab', paths=[], max_cpu=0.9, max_mem=0.9, submit_sleep=1, job_check_sleep=40, \
                  verbose=True, max_files_open=100, single_thread=True, bundle_size=2000,max_jobs=4000,job_time='00:15:00', mem_per_cpu=4096, file_name_prefix=''):
    '''
    Receives a list of matlab scripts to run

    Assumes the code has an output file that will be managed by this function
    
    Returns a list of local file names where the code has presumably stored output
    '''
    # Define some code constants
    #### Do we need to set paths explicitly?

    #### This will be deprecated in future MATLAB - hopefully the -singleCompThread command is sufficient
    matlab_single_thread = '''
maxNumCompThreads(1);
'''
    
    matlab_path_code = '''
addpath(genpath('%s'))
'''
    
    #### TODO - Is this completely stable       
    matlab_completion_code = '''
fprintf('\\nWriting completion flag\\n');
fprintf('\\nGoodbye, World\\n');
quit()
'''
    
    global slurm_obj 
    #slurm_obj = pyslurm_local.slurm()  
    
    slurm_finished = False
    script_files = []
    output_files = []
    
    BUF_SIZE = bundle_size if bundle_size < 2000 else 2000 # Max array size for slurm jobs 1-2000
    loop = len(scripts) / BUF_SIZE # no. of master_jobs needed
    extra = len(scripts) % BUF_SIZE
    if verbose:
        print 'BUF_SIZE:',BUF_SIZE,'loop:',loop,'extra:',extra
    shell_files = [] 
    param_files = []
    
    if LOCATION == 'local':
        temp_dir = LOCAL_TEMP_PATH
        matlab_path = LOCAL_MATLAB
    else:
        temp_dir = HOME_TEMP_PATH
        matlab_path = HOME_MATLAB
    
    # Create shell, param, script, output files
    for master_jobs in range(loop):
        if verbose:
            print 'Creating script files from: %d - %d' %(master_jobs * BUF_SIZE, (master_jobs+1) * BUF_SIZE)
        create_files_for_slurm(scripts, master_jobs * BUF_SIZE, (master_jobs+1) * BUF_SIZE, output_files, script_files, shell_files, param_files, temp_dir, matlab_path, matlab_completion_code, matlab_path_code,paths,data_file)
    if verbose:
        print 'Creating script files from: %d - %d' %(loop * BUF_SIZE, extra + loop * BUF_SIZE)
    create_files_for_slurm(scripts, loop * BUF_SIZE, extra + loop * BUF_SIZE, output_files, script_files, shell_files, param_files, temp_dir, matlab_path, matlab_completion_code, matlab_path_code,paths,data_file) 

    # Let's submit master jobs    
    job_ids = [None] * len(scripts) 
    job_finished = [False] * len(scripts) 
    fail_count = [0] * len(scripts)
    state_fetch_fail = [0] * len(scripts)    
    job_number_buffer = []
    job_time_increased = 0
    master_job_submitted = False
    jobs_alive = 0
    print 'Submitting %d experiments in %d bundles %s' % (len(scripts), len(shell_files), shell_files)
    
    printProgress(sum(job_finished), len(job_finished), prefix = 'Progress:', 
                  suffix = 'Complete', barLength = 50)
    print '\n'
    
    # Loop through jobs, submitting jobs whenever slurm usage low enough, re-submitting failed jobs
    while not slurm_finished:
        try:
            # get all slurm jobs for this username
            slurm_jobs = pyslurm.job().find_user(USERNAME)
            jobs_alive = get_jobs_alive_count(USERNAME,max_jobs,verbose)

            for i in range(len(scripts)): # Make me more pythonic with zipping
                
                # Does the job need to be run and can we run it?
                if (not job_finished[i]) and (job_ids[i] is None) and jobs_alive < max_jobs:
                    job_number_buffer.append(i+1)
                    jobs_alive+= 1

                # Otherwise was it running last we checked?
                elif (not job_finished[i]) and (not job_ids[i] is None):
                    # get the state of the job
                    try:
                        state = find_job_state(slurm_jobs,job_ids[i],verbose)
                        # Has the process terminated?
                        if state == 'COMPLETED' or state == 'FAILED' or state == 'TIMEOUT' or state =='CANCELLED':
                            state_fetch_fail[i] = 0
                            if os.stat(output_files[i]).st_size == 0:
                                # Job has finished but missing output - resubmit later
                                print 'Job_id %s failed with job_time %s mem_per_cpu %d' % (job_ids[i], job_time, mem_per_cpu)
                                # Save job id for file deletion
                                old_job_id = job_ids[i]
                                fail_count[i] += 1 
                                if (state == 'TIMEOUT' or state == 'FAILED') and job_time_increased < 2:
                                    # increase the wall-time for job by one hour
                                    job_time = (dt.strptime(job_time,'%H:%M:%S') + datetime.timedelta(minutes=60)).strftime('%H:%M:%S')
                                    job_time_increased += 1
                            else:
                                # Job has finished successfully
                                job_finished[i] = True
                                # Save job id for file deletion
                                old_job_id = job_ids[i]
                                
                                # Tell the world
                                if verbose:
                                    print '%s job complete' % old_job_id
                            # Deleting temporary stdout & stderr of script
                            job_stdout_file = os.path.join(os.path.split(shell_files[i/BUF_SIZE])[-2], os.path.split(shell_files[i/BUF_SIZE])[-1]) + ('.o%s' % job_ids[i])
                            job_stderr_file = os.path.join(os.path.split(shell_files[i/BUF_SIZE])[-2], os.path.split(shell_files[i/BUF_SIZE])[-1]) + ('.e%s' % job_ids[i])
                            if os.path.exists(job_stdout_file): os.remove(job_stdout_file)
                            if os.path.exists(job_stderr_file): os.remove(job_stderr_file)
                            job_ids[i] = None
                            jobs_alive -= 1
                                
                        elif not(state =='RUNNING' or state =='PENDING' or state == 'COMPLETING' or state =='CONFIGURING'): 
                            # Job has some status other than running, queuing or loading - something is wrong, delete it
                            if not state=='' or state_fetch_fail[i] > 30:
                                print 'Script %s job_id %s stuck, deleting State=\'%s\'' % (os.path.split(script_files[i])[-1], job_ids[i],state), state_fetch_fail[i]
                                old_job_id = job_ids[i]
                                slurm_obj.qdel(job_ids[i])
                                fail_count[i] += 1
                                
                                # Deleting temporary stdout & stderr of script
                                job_stdout_file = os.path.join(os.path.split(shell_files[i/BUF_SIZE])[-2], os.path.split(shell_files[i/BUF_SIZE])[-1]) + ('.o%s' % job_ids[i])
                                job_stderr_file = os.path.join(os.path.split(shell_files[i/BUF_SIZE])[-2], os.path.split(shell_files[i/BUF_SIZE])[-1]) + ('.e%s' % job_ids[i])
                                if os.path.exists(job_stdout_file): os.remove(job_stdout_file)
                                if os.path.exists(job_stderr_file): os.remove(job_stderr_file)                                
                                job_ids[i] = None
                                jobs_alive -= 1
                                state_fetch_fail[i] = 0
                            else:
                                if verbose:
                                    print 'Error fetching state - job_id:', job_ids[i], 'state:', state
                                # check if job has actually completed 
                                if os.stat(output_files[i]).st_size > 0:
                                    # Job has finished successfully
                                    job_finished[i] = True
                                    # Save job id for file deletion
                                    old_job_id = job_ids[i]
                                    
                                    # Tell the world
                                    if verbose:
                                        print 'But output file %s is present, so %s job complete' % (output_files[i], old_job_id)

                                    # Deleting temporary stdout & stderr of script
                                    job_stdout_file = os.path.join(os.path.split(shell_files[i/BUF_SIZE])[-2], os.path.split(shell_files[i/BUF_SIZE])[-1]) + ('.o%s' % job_ids[i])
                                    job_stderr_file = os.path.join(os.path.split(shell_files[i/BUF_SIZE])[-2], os.path.split(shell_files[i/BUF_SIZE])[-1]) + ('.e%s' % job_ids[i])
                                    if os.path.exists(job_stdout_file): os.remove(job_stdout_file)
                                    if os.path.exists(job_stderr_file): os.remove(job_stderr_file)
                                    job_ids[i] = None
                                    jobs_alive -= 1
                                    state_fetch_fail[i] = 0
                                else:
                                    state_fetch_fail[i] += 1
                    except:
                        state = ''
                        continue
                            
            # Flush the job buffer if non-empty
            if len(job_number_buffer) > 0:
                if verbose:
                    print 'Trying to submit %d jobs' % len(job_number_buffer)  
                for itr in range(loop + 1):
                    range_start = (itr * BUF_SIZE) if itr < loop else (loop * BUF_SIZE)
                    range_end = (itr + 1) * BUF_SIZE if itr < loop else (extra + loop * BUF_SIZE)
                    
                    temp_idx = re_range([jnb - range_start for jnb in job_number_buffer if (1 + range_start) <= jnb <= range_end])
                        
                    if len(temp_idx) > 0:
                        new_ids = slurm_obj.qsub(os.path.join(os.path.split(shell_files[itr])[-2], os.path.split(shell_files[itr])[-1]), 
                                            verbose=verbose,job_time=job_time, mem_per_cpu=mem_per_cpu, 
                                            array_index='%s'%(temp_idx)).strip()
                        if not string_is_int(new_ids):
                            print 'Incorrect job id ''%s'' for %s %s' % (new_ids, shell_files[itr], temp_idx)
                        else:
                            for jnb in job_number_buffer:
                                if (1 + range_start) <= jnb <= range_end:
                                    job_ids [jnb - 1] = new_ids + '_%d'%(jnb - range_start)
                            if verbose:
                                print 'Submitted',temp_idx ,'new job_ids[]:',[job_ids[jnb - 1] for jnb in job_number_buffer if (1 + range_start) <= jnb <= range_end]
                job_number_buffer = []

        except (ValueError,AttributeError) as e:
            jobs_alive = 0
            if verbose:
                print 'Exception occured:',repr(e),inspect.trace()[-1]
        
        # Are we finished?
        if all(job_finished):
            slurm_finished = True    
        printProgress(sum(job_finished), len(job_finished), prefix = 'Progress:', suffix = 'Complete', barLength = 50)
        print '\n', time.strftime("%c"), '%d of %d jobs complete, submitted %s / %s alive on slurm \n' % (sum(job_finished), len(job_finished), len(job_ids) - job_ids.count(None),jobs_alive)
        time.sleep(job_check_sleep)

    # Tidy up temporary directory
    if verbose:
        print 'Removing temporary files'
    for script_file in script_files:
        temp_data_file = os.path.split(script_file)[-2]+'/'+data_file
        if os.path.exists(temp_data_file):
            os.remove(temp_data_file)
    [os.remove(script_file) for script_file in script_files if os.path.exists(script_file)]  
    [os.remove(shell_file) for shell_file in shell_files if os.path.exists(shell_file)]
    [os.remove(param_file) for param_file in param_files if os.path.exists(param_file)]      

    return output_files   

def reload_config():
    global slurm_obj
    data = dict()
    with open('config.py') as f:
      for line in f:
         if 'OVERRIDE_SBATCH' in line:
            data[line.split("=",1)[0].strip()] = line.split("=",1)[1].strip()
    if 'OVERRIDE_SBATCH' in data.keys() and 'OVERRIDE_SBATCH_OPTIONS' in data.keys():
      slurm_obj.setOverride(data['OVERRIDE_SBATCH'].split("'")[1].strip(), data['OVERRIDE_SBATCH_OPTIONS'].split("'")[1].strip())
    else:
      print 'SIGUSR1 received but config.py does not contain OVERRIDE_SBATCH or OVERRIDE_SBATCH_OPTIONS'
