'''
A set of utilities to talk to the
slurm manager and perform
common tasks

@authors: Manish Kukreja (mkuk073@aucklanduni.ac.nz)
'''

from config import * # Various constants such as USERNAME
from util import timeoutCommand
import os
import subprocess
import re
import tempfile
import time

class slurm(object):
    '''
    Manages communications with the slurm manager
    '''
    def __init__(self):
        self.override_sbatch = 'False'
        self.override_sbatch_options = ''
 
    def command(self, cmd):
        success = False
        fail_count = 0
        while not success:
            try:
                process = subprocess.Popen(cmd,stdout=subprocess.PIPE, shell=True)
                output = process.communicate()[0].strip()
                success = True
            except:
                fail_count += 1
                if fail_count >= 10:
                    raise RuntimeError('Failed to send command 10 times')
                print 'Failed to excute command: %s' % cmd
                print 'Sleeping for a minute'
                time.sleep(60)
        return output
    def check_output(self, cmd):
        output = ""
	try: 
		output = subprocess.check_output(cmd, shell=True)
	except  subprocess.CalledProcessError as e:
		print "Command_Failed", cmd, e.returncode    
	return output

    def multiple_commands(self, cmds):
        '''
        Just places semicolons between commands - trying to eak out some extra speed
        '''
        return self.command(' ; '.join(cmds))
             
    def rm(self, remote_path):
        #### FIXME - Correctly handle case of missing file (or anything else that might cause an error)
        try:
            output = self.command('rm -f %s' % remote_path)
        except:
            return 'Error'    
    
    def file_exists(self, remote_path):
        response = self.command('if [ -e %s ] \nthen \necho ''exists'' \nfi' % remote_path)
        return response == ['exists\n']
    
    def qsub(self, shell_file, verbose=True, job_time="00:15:00", mem_per_cpu=4096, array_index=''):
        '''
        Submit a job onto the stack.
        '''
        if self.override_sbatch == 'True':
            slurm_string = ' '.join(['cd %s;' % os.path.split(shell_file)[0],
                                'chmod +x %s;' % shell_file,
                                'sbatch -A uoa00427 %s %s;'  % (self.override_sbatch_options,shell_file) ]) 
        else:
            slurm_string = ' '.join(['cd %s;' % os.path.split(shell_file)[0],
                                'chmod +x %s;' % shell_file,
                                'sbatch -A uoa00427 -o %s.o%%A_%%a -e %s.e%%A_%%a --time=%s -c 1 --mem-per-cpu=%d --array=%s %s;' % (os.path.split(shell_file)[-1],os.path.split(shell_file)[-1],job_time,mem_per_cpu,array_index,shell_file) ])
        
        output_text = self.check_output(slurm_string)
        # Return the job id
        output_text = output_text.split(' ')
        
        if verbose:
            print 'Submitting: %s' % os.path.split(shell_file)[-1], slurm_string
            print 'Output:', output_text 
        
       	return output_text[3] if len(output_text) > 3 and output_text[0] == 'Submitted' and output_text[1] == 'batch' and output_text[2] == 'job' else '' 
    
    def qsub_multi(self, shell_files, verbose=True, job_time="00:15:00", mem_per_cpu=4096):
        '''
        Submit multiple jobs onto the stack.
        '''
        output_text = []	 
        print 'qsub_multi', len(shell_files)
        for shell_file in shell_files:
                if not shell_file is None:
                        output_text.append(self.qsub(shell_file, verbose, job_time[shell_file] if type(job_time) is dict else job_time, mem_per_cpu[shell_file] if type(mem_per_cpu) is dict else mem_per_cpu))    
                        time.sleep(4)
        # Return the job ids
        return output_text
    
    def qdel(self, job_id):
        output = self.command('scancel %s' % job_id)
        return output
    
    def qdel_all(self):
        output = self.command('scancel -u %s' % USERNAME)
        return output
    
    def qstat(self):
        '''Updates a dictionary with (job id, status) pairs'''
        cmd = "/usr/bin/squeue --states=all -u %s -h -o '%%.18i %%.15T %%.10M'" % USERNAME
        queue_list = self.command(cmd).split()
        # Clear old status dictionary
        self.status = {}
        # Iterate over jobs 
        for i in range(0,len(queue_list),3):
            self.status[queue_list[i]] = queue_list[i+1]
    
    def get_job_etime(self, job_id):
        '''Returns elapsed time of the job'''
        cmd = "squeue -u %s --states=all -h -o '%%.18i %%.15T %%.10M' -j %s" % (USERNAME, job_id)
        queue_stat = self.command(cmd).split()
        if len(queue_stat) > 0: 
            return queue_stat[2]
        else:
            return None
        
    def job_terminated(self, job_id, update=False):
        '''Returns true if job is failed, timed-out or completed'''
        if update:
            self.qstat()
        #return not self.status.has_key(job_id)
        if self.status.has_key(job_id):
            return self.status[job_id] == 'FAILED' or self.status[job_id] == 'TIMEOUT' or self.status[job_id] == 'COMPLETED'
        else:
            return False
    
    def job_running(self, job_id, update=False):
        if update:
            self.qstat()
        if self.status.has_key(job_id):
            return self.status[job_id] == 'RUNNING'
        else:
            return False
    
    def job_queued(self, job_id, update=False):
        if update:
            self.qstat()
        if self.status.has_key(job_id):
            return self.status[job_id] == 'PENDING'
        else:
            return False
    
    def job_loading(self, job_id, update=False):
        if update:
            self.qstat()
        if self.status.has_key(job_id):
            return (self.status[job_id] == 'COMPLETING' or self.status[job_id] == 'COMPLETED')
        else:
            return False
            
    def jobs_running(self, update=True):
        '''Returns number of jobs currently running'''
        if update:
            self.qstat()
        # Count running jobs
        return len([1 for job_id in self.status if self.job_running(job_id)])
            
    def jobs_queued(self, update=True):
        '''Returns number of jobs currently queued'''
        if update:
            self.qstat()
        # Count queued jobs
        return len([1 for job_id in self.status if self.job_queued(job_id)])
            
    def jobs_loading(self, update=True):
        '''Returns number of jobs currently loading'''
        if update:
            self.qstat()
        # Count loading jobs
        return len([1 for job_id in self.status if self.job_loading(job_id)])
        
    def jobs_alive(self, update=True):
        '''Returns number of jobs currently running, queueing or loading'''
        if update:
            self.qstat()
        # Count jobs
        return self.jobs_running(update=False) + self.jobs_queued(update=False) + self.jobs_loading(update=False)

    def setOverride(self, override, options):
        self.override_sbatch = override
        self.override_sbatch_options = options
        print 'slurm.setOverride() called: %s %s' %(self.override_sbatch, self.override_sbatch_options)
