LOCAL_TEMP_PATH            = '/projects/uoa00427/temp' # A directory where temporary files can be written safely
LOCAL_MATLAB               = 'octave' # Path to matlab - on some machines this will just be 'matlab'
LOCATION                   = 'local' # A vestigial feature - keep as is
LOCAL_GPML_PATH            = '/projects/uoa00427/abcd-research2/source/gpml' # Location of GPML
USERNAME                   = 'mkuk073'
OVERRIDE_SBATCH            = 'True'
OVERRIDE_SBATCH_OPTIONS    = ' --time=02:30:00 -c 1 --mem-per-cpu=4096 '
