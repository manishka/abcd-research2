import bytehook

set_pdb_trace = True

def runpdb2(_locals=None, _globals=None):
    import pdb
    global set_pdb_trace
    if set_pdb_trace:
        pdb.set_trace()
        set_pdb_trace = False

#hookid = bytehook.hook(job_controller.evaluate_models, lineno=2,insert_func=runpdb2,  with_state=True)
hookid = bytehook.hook(experiment.run_experiment_file, lineno =20,insert_func=runpdb2,  with_state=True)
