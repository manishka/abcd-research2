tic;
%rand('twister', 193099094);
%randn('state', 193099094);

a='Load the data, it should contain X and y.'
load '../data/uci-regression/concrete-pred/06-concrete-pred-001.mat'


X_full = double(X);
y_full = double(y);

X_valid = double(Xtest);
y_valid = double(ytest);


if true & (250 < size(X, 1))
    subset = randsample(size(X, 1), 250, false);
    X = X_full(subset,:);
    y = y_full(subset);
end

% Load GPML
addpath(genpath('/projects/uoa00427/abcd-research2/source/gpml'));

% Set up model.
meanfunc = {@meanZero}
hyp.mean = [  ]

covfunc = {@covProd, {{@covNoise}, {@covSum, {{@covConst}, {@covMask, {[0 1 0 0 0 0 0 0], {@covLinear}}}}}}}
hyp.cov = [ 0.08102703757 5.14054852954 -3.60809625127 116.153980168 ]

likfunc = {@likDelta}
hyp.lik = [  ]

inference = @infDelta

% Optimise on subset
[hyp_opt, nlls] = minimize(hyp, @gp, -int32(100 * 3 / 3), inference, meanfunc, covfunc, likfunc, X, y);

% Optimise on full data
if 10 > 0
    hyp_opt = minimize(hyp_opt, @gp, -10, inference, meanfunc, covfunc, likfunc, X_full, y_full);
end
display(hyp_opt)

% Evaluate the nll on the full data
best_nll = gp(hyp_opt, inference, meanfunc, covfunc, likfunc, X_full, y_full)
best_nll
[ymu, ys2, predictions, fs2, loglik] = gp(hyp_opt, inference, meanfunc, covfunc, likfunc, X_full,y_full,X_valid,y_valid);

mae_pred = mean(abs(y_valid - predictions))
mae_pred
mean(loglik)

display(['Timespent=', num2str(toc)]);
% exit();

%quit()
