import numpy as np
import scipy.io
from scipy.stats import norm
import os
variances = {}
var2= {}
for directory in ['/projects/uoa00427/abcd-research2/data/uci-regression/pred','/projects/uoa00427/abcd-research2/data/tsdlr_9010']:
    for (dirn, dirn2, filen) in os.walk(directory):
        for filename in filen:
                if filename.endswith('.mat'):
                    data = scipy.io.loadmat(dirn+"/"+filename)
                    ymean = np.mean(data['y'])
                    variances[filename]= np.var(data['y'])
                    #var2[filename] = np.mean((data['y'] - ymean)**2)
                    var2[filename]= norm.nnlf(norm.fit(data['X']), data['X'])
                    
