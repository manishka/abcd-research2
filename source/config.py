MATLAB_LOCATION = "octave"
GPML_PATH = "gpml/"
COLOR_SCHEME = "dark"
LOCATION = "local"
OVERRIDE_SBATCH            = 'False'
OVERRIDE_SBATCH_OPTIONS    = ' -o tmp4vcX61.sh.o%A_%a -e tmp4vcX61.sh.e%A_%a --time=01:30:00 -c 1 --mem-per-cpu=4096 --array=1-1200 '

