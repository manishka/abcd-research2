import experiment
files = ['01-airline_result.txt',  '06-internet_result.txt',  '07-call-centre_result.txt',  '08-radio_result.txt']
dirs = ['/projects/uoa00427/abcd-research2/mak-results/2016-05-13-GPSS-full/','/projects/uoa00427/abcd-research2/results/2014-01-16-GPSS-full/']

for d in dirs:
	for f in files:
		model = experiment.parse_results(d + f)
		print f.split('_result.txt')[0], model.nll, model.bic, model.kernel.effective_params, model.ndata, model.pretty_print()


