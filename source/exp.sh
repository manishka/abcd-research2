#!/bin/bash
echo "starting experiment" $1
/gpfs1m/projects/uoa00427/python-virtualenv/bin/python -u demo.py $1 1> /projects/uoa00427/cons_out/$1 2> /projects/uoa00427/cons_out/$1-err & disown
ln -s /projects/uoa00427/cons_out/$1 $1
ln -s /projects/uoa00427/cons_out/$1-err $1-err
exit 0
