import experiment
import postprocessing
import sys,signal,datetime
import job_controller
import cblparallel
def sigUSR1Handler(signum, stack):
  print "SIGUSR1 received Date: %s" %datetime.datetime.now()
  experiment.reload_config()


signal.signal(signal.SIGUSR1, sigUSR1Handler)

if len(sys.argv)>1:
  experiment.run_experiment_file('../mak-results/experiment-'+str(sys.argv[1])+'.py')
else:
  #experiment.run_experiment_file('../mak-results/experiment.py')
  experiment.run_experiment_file('../examples/fast_example2.py')
# To see the outcome of this experiment, look in examples/01-airline_result.txt

#postprocessing.make_all_1d_figures(folders='../examples/', save_folder='../examples/', rescale=False)
  
