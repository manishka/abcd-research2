tic;
%rand('twister', 193099094);
%randn('state', 193099094);

a='Load the data, it should contain X and y.'
load '../data/uci-regression/concrete-pred/06-concrete-pred-002.mat'

X_full = double(X);
y_full = double(y);

X_valid = double(Xtest);
y_valid = double(ytest);

% Load GPML
addpath(genpath('/projects/uoa00427/abcd-research2/source/gpml'));

% Set up model.
meanfunc = {@meanZero}
hyp.mean = [  ]

covfunc = {@covProd, {{@covNoise}, {@covSum, {{@covConst}, {@covMask, {[0 1 0 0 0 0 0 0], {@covLinear}}}}}}}
hyp.cov = [ 0.08102703757 5.14054852954 -3.60809625127 116.153980168 ]

likfunc = {@likDelta}
hyp.lik = [  ]

inference = @infDelta

avg_rmse = NaN
try
    if true
        K = 10;
        %cv = cvpartition(numel(y_full), 'kfold',K);
        mae = zeros(K,1);
        for k=1:K

            % training/testing indices for this fold
            testIdx = false(size(X_full,1),1); 
            testIdx(randsample(size(X_full,1), ceil(size(X_full,1)*0.1), false)) = true;
            trainIdx  = !testIdx;
            
            %trainIdx = training(cv,k);
            %testIdx = test(cv,k);

            Xtrain = X_full(trainIdx,:);
            ytrain = y_full(trainIdx);

            Xtest = X_full(testIdx,:);
            ytest = y_full(testIdx);

            if true & (250 < size(Xtrain, 1))
                subset = randsample(size(Xtrain, 1), 250, false);
                Xsub = Xtrain(subset,:);
                ysub = ytrain(subset);
                
                % Optimise on subset
                [hyp_opt(k), nlls] = minimize(hyp, @gp, -int32(10 * 3 / 3), inference, meanfunc, covfunc, likfunc, Xsub, ysub);

            end

            % Optimise on full data
            if 10 > 0
                hyp_opt(k) = minimize(hyp_opt(k), @gp, -10, inference, meanfunc, covfunc, likfunc, Xtrain, ytrain);
            end

            % train and predict
            [ymu, ys2, predictions, fs2, loglik] = gp(hyp_opt(k), inference, meanfunc, covfunc, likfunc, Xtrain,ytrain,Xtest);

            % compute mean squared error
            mae(k) = mean(abs(ytest - predictions));
            display(['fold: ',num2str(k),' mae:',num2str(mae(k)),' hyp_opt:'])
            display(hyp_opt(k)); 
        end
        % average RMSE across k-folds
        %avg_rmse = mean(sqrt(mse))
        %display(avg_rmse)
        [min_mae, min_mae_index] = min(mae)
        % train on full data and get nll
        best_nll = gp(hyp_opt(min_mae_index), inference, meanfunc, covfunc, likfunc, X_full, y_full);
        best_nll 
        [ymu, ys2, predictions, fs2, loglik] = gp(hyp_opt(min_mae_index), inference, meanfunc, covfunc, likfunc, X_full,y_full,X_valid,y_valid);

        mae_pred = mean(abs(y_valid - predictions))
        mae_pred
        mean(loglik)

        %save ( '-6','optim.out', 'hyp_opt', 'nlls', 'avg_rmse');
    else
        %save ( '-6','optim.out', 'hyp_opt', 'nlls', 'avg_rmse');
        
    end
catch
    disp('Error occured')
    disp(lasterror)
    save ( '-6','optim.out', 'hyp_opt',  'nlls', 'avg_rmse');
end_try_catch
display(['Timespent=', num2str(toc)]);
% exit();

