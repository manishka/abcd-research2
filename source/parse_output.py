import os
from flexible_function import GPModel
import experiment
from sklearn.metrics import mean_squared_error
from math import sqrt
import scipy.io
import numpy as np
from sklearn import preprocessing
import csv
from scipy.stats import norm
from postprocessing import *

file_dict={
 '01-airline_result.txt':'01-airline',
 '02-solar_result.txt':'02-solar',
 '03-mauna2003_result.txt':'03-mauna',
 'beveridge-wheat-price-index-1500_result.txt':'04-wheat',
 'daily-minimum-temperatures-in-me_result.txt':'05-min-temp',
 'internet-traffic-data-in-bits-fr-2_result.txt':'06-internet',
 'monthly-average-daily-calls-to-d_result.txt':'07-calls',
 'monthly-critical-radio-frequenci_result.txt':'08-radio',
 'monthly-production-of-gas-in-aus_result.txt':'09-gas',
 'monthly-production-of-sulphuric-_result.txt':'10-sulphuric',
 'monthly-us-male-1619-years-unemp_result.txt':'11-unemploy',
 'number-of-daily-births-in-quebec_result.txt':'12-quebec',
 'real-daily-wages-in-pounds-engla_result.txt':'13-wages',
 '05-quebec_result.txt':'14-quebec-xl',
 '06-concrete_result.txt':'15-concrete',
 '07-parkinsons_result.txt':'16-parkinsons',
 '08-winewhite_result.txt':'17-winewhite'
}

attr_dict={
 '01-airline_result.txt':'1',
 '02-solar_result.txt':'1',
 '03-mauna2003_result.txt':'1',
 'beveridge-wheat-price-index-1500_result.txt':'1',
 'daily-minimum-temperatures-in-me_result.txt':'1',
 'internet-traffic-data-in-bits-fr-2_result.txt':'1',
 'monthly-average-daily-calls-to-d_result.txt':'1',
 'monthly-critical-radio-frequenci_result.txt':'1',
 'monthly-production-of-gas-in-aus_result.txt':'1',
 'monthly-production-of-sulphuric-_result.txt':'1',
 'monthly-us-male-1619-years-unemp_result.txt':'1',
 'number-of-daily-births-in-quebec_result.txt':'1',
 'real-daily-wages-in-pounds-engla_result.txt':'1',
 '05-quebec_result.txt':'5',
 '06-concrete_result.txt':'9',
 '07-parkinsons_result.txt':'21',
 '08-winewhite_result.txt':'12'
}

def calculate_trivial_var_nll_on_training():
    variances = {}
    nlls = {}
    for directory in ['/projects/uoa00427/abcd-research2/data/uci-regression/pred','/projects/uoa00427/abcd-research2/data/tsdlr_9010']:
        for (dirn, dirn2, filen) in os.walk(directory):
            for filename in filen:
                    if filename.endswith('.mat'):
                        data = scipy.io.loadmat(dirn+"/"+filename)
                        variances[filename]= np.var(data['y'])
                        means[filename] = np.mean(data['y'])
                        nlls[filename] = norm.nnlf(norm.fit(data['X']), data['X'])
    return (variances, means, nlls)

def smse(y_actual,y_predicted,var_test, mean_test):
    pred_err = ((y_predicted - y_actual)**2)
    return np.mean(pred_err)/(var_test+mean_test**2)

def msll(y_actual,y_predicted,vars,var_test, mean_test):
    return 0.5 * np.mean(((y_actual - y_predicted)/np.sqrt(vars))**2 + np.log(vars)) - 0.5 * (var_test + mean_test**2)

def print_pred(file_):
    result = scipy.io.loadmat(file_)
    print 'RMSE =', smse(result['actuals'],result['predictions'])
    print 'MAE  =', np.mean(np.abs(result['actuals'] - result['predictions'])) 
    SSE = np.sum((result['actuals'] - result['predictions']) * (result['actuals'] - result['predictions']))
    ymean = np.mean(result['actuals'])
    SST = np.sum((result['actuals'] - ymean) * (result['actuals'] - ymean))
    print 'R2   =', 1 - SSE / SST
                
def print_models(models,score):
    for model in models:
        kernel = str(model.kernel)
        print '%02d' %int(kernel.count('Kernel') - kernel.count('SumKernel') - kernel.count('ProductKernel')), model.ndata, '%f'%(model.nll), '%f'%(GPModel.score(model,score))#,model.pretty_print() 


def print_directory(directory,score='bic',pred=False, cv=False):
    sep = '=' * 80
    cvdir = directory.split(score)[0] + 'cv-'+ score + directory.split(score)[1] if cv else None
    print sep,'\n',directory,'\n',cvdir,'\n',sep
    
    models=[]
    for (dirpath, dirnames, filenames) in os.walk(directory) :
        for file_ in filenames:
            if file_.endswith('_result.txt'):
                try:
                    print file_, '\n', '-'* 40
                    for level in range(3):
                        models.append(experiment.parse_results(dirpath+file_, level))
                        print_models([experiment.parse_results(dirpath+file_, level)], score)
                    if pred:
                        print_pred(dirpath+file_.split('_result.txt')[0]+'_predictions.mat')
                    print sep[1:40]
                    if cv:
                        for level in range(3):
                            models.append(experiment.parse_results(cvdir+file_, level))
                            print_models([experiment.parse_results(cvdir+file_, level)], score)
                        if pred:
                            print_pred(cvdir+file_.split('_result.txt')[0]+'_predictions.mat')
                        print sep[1:40]
                except IOError as e:
                    print e

def print_model_pred(model, score, pred_file, file_, cv, beam=False):
    class_ = score + ('-cv' if cv else '-nocv') + ('-beam' if beam else '')
    kernel = str(model.kernel)
    result = scipy.io.loadmat(pred_file) 
    #SSE = np.sum((result['actuals'] - result['predictions']) * (result['actuals'] - result['predictions']))
    ymean = np.mean(result['actuals'])
    yvars = np.var(result['actuals'])
    #SST = np.sum((result['actuals'] - ymean) * (result['actuals'] - ymean))
    print file_dict[file_],'%08s' %class_, '%02d' %model.kernel.effective_params, model.ndata, model.nll,\
    GPModel.score(model,score), smse(result['actuals'],result['ymu'], yvars,ymean), msll(result['actuals'],result['ymu'], result['ys2'],yvars,ymean), attr_dict[file_] 
#, np.mean(np.abs(result['actuals'] - result['predictions'])), '%2.6f%%'%((1 - SSE / SST)*100) 
    
        
def print_best_model_pred(dir_,startrun=1, endrun=1,score='bic',cv=False, beam=False):
    sep = '=' * 80
    dir_ = dir_ +'%s-' %score
    directory = dir_ + ('beam-' if beam else '') + '%03d/' % startrun
    #print directory
    #print '\n',sep,'\n',dir_,'\n',sep  
    #(vars_train, means_train, nll_trivial) = calculate_trivial_var_nll_on_training() 
    models=[]
    level = 3
    for (dirpath, dirnames, filenames) in os.walk(directory) :
        for file_ in filenames:
            if file_.endswith('_result.txt'):
                #print '\n','-'* 40,'\n',file_, '\n', '-'* 40
                for run in range(startrun,endrun+1):
                    dirpath = dir_ +  ('beam-' if beam else '') + '%03d/' %run
                    #print dirpath
                    try:
                        file_prefix = file_.split('_result.txt')[0]
                        models.append(experiment.parse_results(dirpath+file_, level))
                        print_model_pred(models[-1], score, dirpath+file_prefix+'_predictions.mat', file_, False, beam)
                    except IOError as e:
                        pass#print e
                if cv:
                    #print sep[1:40]
                    for run in range(startrun,endrun+1):
                        dirpath = dir_ + ('beam-' if beam else '') + '%03d/' %run
                        cvdir = dirpath.split(score)[0] + 'cv-'+ score + dirpath.split(score)[1]
                        #print cvdir
                        try:
                            file_prefix = file_.split('_result.txt')[0]
                            models.append(experiment.parse_results(cvdir+file_, level))
                            print_model_pred(models[-1], score, cvdir+file_prefix+'_predictions.mat', file_, True, beam)
                        except IOError as e:
                            pass#print e


def produce_report():
    make_all_1d_figures('../mak-results/2016-08-27-experiment-tsd-bic-001/',data_folder='../data/tsdlr_9010/01-airline.mat')


#variances = {'01-airline.mat': 0.0, '07-parkinsons.mat': -2.2363213775388815e-15, '03-mauna2003.mat': 0.0, '05-quebec.mat': 6.3241700253658638e-15, 'monthly-us-male-1619-years-unemp.mat': 1.982549758633086e-14, 'monthly-production-of-sulphuric-.mat': -1.09577674912401e-14, 'internet-traffic-data-in-bits-fr-2.mat': 2.0491145756116138e-12, 'monthly-average-daily-calls-to-d.mat': -6.1755813083346976e-14, '06-concrete.mat': 4.9055809157115874e-15, 'monthly-critical-radio-frequenci.mat': 2.2204460492503131e-16, 'monthly-production-of-gas-in-aus.mat': -1.0879936619339701e-12, 'real-daily-wages-in-pounds-engla.mat': -4.2998047549779135e-16, 'daily-minimum-temperatures-in-me.mat': -3.1614804705677427e-17, '08-winewhite.mat': -8.3820830897289502e-17, 'beveridge-wheat-price-index-1500.mat': 5.4624306412788482e-15, 'number-of-daily-births-in-quebec.mat': 8.1477688848638588e-15, '02-solar.mat': 2.941371369307185e-13}

#for runs in range(1,3):
    #print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-bic-%03d/'%runs,score='bic',pred=True, cv=True)
    #print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-27-experiment-tsd-bic-%03d/'%runs,score='bic',pred = True, cv = True)
    #print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-yah-bic-%03d/'%runs,score='bic',pred = True, cv = True)
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-bic-',runs=5,score='bic',cv=True)
print 'dataset method parms inst nll score smse msll attr'

# greedy search bic
print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-27-experiment-tsd-',startrun=1, endrun=1,score='bic',cv=True)
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-',startrun=1, endrun=5,score='bic',cv=True)

# greedy search aic
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-27-experiment-tsd-',startrun=11, endrun=15,score='aic',cv=True)
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-',startrun=11,endrun=15,score='aic',cv=True)    

# beam search bic
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-27-experiment-tsd-',startrun=5, endrun=8,score='bic',cv=True, beam=True)
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-',startrun=6,endrun=7,score='bic',cv=True,beam=True)

# beam search aic
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-27-experiment-tsd-',startrun=5, endrun=8,score='aic',cv=True,beam=True)
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/2016-08-28-experiment-',startrun=6,endrun=7,score='aic',cv=True,beam=True)
#print_best_model_pred('/projects/uoa00427/abcd-research2/mak-results/ttsd-', startrun=1, endrun=2, score='bic',cv=True, beam=False) 
# ==========
#print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-18-concrete-bic/')
#print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-18-concrete-cv/')
#print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-18-quebec-bic/')
#print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-18-quebec-cv/')
#print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-23-winewhite-cv/',True)
#print_directory('/projects/uoa00427/abcd-research2/mak-results/2016-08-23-winewhite-bic/',True)
# def parse_models(directory, level):
    # models=[]
    # for (dirpath, dirnames, filenames) in os.walk(directory) :
         # for file_ in filenames:
                 # if file_.endswith('.txt'):
                         # models.append(experiment.parse_results(dirpath+file_, level))
    
#    return models
#numpy.mean([model.nll for model in models if model.rmse is not None])
#numpy.mean([model.rmse for model in models if model.rmse is not None])
#number_of_kernels_in_model = [str(model.kernel).count('Kernel') for model in models]

# def print_pred_rmse(directory):
    # for (dirpath, dirnames, filenames) in os.walk(directory) :
        # for file_ in filenames:
            # if file_.endswith('_predictions.mat'):
                # result = scipy.io.loadmat(dirpath+file_)
                # print 'RMSE=',rmse(result['actuals'],result['predictions'])
