raw_data_folder = '../uci-regression/'
extrap_data_folder = '../uci-regression-9010/'

import scipy.io
import os
import numpy as np

if not os.path.isdir(extrap_data_folder):
	os.mkdir(extrap_data_folder)

for data_file_name in os.listdir(raw_data_folder):
    if data_file_name.endswith('.mat'):
        print 'Working on ',data_file_name
        data = scipy.io.loadmat(os.path.join(raw_data_folder, data_file_name))
        X = data['X']
        y = data['y']
        cut_off = np.min(y) + 0.9*(np.max(y) - np.min(y))
        print X[[0,1,-2,-1]], len(X)
        print cut_off
        print y>cut_off
        X_test = X[y > cut_off]
        y_test = y[y > cut_off]
        y = y[y <= cut_off]
        X = X[y <= cut_off]
        data = {'X' : X, 'y' : y, 'Xtest' : X_test, 'ytest' : y_test}
        scipy.io.savemat(os.path.join(extrap_data_folder, data_file_name), data)