tic;
rand('twister', 0);
randn('state', 0);

arg_list = argv ();

file_name= arg_list{1};
 
K=15;
mat_file = sprintf('%s.mat',file_name)
load (mat_file)
X = double(X);
y = double(y);

X_full = X;
y_full = y;

tic;
cv = cvpartition(y_full, 'HoldOut');
for k=1:K
    % training/testing indices for this fold
    trainIdx = training(cv,1);
    testIdx = test(cv,1);
    
    
    X = X_full(trainIdx,:);
    y = y_full(trainIdx);
    Xtest = X_full(testIdx,:);
    ytest = y_full(testIdx);
    save('-6',[file_name,'-pred-', sprintf('%3.3d',k), '.mat'], 'X', 'y','Xtest','ytest');
    cv = repartition (cv)
end

display(['Timespent CV is: ',num2str(toc), ' seconds']);

