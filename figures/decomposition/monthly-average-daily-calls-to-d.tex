
\documentclass{article} % For LaTeX2e
\usepackage{format/nips13submit_e}
\nipsfinalcopy % Uncomment for camera-ready version
\usepackage{times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{color}
\definecolor{mydarkblue}{rgb}{0,0.08,0.45}
\hypersetup{
    pdfpagemode=UseNone,
    colorlinks=true,
    linkcolor=mydarkblue,
    citecolor=mydarkblue,
    filecolor=mydarkblue,
    urlcolor=mydarkblue,
    pdfview=FitH}

\usepackage{graphicx, amsmath, amsfonts, bm, lipsum, capt-of}

\usepackage{natbib, xcolor, wrapfig, booktabs, multirow, caption}

\usepackage{float}

\def\ie{i.e.\ }
\def\eg{e.g.\ }

\title{An automatic report for the dataset : monthly-average-daily-calls-to-d}

\author{
The Automatic Statistician
}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\setlength{\marginparwidth}{0.9in}
\input{include/commenting.tex}

%% For submission, make all render blank.
%\renewcommand{\LATER}[1]{}
%\renewcommand{\fLATER}[1]{}
%\renewcommand{\TBD}[1]{}
%\renewcommand{\fTBD}[1]{}
%\renewcommand{\PROBLEM}[1]{}
%\renewcommand{\fPROBLEM}[1]{}
%\renewcommand{\NA}[1]{#1}  % Note, NA's pass through!

\begin{document}

\allowdisplaybreaks

\maketitle

\begin{abstract}
This report was produced by the Automatic Bayesian Covariance Discovery (ABCD) algorithm.
%See \url{http://arxiv.org/abs/1302.4922} and \url{http://www-kd.iai.uni-bonn.de/cml/proceedings/papers/2.pdf} for preliminary papers.
\end{abstract}

\section{Executive summary}

The raw data and full model posterior with extrapolations are shown in figure~\ref{fig:rawandfit}.

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_raw_data} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_all}
\end{tabular}
\caption{Raw data (left) and model posterior with extrapolation (right)}
\label{fig:rawandfit}
\end{figure}

The structure search algorithm has identified six additive components in the data.
The  first 3 additive components explain 98.8\% of the variation in the data as shown by the coefficient of determination ($R^2$) values in table~\ref{table:stats}.
The  first 4 additive components explain 99.4\% of the variation in the data.
After the first 3 components the cross validated mean absolute error (MAE) does not decrease by more than 0.1\%.
This suggests that subsequent terms are modelling very short term trends, uncorrelated noise or are artefacts of the model or search procedure.
Short summaries of the additive components are as follows:
\begin{itemize}

  \item \input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_1_short_description.tex} 

  \item \input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_2_short_description.tex} 

  \item \input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_3_short_description.tex} 

  \item \input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_4_short_description.tex} 

  \item \input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_5_short_description.tex} 

  \item \input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_6_short_description.tex} 

\end{itemize}

\begin{table}[htb]
\begin{center}
{\small
\begin{tabular}{|r|rrrrr|}
\hline
\bf{\#} & {$R^2$ (\%)} & {$\Delta R^2$ (\%)} & {Residual $R^2$ (\%)} & {Cross validated MAE} & Reduction in MAE (\%)\\
\hline
- & - & - & - & 523.06 & -\\

1 & 2.8 & 2.8 & 2.8 & 167.83 & 67.9\\

2 & 59.9 & 57.1 & 58.8 & 113.48 & 32.4\\

3 & 98.8 & 38.9 & 97.1 & 100.12 & 11.8\\

4 & 99.4 & 0.6 & 51.9 & 100.12 & 0.0\\

5 & 99.5 & 0.0 & 8.4 & 100.12 & 0.0\\

6 & 100.0 & 0.5 & 100.0 & 100.12 & 0.0\\

\hline
\end{tabular}
\caption{
Summary statistics for cumulative additive fits to the data.
The residual coefficient of determination ($R^2$) values are computed using the residuals from the previous fit as the target values; this measures how much of the residual variance is explained by each new component.
The mean absolute error (MAE) is calculated using 10 fold cross validation with a contiguous block design; this measures the ability of the model to interpolate and extrapolate over moderate distances.
The model is fit using the full data and the MAE values are calculated using this model; this double use of data means that the MAE values cannot be used reliably as an estimate of out-of-sample predictive performance.
}
\label{table:stats}
}
\end{center}
\end{table}

Model checking statistics are summarised in table~\ref{table:check} in section~\ref{sec:check}.
These statistics have revealed statistically significant discrepancies between the data and model in components 3 and 6.

The rest of the document is structured as follows.
In section~\ref{sec:discussion} the forms of the additive components are described and their posterior distributions are displayed.
In section~\ref{sec:extrap} the modelling assumptions of each component are discussed with reference to how this affects the extrapolations made by the model.
Section~\ref{sec:check} discusses model checking statistics, with plots showing the form of any detected discrepancies between the model and observed data.

\section{Detailed discussion of additive components}
\label{sec:discussion}

\subsection{Component 1 : A very smooth monotonically increasing function}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_1_description.tex}

This component explains 2.8\% of the total variance.
The addition of this component reduces the cross validated MAE by 67.9\% from 523.1 to 167.8.


\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1_cum}
\end{tabular}
\caption{Pointwise posterior of component 1 (left) and the posterior of the cumulative sum of components with data (right)}
\label{fig:comp1}
\end{figure}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1_anti_cum}
\end{tabular}
\caption{Pointwise posterior of residuals after adding component 1}
\label{fig:comp1}
\end{figure}

\subsection{Component 2 : A very smooth monotonically decreasing function with marginal standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_2_description.tex}

This component explains 58.8\% of the residual variance; this increases the total variance explained from 2.8\% to 59.9\%.
The addition of this component reduces the cross validated MAE by 32.39\% from 167.83 to 113.48.


\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2_cum}
\end{tabular}
\caption{Pointwise posterior of component 2 (left) and the posterior of the cumulative sum of components with data (right)}
\label{fig:comp2}
\end{figure}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2_anti_cum}
\end{tabular}
\caption{Pointwise posterior of residuals after adding component 2}
\label{fig:comp2}
\end{figure}

\subsection{Component 3 : A smooth monotonically increasing function with marginal standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_3_description.tex}

This component explains 97.1\% of the residual variance; this increases the total variance explained from 59.9\% to 98.8\%.
The addition of this component reduces the cross validated MAE by 11.77\% from 113.48 to 100.12.


\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3_cum}
\end{tabular}
\caption{Pointwise posterior of component 3 (left) and the posterior of the cumulative sum of components with data (right)}
\label{fig:comp3}
\end{figure}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3_anti_cum}
\end{tabular}
\caption{Pointwise posterior of residuals after adding component 3}
\label{fig:comp3}
\end{figure}

\subsection{Component 4 : Uncorrelated noise}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_4_description.tex}

This component explains 51.9\% of the residual variance; this increases the total variance explained from 98.8\% to 99.4\%.
The addition of this component reduces the cross validated MAE by 0.00\% from 100.12 to 100.12.
This component explains residual variance but does not improve MAE which suggests that this component describes very short term patterns, uncorrelated noise or is an artefact of the model or search procedure.

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4_cum}
\end{tabular}
\caption{Pointwise posterior of component 4 (left) and the posterior of the cumulative sum of components with data (right)}
\label{fig:comp4}
\end{figure}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4_anti_cum}
\end{tabular}
\caption{Pointwise posterior of residuals after adding component 4}
\label{fig:comp4}
\end{figure}

\subsection{Component 5 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_5_description.tex}

This component explains 8.4\% of the residual variance; this increases the total variance explained from 99.4\% to 99.5\%.
The addition of this component reduces the cross validated MAE by 0.00\% from 100.12 to 100.12.
This component explains residual variance but does not improve MAE which suggests that this component describes very short term patterns, uncorrelated noise or is an artefact of the model or search procedure.

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5_cum}
\end{tabular}
\caption{Pointwise posterior of component 5 (left) and the posterior of the cumulative sum of components with data (right)}
\label{fig:comp5}
\end{figure}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5_anti_cum}
\end{tabular}
\caption{Pointwise posterior of residuals after adding component 5}
\label{fig:comp5}
\end{figure}

\subsection{Component 6 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_6_description.tex}

This component explains 100.0\% of the residual variance; this increases the total variance explained from 99.5\% to 100.0\%.
The addition of this component reduces the cross validated MAE by 0.00\% from 100.12 to 100.12.
This component explains residual variance but does not improve MAE which suggests that this component describes very short term patterns, uncorrelated noise or is an artefact of the model or search procedure.

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_6} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_6_cum}
\end{tabular}
\caption{Pointwise posterior of component 6 (left) and the posterior of the cumulative sum of components with data (right)}
\label{fig:comp6}
\end{figure}

\section{Extrapolation}
\label{sec:extrap}

Summaries of the posterior distribution of the full model are shown in figure~\ref{fig:extrap}.
The plot on the left displays the mean of the posterior together with pointwise variance.
The plot on the right displays three random samples from the posterior.

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_all} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_all_sample}
\end{tabular}
\caption{Full model posterior with extrapolation. Mean and pointwise variance (left) and three random samples (right)}
\label{fig:extrap}
\end{figure}

Below are descriptions of the modelling assumptions associated with each additive component and how they affect the predictive posterior.
Plots of the pointwise posterior and samples from the posterior are also presented, showing extrapolations from each component and the cuulative sum of components.

\subsection{Component 1 : A very smooth monotonically increasing function}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_1_extrap_description.tex}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1_sample} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1_cum_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_1_cum_sample}
\end{tabular}
\caption{Posterior of component 1 (top) and cumulative sum of components (bottom) with extrapolation. Mean and pointwise variance (left) and three random samples from the posterior distribution (right).}
\label{fig:extrap1}
\end{figure}

\subsection{Component 2 : A very smooth monotonically decreasing function with marginal standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_2_extrap_description.tex}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2_sample} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2_cum_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_2_cum_sample}
\end{tabular}
\caption{Posterior of component 2 (top) and cumulative sum of components (bottom) with extrapolation. Mean and pointwise variance (left) and three random samples from the posterior distribution (right).}
\label{fig:extrap2}
\end{figure}

\subsection{Component 3 : A smooth monotonically increasing function with marginal standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_3_extrap_description.tex}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3_sample} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3_cum_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_3_cum_sample}
\end{tabular}
\caption{Posterior of component 3 (top) and cumulative sum of components (bottom) with extrapolation. Mean and pointwise variance (left) and three random samples from the posterior distribution (right).}
\label{fig:extrap3}
\end{figure}

\subsection{Component 4 : Uncorrelated noise}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_4_extrap_description.tex}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4_sample} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4_cum_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_4_cum_sample}
\end{tabular}
\caption{Posterior of component 4 (top) and cumulative sum of components (bottom) with extrapolation. Mean and pointwise variance (left) and three random samples from the posterior distribution (right).}
\label{fig:extrap4}
\end{figure}

\subsection{Component 5 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_5_extrap_description.tex}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5_sample} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5_cum_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_5_cum_sample}
\end{tabular}
\caption{Posterior of component 5 (top) and cumulative sum of components (bottom) with extrapolation. Mean and pointwise variance (left) and three random samples from the posterior distribution (right).}
\label{fig:extrap5}
\end{figure}

\subsection{Component 6 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

\input{monthly-average-daily-calls-to-d/monthly-average-daily-calls-to-d_6_extrap_description.tex}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_6_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_6_sample} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_6_cum_extrap} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_6_cum_sample}
\end{tabular}
\caption{Posterior of component 6 (top) and cumulative sum of components (bottom) with extrapolation. Mean and pointwise variance (left) and three random samples from the posterior distribution (right).}
\label{fig:extrap6}
\end{figure}

\section{Model checking}
\label{sec:check}

Several posterior predictive checks have been performed to assess how well the model describes the observed data.
These tests take the form of comparing statistics evaluated on samples from the prior and posterior distributions for each additive component.
The statistics are derived from autocorrelation function (ACF) estimates, periodograms and quantile-quantile (qq) plots.

Table~\ref{table:check} displays cumulative probability and $p$-value estimates for these quantities.
Cumulative probabilities near 0/1 indicate that the test statistic was lower/higher under the posterior compared to the prior unexpectedly often \ie they contain the same information as a $p$-value for a two-tailed test and they also express if the test statistic was higher or lower than expected.
$p$-values near 0 indicate that the test statistic was larger in magnitude under the posterior compared to the prior unexpectedly often.

\begin{table}[htb]
\begin{center}
{\small
\begin{tabular}{|r|rr|rr|rr|}
\hline
 & \multicolumn{2}{|c|}{ACF} & \multicolumn{2}{|c|}{Periodogram} & \multicolumn{2}{|c|}{QQ} \\
\bf{\#} & {min} & {min loc} & {max} & {max loc} & {max} & {min}\\
\hline

1 & \textcolor{gray}{0.506} & \textcolor{gray}{0.436} & \textcolor{gray}{0.820} & \textcolor{gray}{0.361} & \textcolor{gray}{0.161} & \textcolor{gray}{0.863}\\

2 & \textcolor{gray}{0.271} & \textcolor{gray}{0.305} & \textcolor{gray}{0.326} & \textcolor{gray}{0.495} & \textcolor{gray}{0.283} & \textcolor{gray}{0.547}\\

3 & \textcolor{gray}{0.482} & \textcolor{gray}{0.527} & 0.992 & \textcolor{gray}{0.276} & \textcolor{gray}{0.432} & \textcolor{gray}{0.372}\\

4 & \textcolor{gray}{0.464} & \textcolor{gray}{0.511} & \textcolor{gray}{0.472} & \textcolor{gray}{0.496} & \textcolor{gray}{0.422} & \textcolor{gray}{0.583}\\

5 & \textcolor{gray}{0.505} & \textcolor{gray}{0.511} & \textcolor{gray}{0.508} & \textcolor{gray}{0.486} & \textcolor{gray}{0.479} & \textcolor{gray}{0.476}\\

6 & \textcolor{gray}{0.484} & \textcolor{gray}{0.480} & \textcolor{gray}{0.534} & \textcolor{gray}{0.505} & \textcolor{gray}{0.399} & 0.038\\

\hline
\end{tabular}
\caption{
Model checking statistics for each component.
Cumulative probabilities for minimum of autocorrelation function (ACF) and its location.
Cumulative probabilities for maximum of periodogram and its location.
$p$-values for maximum and minimum deviations of QQ-plot from straight line.
}
\label{table:check}
}
\end{center}
\end{table}

The nature of any observed discrepancies is now described and plotted and hypotheses are given for the patterns in the data that may not be captured by the model.

\subsection{Moderately statistically significant discrepancies}

\subsubsection{Component 3 : A smooth monotonically increasing function with marginal standard deviation increasing linearly away from Sep 1964}


The following discrepancies between the prior and posterior distributions for this component have been detected.

\begin{itemize}

    \item The maximum value of the periodogram is unexpectedly high. This discrepancy has an estimated $p$-value of 0.016.
\end{itemize}

The large maximum value of the periodogram can indicate periodicity that is not being captured by the model.


\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_acf_bands_3} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_pxx_bands_3} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_qq_bands_3}
\end{tabular}
\caption{
ACF (top left), periodogram (top right) and quantile-quantile (bottom left) uncertainty plots.
The blue line and shading are the pointwise mean and 90\% confidence interval of the plots under the prior distribution for component 3.
The green line and green dashed lines are the corresponding quantities under the posterior.}
\label{fig:check3}
\end{figure}

\subsubsection{Component 6 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}


The following discrepancies between the prior and posterior distributions for this component have been detected.

\begin{itemize}

    \item The qq plot has an unexpectedly large negative deviation from equality ($x = y$). This discrepancy has an estimated $p$-value of 0.038.
\end{itemize}

The negative deviation in the qq-plot can indicate light positive tails if it occurs at the right of the plot or heavy negative tails if it occurs as the left.


\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_acf_bands_6} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_pxx_bands_6} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_qq_bands_6}
\end{tabular}
\caption{
ACF (top left), periodogram (top right) and quantile-quantile (bottom left) uncertainty plots.
The blue line and shading are the pointwise mean and 90\% confidence interval of the plots under the prior distribution for component 6.
The green line and green dashed lines are the corresponding quantities under the posterior.}
\label{fig:check6}
\end{figure}

\subsection{Model checking plots for components without statistically significant discrepancies}

\subsubsection{Component 1 : A very smooth monotonically increasing function}

No discrepancies between the prior and posterior of this component have been detected

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_acf_bands_1} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_pxx_bands_1} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_qq_bands_1}
\end{tabular}
\caption{
ACF (top left), periodogram (top right) and quantile-quantile (bottom left) uncertainty plots.
The blue line and shading are the pointwise mean and 90\% confidence interval of the plots under the prior distribution for component 1.
The green line and green dashed lines are the corresponding quantities under the posterior.}
\label{fig:check1}
\end{figure}

\subsubsection{Component 2 : A very smooth monotonically decreasing function with marginal standard deviation increasing linearly away from Sep 1964}

No discrepancies between the prior and posterior of this component have been detected

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_acf_bands_2} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_pxx_bands_2} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_qq_bands_2}
\end{tabular}
\caption{
ACF (top left), periodogram (top right) and quantile-quantile (bottom left) uncertainty plots.
The blue line and shading are the pointwise mean and 90\% confidence interval of the plots under the prior distribution for component 2.
The green line and green dashed lines are the corresponding quantities under the posterior.}
\label{fig:check2}
\end{figure}

\subsubsection{Component 4 : Uncorrelated noise}

No discrepancies between the prior and posterior of this component have been detected

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_acf_bands_4} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_pxx_bands_4} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_qq_bands_4}
\end{tabular}
\caption{
ACF (top left), periodogram (top right) and quantile-quantile (bottom left) uncertainty plots.
The blue line and shading are the pointwise mean and 90\% confidence interval of the plots under the prior distribution for component 4.
The green line and green dashed lines are the corresponding quantities under the posterior.}
\label{fig:check4}
\end{figure}

\subsubsection{Component 5 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

No discrepancies between the prior and posterior of this component have been detected

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\begin{tabular}{cc}
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_acf_bands_5} & \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_pxx_bands_5} \\
\mbm \includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_qq_bands_5}
\end{tabular}
\caption{
ACF (top left), periodogram (top right) and quantile-quantile (bottom left) uncertainty plots.
The blue line and shading are the pointwise mean and 90\% confidence interval of the plots under the prior distribution for component 5.
The green line and green dashed lines are the corresponding quantities under the posterior.}
\label{fig:check5}
\end{figure}

\section{MMD - experimental section}
\label{sec:mmd}

\begin{table}[htb]
\begin{center}
{\small
\begin{tabular}{|r|r|}
\hline
\bf{\#} & {mmd}\\
\hline

1 & \textbf{0.000}\\

2 & \textbf{0.000}\\

3 & \textbf{0.000}\\

4 & 0.029\\

5 & \textcolor{gray}{0.936}\\

6 & \textcolor{gray}{0.183}\\

\hline
\end{tabular}
\caption{
MMD $p$-values
}
\label{table:mmd}
}
\end{center}
\end{table}

\subsubsection{Component 1 : A very smooth monotonically increasing function}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_mmd_1}
\caption{
MMD plot}
\label{fig:mmd1}
\end{figure}

\subsubsection{Component 2 : A very smooth monotonically decreasing function with marginal standard deviation increasing linearly away from Sep 1964}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_mmd_2}
\caption{
MMD plot}
\label{fig:mmd2}
\end{figure}

\subsubsection{Component 3 : A smooth monotonically increasing function with marginal standard deviation increasing linearly away from Sep 1964}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_mmd_3}
\caption{
MMD plot}
\label{fig:mmd3}
\end{figure}

\subsubsection{Component 4 : Uncorrelated noise}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_mmd_4}
\caption{
MMD plot}
\label{fig:mmd4}
\end{figure}

\subsubsection{Component 5 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_mmd_5}
\caption{
MMD plot}
\label{fig:mmd5}
\end{figure}

\subsubsection{Component 6 : Uncorrelated noise with standard deviation increasing linearly away from Sep 1964}

\begin{figure}[H]
\newcommand{\wmgd}{0.5\columnwidth}
\newcommand{\hmgd}{3.0cm}
\newcommand{\mdrd}{monthly-average-daily-calls-to-d}
\newcommand{\mbm}{\hspace{-0.3cm}}
\includegraphics[width=\wmgd,height=\hmgd]{\mdrd/monthly-average-daily-calls-to-d_mmd_6}
\caption{
MMD plot}
\label{fig:mmd6}
\end{figure}

\end{document}
